package com.yesitlabs.govava_daniyal.model;

public class ChatListModel {
    private String cgroup_id, group_name, user_id;

        public ChatListModel() {
        }

        public ChatListModel(String cgroup_id, String group_name, String user_id) {
            this.cgroup_id = cgroup_id;
            this.group_name = group_name;
            this.user_id = user_id;
        }

        public String getCgroup_id() {
            return cgroup_id;
        }

        public void setCgroup_id(String cgroup_id) {
            this.cgroup_id = cgroup_id;
        }

        public String getGroup_name() {
            return group_name;
        }

        public void setGroup_name(String group_name) {
            this.group_name = group_name;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }
}