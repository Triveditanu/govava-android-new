package com.yesitlabs.govava_daniyal.model;

public class ChatMemberListModel {


    private String gmember, member_phone, group_id,name;

    public ChatMemberListModel() {
    }

    public ChatMemberListModel(String gmember, String member_phone, String group_id,String name) {
        this.gmember = gmember;
        this.member_phone = member_phone;
        this.group_id = group_id;
        this.name = name;
    }

    public String getGmember() {
        return gmember;
    }

    public void setGmember(String gmember) {
        this.gmember = gmember;
    }


    public String getMember_phone() {
        return member_phone;
    }

    public void setMember_phone(String member_phone) {
        this.member_phone = member_phone;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



}
