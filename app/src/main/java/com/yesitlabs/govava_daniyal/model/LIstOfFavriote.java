package com.yesitlabs.govava_daniyal.model;

public class LIstOfFavriote {
    private String product_id;
    private String type;

    public LIstOfFavriote(String product_id, String type) {
        this.product_id = product_id;
        this.type = type;

    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getProduct_id() {
        return product_id;
    }



}
