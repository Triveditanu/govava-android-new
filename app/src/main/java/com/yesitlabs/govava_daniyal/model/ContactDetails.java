package com.yesitlabs.govava_daniyal.model;

import java.io.Serializable;

/**
 * Created by AST on 8/20/2018.
 */

public class ContactDetails implements Serializable {

    public String name,image,phoneNo;

    public ContactDetails(String name, String image, String phoneNo) {
        this.name = name;
        this.image = image;
        this.phoneNo = phoneNo;
    }
}
