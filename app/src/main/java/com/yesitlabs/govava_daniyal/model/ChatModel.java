package com.yesitlabs.govava_daniyal.model;

public class ChatModel {

    private String username;
    private String messages;
    private String chat_groupid;
    private String user_id;
    private String productId,productImage,productName,productPrice;


    public ChatModel(String username, String messages,String chat_groupid,String user_id) {
        this.username = username;
        this.messages = messages;
        this.chat_groupid = chat_groupid;
        this.user_id = user_id;

    }

    public ChatModel(String username, String messages, String chat_groupid, String user_id, String productId, String productImage, String productName, String productPrice) {
        this.username = username;
        this.messages = messages;
        this.chat_groupid = chat_groupid;
        this.user_id = user_id;
        this.productId = productId;
        this.productImage = productImage;
        this.productName = productName;
        this.productPrice = productPrice;
    }

    public String getUsername() {
        return username;
    }

    public String getMessages() {
        return messages;
    }


    public String getChat_groupid() {
        return chat_groupid;
    }
    public String getUser_id() {
        return user_id;
    }

    public String getProductId() {
        return productId;
    }

    public String getProductImage() {
        return productImage;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public String getProductName() {
        return productName;
    }

}
