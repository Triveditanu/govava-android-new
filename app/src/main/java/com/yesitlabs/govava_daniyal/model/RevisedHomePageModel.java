package com.yesitlabs.govava_daniyal.model;

import java.util.List;

public class RevisedHomePageModel {

    private String productTitle;
    private String productDescription;
    private String prodouctImage;
    private String productPrice;
    private String productReview;
    private String productMarketType;
    private String productId;

    private List<CatagoriesModel> catagoriesModelList;

    public List<CatagoriesModel> getCatagoriesModelList() {
        return catagoriesModelList;
    }

    public void setCatagoriesModelList(List<CatagoriesModel> catagoriesModelList) {
        this.catagoriesModelList = catagoriesModelList;
    }

    // itemType = 1 for category 2 for productList
    private int itemType;

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProdouctImage() {
        return prodouctImage;
    }

    public void setProdouctImage(String prodouctImage) {
        this.prodouctImage = prodouctImage;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductReview() {
        return productReview;
    }

    public void setProductReview(String productReview) {
        this.productReview = productReview;
    }

    public String getProductMarketType() {
        return productMarketType;
    }

    public void setProductMarketType(String productMarketType) {
        this.productMarketType = productMarketType;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }


}
