package com.yesitlabs.govava_daniyal.model;

public class ProductModel {


    private String title;
    private String description;
    private String prodouct_image;
    private String price;
    private String review;
    private String market_type;
    private String product_id;

    private String favoriteProductId;



    public ProductModel(String title, String description,String prodouct_image, String price,String review, String market_type,String product_id, String favoriteProductId  ) {
        this.title = title;
        this.description = description;
        this.prodouct_image = prodouct_image;
        this.price = price;
        this.review = review;
        this.market_type = market_type;
        this.product_id = product_id;
        this.favoriteProductId = favoriteProductId;

    }

    public String getTitle() {
        return title;
    }

    public String getFavoriteProductId() {
        return favoriteProductId;
    }

    public void setFavoriteProductId(String favoriteProductId) {
        this.favoriteProductId = favoriteProductId;
    }


    public String getDescription() {
        return description;
    }

    public String getProdouct_image() {
        return prodouct_image;
    }
    public String getPrice() {
        return price;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setProdouct_image(String prodouct_image) {
        this.prodouct_image = prodouct_image;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public void setMarket_type(String market_type) {
        this.market_type = market_type;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getReview() {
        return review;
    }

    public String getMarket_type() {
        return market_type;
    }
    public String getProduct_id() {
        return product_id;
    }
}

