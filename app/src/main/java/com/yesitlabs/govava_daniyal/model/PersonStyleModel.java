package com.yesitlabs.govava_daniyal.model;

public class PersonStyleModel {

    private String id, title, about,icon;

    public PersonStyleModel() {
    }

    public PersonStyleModel(String id, String title, String about,String icon) {
        this.id = id;
        this.title = title;
        this.about = about;
        this.icon = icon;
    }

    public String getId() {
        return id;
    }



    public String getTitle() {
        return title;
    }



    public String getAbout() {
        return about;
    }

    public String getIcon() {
        return icon;
    }


}
