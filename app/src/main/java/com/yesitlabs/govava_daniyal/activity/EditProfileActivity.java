package com.yesitlabs.govava_daniyal.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.URLS.Url_List;
import com.yesitlabs.govava_daniyal.Utils.MarshMallowPermissionUtils;
import com.yesitlabs.govava_daniyal.Utils.VolleyMultipartRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.yesitlabs.govava_daniyal.Utils.Constants.Profile_Image;

public class EditProfileActivity extends AppCompatActivity {

    private EditText et_userName,edt_dob;
    private EditText edt_email;
    private EditText editText3;
    private EditText editText4;
    private EditText edt_address;
    private EditText editText6;
    private Button button2;
    private String username = "";
    private String email = "";
    private String mobile = "";
    private String password = "";
    private SharedPreferences sp;
    private static final int STORAGE_PERMISSION_CODE = 123;

    //Bitmap to get image from gallery
    private Bitmap bitmap;
    int CAMERA_REQUEST_CODE = 101;
    int GALLERY_REQUEST_CODE = 201;
    private Uri filePath;
    private AlertDialog.Builder builder;
    private Bitmap selectedThumbBitmap;
    private String imageStr = "";
    private  String uploadId = "";
    String path = "";
    private Uri uri;
    private CircleImageView img_profile;
    private String userid = "";
    private TextView edt_mobile;
    private String address = "";
    private ImageView img_cam;
    private TextView txt_dob;
    private String dob = "";
    private ProgressDialog progressDialog;

    final int CAMERA_CAPTURE = 1;
    final int CROP_PIC = 2;
    private Uri picUri;
    final int PIC_CROP = 2;
    private Uri mCropImageUri;
    Calendar myCalendar;
    
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_edit__profile_);

        edt_dob = findViewById(R.id.edt_dob);

        myCalendar = Calendar.getInstance();

        SharedPreferences prefs = getSharedPreferences("LoginDetail", Context.MODE_PRIVATE);
        userid = prefs.getString("userid", "");
        dob = prefs.getString("dob", "");

        profileDetail();

        requestStoragePermission();

        et_userName =  findViewById(R.id.et_userName);
        edt_email =  findViewById(R.id.edt_email);

        edt_address =  findViewById(R.id.edt_address);
        edt_mobile = findViewById(R.id.edt_mobile);

        txt_dob =  findViewById(R.id.txt_dob);

        img_profile =  findViewById(R.id.img_profile);

        img_cam  =  findViewById(R.id.img_cam);
        button2 =  findViewById(R.id.button2);




        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        try {

            dob = prefs.getString("dob", "");

            //      Constants.changeDateFormat(dob,"yyyy-MM-dd","MM-dd-yyyy");
            if(dob.equalsIgnoreCase("null"))
            {
                txt_dob.setText("Date Of Birth");
            }
            else
            {
                txt_dob.setText(dob);
            }

        }catch (Exception e){

            dob = "";
            txt_dob.setText("");
        }


        if (dob == (null)||dob.equals("null")||dob.equals("")){
          //  edt_dob.setVisibility(View.VISIBLE);
            txt_dob.setVisibility(View.VISIBLE);

            txt_dob.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    new DatePickerDialog(EditProfileActivity.this, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            });

            dob = txt_dob.getText().toString().trim();


        }else {
            edt_dob.setVisibility(View.GONE);
            txt_dob.setVisibility(View.VISIBLE);
        }


        edt_email.setOnFocusChangeListener(new  View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                edt_email.setSelection(0);
            }
        });
        button2.setOnClickListener(new  View.OnClickListener() {
            @Override
            public void onClick(View v) {

                username = et_userName.getText().toString().trim();
                email = edt_email.getText().toString().trim();
                address = edt_address.getText().toString().trim();
                dob = edt_dob.getText().toString().trim();
//                password = editText3.getText().toString().trim();
                mobile = edt_mobile.getText().toString().trim();

                userProfileEdit(username,email,address,dob);
            }
        });

        img_profile.setOnClickListener(new  View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  selectThumbLayout();
                onSelectImageClick(v);
            }
        });
        img_cam.setOnClickListener(new  View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelectImageClick(v);
            }
        });


    }


    private void userProfileEdit(final String username,final String email, final String address,final String dob) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.URL_SIGNUP,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //   progressBar.setVisibility( GONE);

                        try {
                            Gson gson = new Gson();
                            JSONObject obj = new JSONObject(response);


                            Log.i("RESPONSE",response);

                            String status = obj.optString("status");
                            String message = obj.optString("message");

                            if(status.contains("1")){
                                Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Unable to process this request Please try again later", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "updateProfile");
                params.put("user_id", userid);
                params.put("email", email);
                params.put("username", username);
                params.put("dob", dob);
                params.put("address", address);

                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(EditProfileActivity.this);
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }




    private void profileDetail() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //   progressBar.setVisibility( GONE);

                        try {
                            Gson gson = new Gson();
                            JSONObject obj = new JSONObject(response);


                            Log.i("RESPONSE",response);

                            String status = obj.optString("status");
                            String message = obj.optString("message");
                            JSONObject data = obj.getJSONObject("data");
                            String id = data.optString("id");
                            String username = data.optString("username");
                            String firstname = data.optString("firstname");
                            String email = data.optString("email");
                            String mobile = data.optString("mobile");
                            String avatar = data.optString("avatar");
                            String address = data.optString("address");
                            String dob = data.optString("dob");
                            et_userName.setText(username);
                            if(!data.isNull("address"))
                            {
                                edt_address.setText(data.optString("address"));
                            }
                            if(!data.isNull("email"))
                            {
                                edt_email.setText(data.optString("email"));
                            }
                            if(!data.isNull("avatar"))
                            {
                                Glide.with(getApplicationContext())
                                        .load(Profile_Image + avatar)
                                        .into(img_profile);

                            } else {
                                img_profile.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.dummy_pic));
                            }if(!data.isNull("mobile"))
                            {
                                edt_mobile.setText(mobile);
                            }

                            if(!data.isNull("dob"))
                            {
                                  txt_dob.setText(data.optString("dob"));
                            }
                            sp = getSharedPreferences("LoginDetail", Context.MODE_PRIVATE);
                            SharedPreferences.Editor edit = sp.edit();
                            edit.putString("userid", id).commit();
                            edit.putString("email", email).commit();
                            edit.putString("mobile", mobile).commit();
                            edit.putString("dob", dob).commit();
                            edit.commit();

                            sendMessage(username,email,avatar,dob);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Unable to process this request Please try again later", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "profile_byid");
                params.put("user_id", userid);


                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(EditProfileActivity.this);
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }


    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this, Manifest.permission.CAMERA)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
            showAlertPermission();

        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.CAMERA}, CAMERA_REQUEST_CODE);
    }

    public void selectThumbLayout() {


        builder = new AlertDialog.Builder(EditProfileActivity.this);

        View view = LayoutInflater.from(EditProfileActivity.this).inflate(R.layout.popup_select_thumb_layout, null);
        builder.setView(view);
        final AlertDialog dialog = builder.show();

         findViewById(R.id.cameraButton).setOnClickListener(new  View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeImageFromCamera();
                dialog.dismiss();
            }
        });

         findViewById(R.id.galleryButton).setOnClickListener(new  View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImageFromGallery();
                dialog.dismiss();
            }
        });
    }

    private void takeImageFromCamera() {

        if (MarshMallowPermissionUtils.checkPermissionForCamera(this)) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, CAMERA_REQUEST_CODE);
        } else {
            MarshMallowPermissionUtils.requestPermissionForCamera(this);
        }
    }

    private void pickImageFromGallery() {

        if (MarshMallowPermissionUtils.checkPermissionToReadExternalStorage(this)) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(intent, GALLERY_REQUEST_CODE);
        } else {
            MarshMallowPermissionUtils.requestPermissionToReadExternalStorage(this);
        }
    }


    private void openAlertDialog(final int title, int message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton("Ok", null);
        builder.show();
    }


    public void onSelectImageClick(View view) {
        CropImage.startPickImageActivity(this);
    }


    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getApplicationContext(), data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(getApplicationContext(), imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                img_profile.setImageURI(result.getUri());

                img_profile.buildDrawingCache();
                Bitmap bitmap = img_profile.getDrawingCache();

//                selectedThumbBitmap = (Bitmap) data.getExtras().get("data");
                   uploadBitmap(bitmap);

             //   Toast.makeText(getApplicationContext(), "Cropping successful, Sample: " + result.getSampleSize(), Toast.LENGTH_LONG).show();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(getApplicationContext(), "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // required permissions granted, start crop image activity
            startCropImageActivity(mCropImageUri);
        } else {
//            Toast.makeText(getApplicationContext(), "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }

    public void addView(Bitmap selectedThumbBitmap) {


        img_profile.setImageBitmap(selectedThumbBitmap);


    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }





    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(this);
    }
    private void uploadBitmap(final Bitmap bitmap) {

        progressDialog = new ProgressDialog(EditProfileActivity.this);
        progressDialog.setMessage(" Image Uploading..."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, Url_List.URL_LOGIN,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        String jsonString = "";
                        try {
                            jsonString = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers));
                            Log.i("Image Upload RESPONSE", String.valueOf(jsonString));
                            try {
                                JSONObject obj = new JSONObject(jsonString);
                                String status = obj.optString("status");
                                String data = obj.optString("data");

                                if(status.contains("1")){

                                    progressDialog.dismiss();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


//                            JSONObject obj = new JSONObject(new String(response.data));
//
//
//                            Intent i1 = new Intent(SignUPActivity.this, LoginActivity.class);
//                            startActivity(i1);

                        //  Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Unable to process this request,please try again later", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "update_userimage");
                params.put("userid", userid);
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("photo_id", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));
                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(EditProfileActivity.this);
        volleyMultipartRequest.setShouldCache(false);
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                200000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(volleyMultipartRequest);
    }


    private void updateLabel() {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        edt_dob.setText(formatter.format(myCalendar.getTime()));
    }



    private void sendMessage(String username,String email,String avatar,String dob) {
        Log.d("sender", "Broadcasting message");
        Intent intent = new Intent("custom-event-name");
        // You can also include some extra data.
        intent.putExtra("username", username);
        intent.putExtra("email", email);
        intent.putExtra("avatar", avatar);
        intent.putExtra("dob", dob);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


    public void showAlertPermission(){
        new android.support.v7.app.AlertDialog.Builder(this)
                .setMessage("Are you sure you want to exit?")
                .setCancelable(true)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        requestStoragePermission();

                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

}
