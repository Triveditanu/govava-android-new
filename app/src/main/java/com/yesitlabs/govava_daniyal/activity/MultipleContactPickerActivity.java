package com.yesitlabs.govava_daniyal.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.yesitlabs.govava_daniyal.ContactHelper.ContactsList;
import com.yesitlabs.govava_daniyal.ContactHelper.ContactsListAdapter;
import com.yesitlabs.govava_daniyal.ContactHelper.ContactsLoader;
import com.yesitlabs.govava_daniyal.R;

import java.util.ArrayList;

public class MultipleContactPickerActivity extends BaseActivity {
    ListView contactsChooser;
    Button btnDone;
    EditText txtFilter;
    TextView txtLoadInfo;
    public ContactsListAdapter contactsListAdapter;

    private static final int READ_CONTACTS_PERMISSIONS_REQUEST = 1;

    ContactsLoader contactsLoader;
    private String prodocut_title = "";
    private String addMember = "",productId="",product_img = "",market_type = "",product_price = "";

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiple_contact_picker);
        contactsChooser = (ListView) findViewById(R.id.lst_contacts_chooser);
        btnDone = (Button) findViewById(R.id.btn_done);
        txtFilter = (EditText) findViewById(R.id.txt_filter);
        txtLoadInfo = (TextView) findViewById(R.id.txt_load_progress);
        contactsListAdapter = new ContactsListAdapter(this,new ContactsList());

        contactsChooser.setAdapter(contactsListAdapter);
        try{
            addMember =getIntent().getStringExtra("add_member");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            addMember="";
        }
        try{
            productId =getIntent().getStringExtra("productId");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            productId="";
        }

        try{
            product_price =getIntent().getStringExtra("product_price");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            product_price="";
        }

        try{
            product_img =getIntent().getStringExtra("product_img");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            product_img="";
        }

        try{
            market_type =getIntent().getStringExtra("market_type");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            market_type="";
        }

        loadContacts("");

        prodocut_title = getIntent().getStringExtra("prodocut_title");
        Log.i("Product_Name",prodocut_title);

        txtFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                contactsListAdapter.filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(contactsListAdapter.selectedContactsList.contactArrayList.isEmpty()){
                    setResult(RESULT_CANCELED);
                }
                else{

                    if(addMember!=null)
                    {
                        Intent resultIntent = new Intent();

                        resultIntent.putParcelableArrayListExtra("SelectedContacts", contactsListAdapter.selectedContactsList.contactArrayList);
                        setResult(RESULT_OK,resultIntent);
                        finish();
                    }
                    else
                    {
                        String contactName="";
                        String contactNumber="";

                        for(int i=0;i<contactsListAdapter.selectedContactsList.contactArrayList.size();i++){

                            contactName += contactsListAdapter.selectedContactsList.contactArrayList.get(i).name+",";
                            contactNumber += contactsListAdapter.selectedContactsList.contactArrayList.get(i).phone+",";
                        }

                        int lastCommaIndex = contactName.lastIndexOf(",");
                        String revisedContactName = contactName.substring(0, lastCommaIndex);
                        int lastCommaIndex1 = contactNumber.lastIndexOf(",");
                        String revisedContactNumber = contactNumber.substring(0, lastCommaIndex1);

                        System.out.println("Selected Contacts :"+revisedContactName+"\n");
                        System.out.println("Selected Contacts :"+revisedContactNumber.replaceAll(" ","")+"\n");

                        Intent intent = new Intent(MultipleContactPickerActivity.this,ChatActivity.class);
                        intent.putExtra("SelectedContactName",revisedContactName);
                        intent.putExtra("SelectedContactNumber",revisedContactNumber);
                        intent.putExtra("prodocut_title",prodocut_title);
                        intent.putExtra("productId",productId);
                        intent.putExtra("product_img",product_img);
                        intent.putExtra("market_type",market_type);
                        intent.putExtra("product_price",product_price);

                        startActivity(intent);
                        finish();
                    }



                }

            }
        });
    }

    private void loadContacts(String filter){

        if(contactsLoader!=null && contactsLoader.getStatus()!= AsyncTask.Status.FINISHED){
            try{
                contactsLoader.cancel(true);
            }catch (Exception e){

            }
        }
        if(filter==null) filter="";

        try{
            //Running AsyncLoader with adapter and  filter
            contactsLoader = new ContactsLoader(this,contactsListAdapter);
            contactsLoader.txtProgress = txtLoadInfo;
            contactsLoader.execute(filter);
        }catch(Exception e){
            e.printStackTrace();
        }
    }




}
