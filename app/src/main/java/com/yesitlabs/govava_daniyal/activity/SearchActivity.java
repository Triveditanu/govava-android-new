package com.yesitlabs.govava_daniyal.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.adapter.SearchAdapter;
import com.yesitlabs.govava_daniyal.model.LIstOfFavriote;
import com.yesitlabs.govava_daniyal.model.SearchModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SearchActivity extends AppCompatActivity {

    private ImageView img_back;
    private ArrayList<SearchModel> searchModelList;

    private SearchAdapter searchAdapter;
    private RecyclerView recyclerView;
    private LinearLayoutManager manager;
    private View view;
    Boolean isScrolling = false;
    private AutoCompleteTextView edtSearch;
    private ArrayList<String> searchList;
    String searchText = "";
    private SharedPreferences prefs;
    List<String> sample;
    ArrayAdapter<String> adapterSearch;
    Context mContext;
    private TextView clear_history_txt;
    private ImageView img_cross;
    private ArrayList<LIstOfFavriote> listOfFavriote;
    private JSONArray favoriteArray;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);


        img_back = (ImageView)findViewById(R.id.img_back);
        edtSearch = findViewById(R.id.edtSearch);

        img_cross =findViewById(R.id.img_cross);

        clear_history_txt = findViewById(R.id.clear_history_txt);
        listOfFavriote = new ArrayList<LIstOfFavriote>();
        favoriteArray = new JSONArray();

        recyclerView = findViewById(R.id.cat_grid_recycler);
        int numberOfColumns = 3;

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                firstLoad();
//                Intent i = new Intent(SearchActivity.this, HomeActivity.class);
//                startActivity(i);
                onBackPressed();
            }
        });


        img_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtSearch.setText("");
            }
        });



        searchList = new ArrayList<>();
        searchModelList = new ArrayList<>();
//
        searchAdapter = new SearchAdapter(this,searchModelList,listOfFavriote,favoriteArray);


        //   productAdapter = new ProductAdapter(productModel);
        manager = new GridLayoutManager(this, numberOfColumns);

        recyclerView.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        recyclerView.setHasFixedSize(true);

        //  recyclerView.setAdapter(productAdapter);
        recyclerView.setLayoutManager(manager);


        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    searchText = edtSearch.getText().toString().trim();
                    searchList.add(searchText);

                    adapterSearch = new ArrayAdapter<String>(SearchActivity.this,android.R.layout.simple_spinner_dropdown_item, loadArray());
                    //Find TextView control
                    adapterSearch.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);
                    //Set the number of characters the user must type before the drop down list is shown
                    edtSearch.setThreshold(1);
                    //Set the adapter
                    edtSearch.setAdapter(adapterSearch);

                    saveArrayList();
                    Log.i("List_Search", String.valueOf(searchList));

                    InputMethodManager imm = (InputMethodManager) SearchActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                    firstLoad(searchText);
                    return true;
                }
                return false;
            }
        });

        edtSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                adapterSearch = new ArrayAdapter<String>(SearchActivity.this, android.R.layout.simple_spinner_dropdown_item, loadArray());
                //Find TextView control
                adapterSearch.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                //Set the number of characters the user must type before the drop down list is shown
                edtSearch.setThreshold(1);
                //Set the adapter
                edtSearch.setAdapter(adapterSearch);
                return false;
            }
        }) ;

        clear_history_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(searchList == null){

                }else {
                    showAlertDialog();
                }


            }
        });



    }

    private void firstLoad(String searchText) {
        String url = "https://govava.yesitlabs.com/api/product.php?access=true&action=get_product_list_bycat&cat_name="+searchText+"&start=0&end=100" ;

//        isScrolling = false; // lock this guy,(isScrolling) to make sure,
        // user will not load more when volley is processing another request
        // only load more when  volley is free

        final ProgressDialog progressDialog = new ProgressDialog(SearchActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,



                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressDialog.dismiss();
                        // remember here we are in the main thread, that means,
                        //volley has finished processing request, and we have our response.
                        // What else are you waiting for? update isScrolling = true;
//                        isScrolling = true;

                        //   progressBar.setVisibility(View.GONE);

                        try {

                            JSONObject obj = new JSONObject(response);
                            searchModelList = new ArrayList<>();


                            Log.i("RESPONSE",response);

                            String status = obj.optString("status");

                            if(status.equalsIgnoreCase("true"))
                            {
                                String totalPage = obj.optString("Total_page");


                                JSONArray dataList = obj.getJSONArray("data");
                                for (int i = 0; i < dataList.length(); i++) {
                                    JSONObject jsonObject = dataList.getJSONObject(i);
                                    String prodocut_title = jsonObject.optString("prodocut_title");
                                    String prodocut_description = jsonObject.optString("prodocut_description");
                                    String prodouct_image = jsonObject.optString("prodouct_image");
                                    String prodocut_price = jsonObject.optString("prodocut_price");
                                    String review = jsonObject.optString("review");
                                    String market_type = jsonObject.optString("market_type");
                                    String product_id = jsonObject.optString("product_id");

                                    searchModelList.add(new SearchModel(prodocut_title, prodocut_description, prodouct_image, prodocut_price, review, market_type, product_id));
                                    ViewCompat.setNestedScrollingEnabled(recyclerView, false);

                                }
                                favoriteArray = obj.optJSONArray("favourite");

                                searchAdapter = new SearchAdapter(SearchActivity.this, searchModelList, listOfFavriote,favoriteArray);

                                recyclerView.setAdapter(searchAdapter);
                                searchAdapter.notifyDataSetChanged();
                                isScrolling = true;
                                progressDialog.dismiss();

                            }
                            else {
                                Toast.makeText(SearchActivity.this,obj.optString("displayMessage"),Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //    Toast.makeText(SearchActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                        isScrolling = true;
                        progressDialog.dismiss();
                        Toast.makeText(SearchActivity.this, "Unable to process this request Please try again later", Toast.LENGTH_SHORT).show();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();




                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(SearchActivity.this);
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }
    public void saveArrayList(){
        prefs = SearchActivity.this.getSharedPreferences("searchPrefsKey",Context.MODE_PRIVATE);
        SharedPreferences.Editor edit=prefs.edit();

        Set<String> set = new HashSet<String>();
        set.addAll(searchList);
        edit.putStringSet("searchKey", set);
        edit.commit();    // This line is IMPORTANT !!!
    }

    public List<String> loadArray() {
        prefs = SearchActivity.this.getSharedPreferences("searchPrefsKey",Context.MODE_PRIVATE);
        Set<String> set = prefs.getStringSet("searchKey", null);
        assert set != null;
        if(set==(null))
        {
            Log.i("Check","Check Invoked");
        }
        else
        {
            sample = new ArrayList<String>(set);
            Log.i("Save Search", String.valueOf(sample));
        }


        return sample;
    }


    public void showAlertDialog() {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(SearchActivity.this);
        builder1.setMessage("Are you sure you want to clear search history");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SharedPreferences preferences = SearchActivity.this.getSharedPreferences("searchPrefsKey", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear()
                                .apply();

                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}
