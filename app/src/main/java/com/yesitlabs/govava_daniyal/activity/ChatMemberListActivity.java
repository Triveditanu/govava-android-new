package com.yesitlabs.govava_daniyal.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.URLS.Url_List;
import com.yesitlabs.govava_daniyal.adapter.ChatMemberListAdapter;
import com.yesitlabs.govava_daniyal.model.ChatListModel;
import com.yesitlabs.govava_daniyal.model.ChatMemberListModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChatMemberListActivity extends BaseActivity {


    private ChatMemberListAdapter adapter;
    private List<ChatListModel> movieList = new ArrayList<>();
    private  List<ChatMemberListModel>chat_member_list;
    private String userid = "";
    private RecyclerView recyclerView;
    private String group_id = "";
    private String product_img = "",prodocut_title = "",product_price = "";
    private ImageView product_Img;
    private TextView product_name;
    private TextView txt_price;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_list_fragment);

        chat_member_list = new ArrayList<>();


        SharedPreferences prefs = getSharedPreferences("LoginDetail", Context.MODE_PRIVATE);
        userid = prefs.getString("userid", "");


        product_Img = findViewById(R.id.product_img);
        product_name = findViewById(R.id.product_name);
        txt_price = findViewById(R.id.txt_price);

        Intent  intent = getIntent();
        if (intent != null) {
            group_id = getIntent().getStringExtra("group_id");

            member_list_by_group(group_id);

        }


        try
        {
            prodocut_title = getIntent().getStringExtra("prodocut_title");
            product_name.setText(prodocut_title);

            Log.i("prodocut_title",prodocut_title);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


        try
        {
            product_img = getIntent().getStringExtra("product_img");

            Glide.with(ChatMemberListActivity.this).load(product_img).into(product_Img);


            Log.i("product_img",product_img);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        try
        {
            product_price = getIntent().getStringExtra("product_price");
            txt_price.setText(product_price);


            Log.i("product_price",product_price);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


        recyclerView = findViewById(R.id.chat_rv);


//        adapter = new ChatListAdapter(movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());




    }

    private void member_list_by_group( String group_id) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.URL_SIGNUP,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //   progressBar.setVisibility(View.GONE);

                        try {
                            Gson gson = new Gson();
                            JSONObject obj = new JSONObject(response);


                            Log.i("Group List RESPONSE",response);

                            String status = obj.optString("status");
                            String message = obj.optString("message");



                            if(status.equalsIgnoreCase("1"))
                            {

                                JSONArray dataList = obj.getJSONArray("data");
                                for (int i = 0; i < dataList.length(); i++) {
                                    JSONObject jsonObject = dataList.getJSONObject(i);
                                    String gmember = jsonObject.optString("gmember");
                                    String member_phone = jsonObject.optString("member_phone");
                                    String group_id = jsonObject.optString("group_id");
                                    String name = jsonObject.optString("name");






                                    chat_member_list.add(new ChatMemberListModel(gmember, member_phone,group_id,name));



//                                ImageArrayList.add("https://govava.yesitlabs.com/"+image);
//                                nameArrayList.add(name);


                                }
                                adapter = new ChatMemberListAdapter(chat_member_list,getApplicationContext());
                                recyclerView.setAdapter(adapter);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ChatMemberListActivity.this, "Unable to process this request Please try again later", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "get_userlist_bygroup");
                params.put("group_id", group_id);



                return params;
            }
        };


        RequestQueue queue = Volley.newRequestQueue(ChatMemberListActivity.this);
        stringRequest.setShouldCache(false);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                300000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }

}
