package com.yesitlabs.govava_daniyal.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.URLS.Url_List;
import com.yesitlabs.govava_daniyal.Utils.Constants;
import com.yesitlabs.govava_daniyal.Utils.FilePath;
import com.yesitlabs.govava_daniyal.Utils.MarshMallowPermissionUtils;
import com.yesitlabs.govava_daniyal.Utils.Utils;
import com.yesitlabs.govava_daniyal.Utils.VolleyMultipartRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.yesitlabs.govava_daniyal.Utils.Constants.emailPattern;
import static com.yesitlabs.govava_daniyal.Utils.Constants.isValidPassword;

public class SignUPActivity extends BaseActivity {

    private EditText et_userName;
    private EditText edtEmail;
    private EditText editText3;
    private TextView txtDob;
    private EditText editText5;
    private EditText editText6;
    private Button button2;
    private String username = "";
    private String email = "";
    private String password = "";
    private String txtdob = "";
    private String mobile = "";
    private static final int STORAGE_PERMISSION_CODE = 123;
    private static final int CAMERA_PERMISSION_REQUEST_CODE = 3;
    //Bitmap to get image from gallery
    private Bitmap bitmap;
    int CAMERA_REQUEST_CODE = 101;
    int GALLERY_REQUEST_CODE = 201;
    private Uri filePath;
    private AlertDialog.Builder builder;
    private Bitmap selectedThumbBitmap;
    private String imageStr = "";
    private  String uploadId = "",address ="";
    String path = "";
    private Uri uri;
    private ImageView img_profile;
    private String userid = "";
    private CircleImageView profile_img;
    ProgressDialog progressDialog;
    private ImageView img_cam;
    String MobilePattern = "[0-9]{10}";
    String messageRegister = "";
     Calendar myCalendar;
    private int year;
    private int month;
    private int day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_signup);


         myCalendar = Calendar.getInstance();

        button2 = findViewById(R.id.button2);
        et_userName =findViewById(R.id.et_userName);
        edtEmail =findViewById(R.id.edtEmail);
        editText3 =findViewById(R.id.editText3);
        txtDob =findViewById(R.id.txtDob);
        editText5 =findViewById(R.id.editText5);
        editText6 =findViewById(R.id.editText6);

        img_cam = findViewById(R.id.img_cam);

        edtEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                edtEmail.setSelection(0);
            }
        });



        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };


        txtDob.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(SignUPActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        edtEmail .addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                if (!edtEmail.getText().toString().matches(emailPattern) && s.length() > 0)
                {
                    edtEmail.setError("invalid email");
                }
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // other stuffs
            }
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // other stuffs
            }
        });


        profile_img = findViewById(R.id.img_profile);
        requestStoragePermission();
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(et_userName.length()==0)
                {
                    et_userName.setError("Field Can't be Empty.");
                    et_userName.requestFocus();
                    return;
                }

                if(edtEmail.length()==0)
                {
//                    edtEmail.setError("Field Can't be Empty.");
//                    edtEmail.requestFocus();
                    email = " ";
                }
                if(edtEmail.getText().toString().trim().isEmpty())
                {
                    email = " ";

                }
                else
                {
                    email = edtEmail.getText().toString();
                }

                if(editText3.length()==0)
                {
                    editText3.setError("Field Can't be Empty.");
                    editText3.requestFocus();

                    return;
                }

                if(editText3.getText().toString().trim().isEmpty())
                {
                    editText3.setError("Space Not Allowed");
                    editText3.requestFocus();

                    return;
                }
                if (editText3.length()<8)
                {
                    editText3.setError("Password must be of at least 8 character");

                    return;
                }

                if (!isValidPassword(editText3.getText().toString().trim())) {
                    editText3.setError("Password should contain at least 1 digit, 1 special character, 1 upper case, and minimum 8 character");

                    return;
                }

                if(txtDob.getText().toString().matches(""))
                {
                    // not null not empty
                    Constants.showDialog(getResources().getDrawable(R.drawable.ic_warning_black_24dp),SignUPActivity.this,
                            "Select Date Of Birth to proceed");
                    return;
                }
//
//                if(editText4.getText().toString().trim().isEmpty())
//                {
//                    editText4.setError("Space Not Allowed");
//                    editText4.requestFocus();
//
//                    return;
//                }

//                if (editText4.length()<8)
//                {
//                    editText4.setError("Password must be of at least 8 character");
//                    return;
//                }
//
//                if (!isValidPassword(editText4.getText().toString().trim())) {
//                    editText4.setError("Password should contain at least 1 digit, 1 special character, 1 upper case, and minimum 8 character");
//
//                    return;
//                }

//                if(!editText3.getText().toString().equals(editText4.getText().toString()))
//                {
//                    editText3.setError("Password Mismatch");
//                    editText4.setError("Password Mismatch");
//                    Log.i("Passowrd",editText3.getText().toString());
//                    Log.i("Confirm password",editText4.getText().toString());
//                    return;
//                }

                if(editText6.length()<10){
                 editText6.setError("Mobile number should be 10 digit ");
                    editText6.requestFocus();

                }

                else {
                    username = et_userName.getText().toString().trim();
                    password = editText3.getText().toString().trim();
                    txtdob = txtDob.getText().toString().trim();
                    mobile = editText6.getText().toString().trim();
                    address = editText5.getText().toString().trim();

                    if(selectedThumbBitmap != null) {

                        register(username, password, email, mobile,address,txtdob);
                    }else {


                        Constants.showDialog(getResources().getDrawable(R.drawable.ic_warning_black_24dp),SignUPActivity.this,
                                "Select image to proceed");

//                        android.support.v7.app.AlertDialog.Builder builder1 = new android.support.v7.app.AlertDialog.Builder(SignUPActivity.this);
//                        builder1.setMessage("Please select profile picture first");
//                        builder1.setCancelable(true);
//
//                        builder1.setPositiveButton(
//                                "Ok",
//                                new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.cancel();
//                                    }
//                                });
//
//                        android.support.v7.app.AlertDialog alert11 = builder1.create();
//                        alert11.show();
                    }
                }
            }
        });


        img_cam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectThumbLayout();
            }
        });

        profile_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectThumbLayout();
            }
        });

    }



    private void register(final String username, final String password, final String email, final String mobile,final String address,final String txtdob) {
        progressDialog = new ProgressDialog(SignUPActivity.this);
        progressDialog.setMessage("Registration is in progress..."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.URL_SIGNUP,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        try {
                            Gson gson = new Gson();
                            JSONObject obj = new JSONObject(response);


                            Log.i("Register RESPONSE",response);

                            String status = obj.optString("status");
                             messageRegister = obj.optString("message");
                            String userid = obj.optString("userid");

                            if (status.contains("1")) {

                                uploadBitmap(selectedThumbBitmap,userid);


                            } else {
                                android.support.v7.app.AlertDialog.Builder builder1 = new android.support.v7.app.AlertDialog.Builder(SignUPActivity.this);
                                builder1.setMessage(messageRegister);
                                builder1.setCancelable(false);

                                builder1.setPositiveButton(
                                        "Ok",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                                progressDialog.dismiss();
                                            }
                                        });

                                android.support.v7.app.AlertDialog alert11 = builder1.create();
                                alert11.show();


                                //     Toast.makeText(getApplicationContext(),displayMessage,Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Unable to process this request, please try again later", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "signup");
                params.put("email", email);
                params.put("password", password);
                params.put("mobile", mobile);
                params.put("address", address);
                params.put("username", username);
                params.put("dob", txtdob);

                return params;
            }
        };


        RequestQueue queue = Volley.newRequestQueue(SignUPActivity.this);
        stringRequest.setShouldCache(false);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                300000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }


    //Requesting permission
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED){
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        if (ActivityCompat.shouldShowRequestPermissionRationale(SignUPActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        ActivityCompat.requestPermissions(SignUPActivity.this, new String[] {Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST_CODE);
        //And finally ask for the permission
        ActivityCompat.requestPermissions(SignUPActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }

    public void selectThumbLayout() {


        builder = new AlertDialog.Builder(this);

        View view = LayoutInflater.from(this).inflate(R.layout.popup_select_thumb_layout, null);
        builder.setView(view);
        final AlertDialog dialog = builder.show();

        view.findViewById(R.id.cameraButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeImageFromCamera();
                dialog.dismiss();
            }
        });

        view.findViewById(R.id.galleryButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImageFromGallery();
                dialog.dismiss();
            }
        });
    }

    private void takeImageFromCamera() {

        if (MarshMallowPermissionUtils.checkPermissionForCamera(SignUPActivity.this)) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, CAMERA_REQUEST_CODE);
        } else {
            MarshMallowPermissionUtils.requestPermissionForCamera(SignUPActivity.this);
        }
    }

    private void pickImageFromGallery() {

        if (MarshMallowPermissionUtils.checkPermissionToReadExternalStorage(this)) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(intent, GALLERY_REQUEST_CODE);
        } else {
            MarshMallowPermissionUtils.requestPermissionToReadExternalStorage(this);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MarshMallowPermissionUtils.CAMERA_PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.

                if (requestCode == MarshMallowPermissionUtils.CAMERA_PERMISSION_REQUEST_CODE) {

                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

//                        Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();

                    } else {

//                        Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();

                    }

                }}

//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    takeImageFromCamera();
//                } else {
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//                    openAlertDialog(R.string.camera_access_denied, R.string.camera_denied_marsh);
//                }
                return;


            case MarshMallowPermissionUtils.EXTERNAL_STORAGE_READ_PERMISSION_REQUEST_CODE: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickImageFromGallery();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    openAlertDialog(R.string.storage_access_denied, R.string.storage_denied_marsh);
                }
            }
        }

        switch (requestCode) {

            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //                    Toast.makeText(UploadDocumentsActivity.this, "Permission Granted", Toast.LENGTH_SHORT).show();
                } else {
//                    Toast.makeText(this, "Permission Canceled", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    private void openAlertDialog(final int title, int message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton("Ok", null);
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_REQUEST_CODE) {

                if (data == null) {
                    Toast.makeText(this, R.string.unable_to_fetch_data, Toast.LENGTH_SHORT).show();
                    return;
                }
                selectedThumbBitmap = (Bitmap) data.getExtras().get("data");



                 //   uploadBitmap(selectedThumbBitmap);
                    addView(selectedThumbBitmap);



                if (imageStr.equals("")) {
                    imageStr = Utils.getStringImage(selectedThumbBitmap);
                } else {
                    imageStr = imageStr + Utils.getStringImage(selectedThumbBitmap);
                }

            } else if (requestCode == GALLERY_REQUEST_CODE) {

                if (data == null) {
                    Toast.makeText(this, R.string.unable_to_fetch_data, Toast.LENGTH_SHORT).show();
                    return;
                }

                try {
                    InputStream inputStream = getContentResolver().openInputStream(data.getData());

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeStream(inputStream, null, options);

                    Log.d("Actual size", "" + options.outWidth + "   " + options.outHeight);

                    if ((options.outWidth == -1) || (options.outHeight == -1)) {
                        Toast.makeText(getApplicationContext(), R.string.unable_to_fetch_data, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    int requiredSize = Utils.dpToPx(this, 100);
                    if (options.outWidth > requiredSize || options.outHeight > requiredSize) {
                        options.inSampleSize = (options.outHeight > options.outWidth) ? (options.outHeight / requiredSize) : (options.outWidth / requiredSize);
                    }

                    options.inJustDecodeBounds = false;

                    inputStream = getContentResolver().openInputStream(data.getData());


                    selectedThumbBitmap = BitmapFactory.decodeStream(inputStream, null, options);


                      //  uploadBitmap(selectedThumbBitmap);
                        addView(selectedThumbBitmap);


                    if (imageStr.equals("")) {
                        imageStr = Utils.getStringImage(selectedThumbBitmap);
                    } else {
                        imageStr = imageStr + Utils.getStringImage(selectedThumbBitmap);
                    }


                    Log.d("After scaling", "" + selectedThumbBitmap.getWidth() + "   " + selectedThumbBitmap.getHeight());

                } catch (FileNotFoundException e) {
                    Toast.makeText(getApplicationContext(), R.string.unable_to_fetch_data, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }
//        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {
//
//            //getting the image Uri
//            Uri imageUri = data.getData();
//            try {
//                //getting bitmap object from uri
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), imageUri);
//
//                //displaying selected image to imageview
//                img_set2.setImageBitmap(bitmap);
//
//                //calling the method uploadBitmap to upload image
//                uploadBitmap(bitmap);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }

        if (requestCode == 501 && resultCode == RESULT_OK && data != null && data.getData() != null) {

            uri = data.getData();

            String filePath = FilePath.getPath(this, uri);

            if (filePath == null) {
                Toast.makeText(getApplicationContext(), "Please move your PDF file to internal storage & try again.", Toast.LENGTH_LONG).show();
            } else {

                File file = new File(filePath);

                if (!(file.getName().endsWith(".txt") || file.getName().endsWith(".pdf") || file.getName().endsWith(".docx") || file.getName().endsWith(".jpeg") || file.getName().endsWith(".jpg") || file.getName().endsWith(".png"))) {
                    Toast.makeText(getApplicationContext(), "Only txt, pdf and docx allowed !", Toast.LENGTH_LONG).show();
                    return;
                }

                Toast.makeText(getApplicationContext(), "File selected !", Toast.LENGTH_SHORT).show();
                /* try {
                 *//* documentStr = getBase64String(file);
                    userIdProof_ext = file.getName().substring(file.getName().lastIndexOf(".") + 1);
                    if (documentStr.equals("")) {
                        documentStr = documentStr + "." + userIdProof_ext;
                    } else {
                        documentStr = documentStr + "," + documentStr + "." + userIdProof_ext;
                    }
                    addDocumentContent(file.getName());*//*

                } catch (IOException e) {
                    e.printStackTrace();
                }*/
            }
        }


    }


    public void addView(Bitmap selectedThumbBitmap) {


           profile_img.setImageBitmap(selectedThumbBitmap);


    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }





    private void uploadBitmap(final Bitmap bitmap,final String userid) {


        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, Url_List.URL_LOGIN,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {



                        String jsonString = "";
                        try {
                            jsonString = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers));
                            Log.i("SIGNUP RESPONSE", String.valueOf(jsonString));
                            try {
                                JSONObject obj = new JSONObject(jsonString);
                                String status = obj.optString("status");
                                String data = obj.optString("data");

                                if(status.contains("1")){

                                    Toast.makeText(getApplicationContext(), messageRegister, Toast.LENGTH_SHORT).show();
//                                    Intent i1 = new Intent(SignUPActivity.this, LoginActivity.class);
//                                                    startActivity(i1);
//                                                    finish();

                                    otpAlertBox(SignUPActivity.this);

                                }
                                progressDialog.dismiss();


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


//                            JSONObject obj = new JSONObject(new String(response.data));
//
//
//                            Intent i1 = new Intent(SignUPActivity.this, LoginActivity.class);
//                            startActivity(i1);

                        //  Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Unable to process this request,Please try again later", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "update_userimage");
                params.put("userid", userid);
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("photo_id", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));
                return params;
            }
        };

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        //adding the request to volley
        Volley.newRequestQueue(getApplicationContext()).add(volleyMultipartRequest);
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    private void updateLabel() {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        txtDob.setText(formatter.format(myCalendar.getTime()));
    }


    public void otpAlertBox(Context context){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.otp_alert_dialog);

        EditText edt_otp = (EditText) dialog.findViewById(R.id.edt_otp);
        String otp = edt_otp.getText().toString().trim();


        Button btn_dialogYes = (Button) dialog.findViewById(R.id.btn_dialogYes);
        btn_dialogYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(edt_otp.length()==0)
                {
                    edt_otp.setError("Field Can't be Empty.");
                    edt_otp.requestFocus();

                    return;
                }

                if(edt_otp.getText().toString().trim().isEmpty())
                {
                    edt_otp.setError("Space Not Allowed");
                    edt_otp.requestFocus();

                    return;
                }
                if (edt_otp.length()<5)
                {
                    edt_otp.setError("Password must be of at least 5 character");

                    return;
                }

                otpVerify(otp);


            }
        });


        Button btn_dialogNo = (Button) dialog.findViewById(R.id.btn_dialogNo);
        btn_dialogNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }


    private void otpVerify(String otp) {
        progressDialog = new ProgressDialog(SignUPActivity.this);
        progressDialog.setMessage("Registration is in progress..."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.URL_SIGNUP,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        try {
                            Gson gson = new Gson();
                            JSONObject obj = new JSONObject(response);


                            Log.i("Register RESPONSE",response);

                            String status = obj.optString("status");
                            messageRegister = obj.optString("message");


                            if (status.contains("1")) {

                                Intent intent = new Intent(SignUPActivity.this, EditProfileActivity.class);
                                startActivity(intent);


                            } else {
                                android.support.v7.app.AlertDialog.Builder builder1 = new android.support.v7.app.AlertDialog.Builder(SignUPActivity.this);
                                builder1.setMessage(messageRegister);
                                builder1.setCancelable(false);

                                builder1.setPositiveButton(
                                        "Ok",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });

                                android.support.v7.app.AlertDialog alert11 = builder1.create();
                                alert11.show();


                                //     Toast.makeText(getApplicationContext(),displayMessage,Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Unable to process this request, please try again later", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "varify_mobile");
                params.put("post_otp", otp);

                return params;
            }
        };


        RequestQueue queue = Volley.newRequestQueue(SignUPActivity.this);
        stringRequest.setShouldCache(false);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                300000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }


}
