package com.yesitlabs.govava_daniyal.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;
import com.twitter.sdk.android.core.services.AccountService;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.URLS.Url_List;
import com.yesitlabs.govava_daniyal.Utils.Constants;
import com.yesitlabs.govava_daniyal.Utils.UserSessionManager;

import net.londatiga.android.instagram.Instagram;
import net.londatiga.android.instagram.InstagramSession;
import net.londatiga.android.instagram.InstagramUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;

public class LoginActivity extends BaseActivity {

    private Button login_btn;
    private TextView register_btn;
    private TextView txt_forgot;
    private EditText et_userName;
    private EditText editText2;
    private String email = "";
    private String password = "";
    private SharedPreferences sp;
    private String emailStr = "";
    private String passwordStr = "";
    private CallbackManager callbackManager;
    private LoginButton loginButton;
    private ImageView imgFacebook;
    private Profile profile;
    private String userIdFb, firstName,lastName;
    private static String TAG = "LOGIN_ACTIVITY";
    private GoogleSignInClient mGoogleSignInClient;
    private int RC_SIGN_IN = 1;
    private int Twitter_Result = 2;
    private Button btnConnect, btnViewInfo, btnGetAllImages;
    private LinearLayout llAfterLoginView;


    private InstagramSession mInstagramSession;
    private Instagram mInstagram;

    private ProgressBar mLoadingPb;


    private static final String CLIENT_ID = "7e72c2c02d4b4c1e94a3c0876b91ebf5";
    private static final String CLIENT_SECRET = "7bb1a8b89f1c49c18e8d35be16ed5e85";
    private static final String REDIRECT_URI = "http://www.yesitlabs.com";
    private ImageView img_twitter;
    private TwitterLoginButton twitterLoginButton;
    private TwitterAuthClient authClient;
    private String userEmail = "";
    private String userName = "";
    private String userId = "";
    private String oauth_provider = "";
    private String providerName = "custom";
    private FirebaseAuth mAuth;
    private UserSessionManager session;
    ProgressDialog progressDialog;
    private String displayMessage = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        session = new UserSessionManager(this);
        Twitter.initialize(this);

        mInstagram          = new Instagram(this, CLIENT_ID, CLIENT_SECRET, REDIRECT_URI);

        mInstagramSession   = mInstagram.getSession();


        ((ImageView) findViewById(R.id.instagram_img)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                mInstagram.authorize(mAuthListener);
            }
        });

        if (getIntent().getBooleanExtra("EXIT", false)) {
            finish();
            return;
        }

        try
        {
            if (getIntent().getBooleanExtra("LOGOUT", false)) {
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        img_twitter = (ImageView) findViewById(R.id.img_twitter);
        twitterLoginButton = (TwitterLoginButton) findViewById(R.id.default_twitter_login_button);

        img_twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                providerName = "Twitter";
                twitterLoginButton.performClick();
            }
        });

        twitterLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                // Do something with result, which provides a TwitterSession for making API calls
                Log.e("result", "result " + result);
                TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();
                AccountService accountService = twitterApiClient.getAccountService();
                Call<User> call = accountService.verifyCredentials(true, true, true);
                call.enqueue(new Callback<com.twitter.sdk.android.core.models.User>() {
                    @Override
                    public void success(Result<com.twitter.sdk.android.core.models.User> result) {
                        //here we go User details
                        Log.e("result", "result user " + result);
                        String imageUrl = result.data.profileImageUrl;
                        String email = result.data.email;
                        String userName = result.data.name;
                        System.out.println(imageUrl);
                        System.out.println(email);
                        System.out.println(userName);

                        providerName = "Twitter";
                        userId = String.valueOf(result.data.id);
                        userName= result.data.name;
                        social_login();


                    }
                    @Override
                    public void failure(TwitterException exception) {
                        exception.printStackTrace();
                    }
                });
            }
            @Override
            public void failure(TwitterException exception) {

                exception.printStackTrace();
            }
        });


            login_btn = (Button) findViewById(R.id.login_btn);
        imgFacebook = findViewById(R.id.imgFacebook);
        register_btn = (TextView) findViewById(R.id.register_btn);

        txt_forgot = findViewById(R.id.txtForget);




        et_userName =findViewById(R.id.et_userName);
        editText2 =findViewById(R.id.editText2);


        et_userName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                et_userName.setSelection(0);
            }
        });

        callbackManager = CallbackManager.Factory.create();

        loginButton = findViewById(R.id.login_button);
        loginButton.setHeight(100);
        loginButton.setTextColor(Color.WHITE);
        loginButton.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        loginButton.setCompoundDrawablePadding(0);

        imgFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                providerName = "Facebook";

                loginButton.performClick();
            }
        });
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
     //   updateUI(account);
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(et_userName.length()==0)
                {
                    et_userName.setError("Field's Cant be Empty.");
                    et_userName.requestFocus();
                    return;
                }
                if(et_userName.getText().toString().trim().isEmpty())
                {
                    et_userName.setError("Space Not Allowed");
                    et_userName.requestFocus();
                    return;
                }
//                if(!et_userName.getText().toString().matches(emailPattern))
//                {
//                    et_userName.setError("Invalid Email");
//                    et_userName.requestFocus();
//                    return;
//                }
                if(editText2.length()==0)
                {
                    editText2.setError("Field's Cant be Empty.");
                    editText2.requestFocus();
                    return;
                }

                if(editText2.getText().toString().trim().isEmpty())
                {
                    editText2.setError("Space Not Allowed");
                    editText2.requestFocus();
                    return;
                }
                if(editText2.length()<7)
                {
                    Constants.showDialog(getResources().getDrawable(R.mipmap.cross_gray),LoginActivity.this,"Password should be a combination of a Uppercase, lowercase, special character and a digit and must contain 8 characters.");
                    editText2.requestFocus();
                }

                else
                {


                 login(et_userName.getText().toString(),editText2.getText().toString());


                }


            }
        });


        sp = getSharedPreferences("RememberMe" , Context.MODE_PRIVATE);
        emailStr = sp.getString("email","");
        passwordStr = sp.getString("password","");

        password = editText2.getText().toString().trim();
        email = et_userName.getText().toString().trim();

        if((emailStr != null && !emailStr.isEmpty() && !emailStr.equals("null"))||(passwordStr != null && !passwordStr.isEmpty() && !passwordStr.equals("null"))){
            et_userName.setText(emailStr);
            editText2.setText(passwordStr);
        }




        register_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, SignUPActivity.class);
                startActivity(i);
            }
        });


        txt_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.this);
                alert.setTitle("Forgot Password");
                // this is set the view from XML inside AlertDialog
                alert.setView(R.layout.custom_forgot_password);
                // disallow cancel of AlertDialog on click of back button and outside touch
                alert.setCancelable(false);
                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Toast.makeText(getBaseContext(), "Cancel clicked", Toast.LENGTH_SHORT).show();
                    }
                });

                alert.setPositiveButton("Submit", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        String user = etUsername.getText().toString();
//                        String pass = etEmail.getText().toString();
                        //  Toast.makeText(getBaseContext(), "Username: " + user + " Email: " + pass, Toast.LENGTH_SHORT).show();
                    }
                });
                AlertDialog dialog = alert.create();
                dialog.show();
            }
        });



        Profile.getCurrentProfile();
        profile = Profile.getCurrentProfile();
        if (profile != null) {

            startActivity(new Intent(LoginActivity.this, HomeActivity.class));

        } else {

        }

        FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.e(TAG,object.toString());
                        Log.e(TAG,response.toString());
                        String AccessToken = loginResult.getAccessToken().getToken();
                        System.out.println("Access Token : "+AccessToken);

                        try {
                            providerName = "Facebook";
                                    userId = object.getString("id");
                            if(object.has("first_name"))
                                userName = object.getString("first_name");
                            if(object.has("last_name"))
                                lastName = object.getString("last_name");
                            if (object.has("email"))
                                email = object.getString("email");

                            Log.i("INFO", userId +"name"+userName);

                            social_login();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                //Here we put the requested fields to be returned from the JSONObject
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, first_name, last_name, email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException e) {
                e.printStackTrace();
            }
        };

        loginButton.setReadPermissions("email");


        loginButton.registerCallback(callbackManager, callback);





}

    private void login(final String email, final String password) {
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("Logging In..."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
         StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    //    progressDialog.dismiss();
                        try {
                            Gson gson = new Gson();
                            JSONObject obj = new JSONObject(response);


                            Log.i("Login RESPONSE",response);

                            String status = obj.optString("status");
                            displayMessage = obj.optString("displayMessage");


                            if (status.equals("1")) {

                                JSONObject data = obj.getJSONObject("data");
                                String id = data.optString("id");
                                String username = data.optString("username");
                                String avatar = data.optString("avatar");
                                String dob = data.optString("dob");

//                            sendMessage(id,username,avatar,dob);

                                sp = getSharedPreferences("LoginDetail", Context.MODE_PRIVATE);
                                SharedPreferences.Editor edit = sp.edit();
                                edit.putString("userid", id).commit();
                                edit.putString("username", username).commit();
                                edit.putString("avatar", avatar).commit();
                                edit.putString("dob", dob).commit();
                                edit.commit();
                                progressDialog.dismiss();

                                Toast.makeText(getApplicationContext(),displayMessage,Toast.LENGTH_SHORT).show();

                                Intent i1 = new Intent(LoginActivity.this, HomeActivity.class);
                                      startActivity(i1);
                            }
                            else {
                                progressDialog.dismiss();

                                Constants.showDialog(getResources().getDrawable(R.drawable.ic_warning_black_24dp), LoginActivity.this,
                                        displayMessage);

                            }

                        } catch (JSONException e) {
                            progressDialog.dismiss();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Unable to process this request Please try again later", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "login");
                params.put("mobile", email);
                params.put("password", password);

                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
        stringRequest.setShouldCache(false);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                300000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }


    public void linkedLogin(View view){
        LISessionManager.getInstance(getApplicationContext())
                .init(this, buildScope(), new AuthListener() {
                    @Override
                    public void onAuthSuccess() {

                        Toast.makeText(getApplicationContext(), "success" +
                                        LISessionManager
                                                .getInstance(getApplicationContext())
                                                .getSession().getAccessToken().toString(),
                                Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onAuthError(LIAuthError error) {

                        Toast.makeText(getApplicationContext(), "failed "
                                        + error.toString(),
                                Toast.LENGTH_LONG).show();
                    }
                }, true);
    }

    // set the permission to retrieve basic information of User's linkedIn account
    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.R_EMAILADDRESS);
    }

    public void gmail_login(View view){
        providerName="Gmail";
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(session.mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }



    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int responseCode, Intent intent) {

        if(providerName.equalsIgnoreCase("Facebook"))
        {
            super.onActivityResult(requestCode, responseCode, intent);
            callbackManager.onActivityResult(requestCode, responseCode, intent);
        }
        else if(providerName.equalsIgnoreCase("Gmail"))
        {
            if (requestCode == RC_SIGN_IN) {

                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(intent);
                handleSignInResult(result);

            } else {
                session.mGoogleApiClient.stopAutoManage(LoginActivity.this);
                session.mGoogleApiClient.disconnect();
            }
        }

        else if(providerName.equalsIgnoreCase("Twitter"))
        {
            if (authClient != null)
                authClient.onActivityResult(requestCode, responseCode, intent);

            // Pass the activity result to the login button.
            twitterLoginButton.onActivityResult(requestCode, responseCode, intent);

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            Log.e(TAG, "display name: " + acct.getDisplayName());
            Log.e(TAG, "display name: " + acct.getId());


            userId = acct.getId();
            //    userEmail = account.getEmail();
            userName = acct.getGivenName();

            providerName = "Gmail";

            Log.e(TAG, "Name: " + userName + ", email: " + userEmail);

            social_login();

        } else {
//                showToast("Failure");
        }
    }



    private void showToast(String text) {
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
    }

    private Instagram.InstagramAuthListener mAuthListener = new Instagram.InstagramAuthListener() {
        @Override
        public void onSuccess(InstagramUser user) {


            userId =user.id;
            userName = user.username;
            providerName = "Instagram";
            social_login();

        }

        @Override
        public void onError(String error) {
            showToast(error);
        }

        @Override
        public void onCancel() {

        }
    };


    private void social_login() {
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("Loging In..."); // Setting Message

        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        String url = "https://govava.yesitlabs.com/api/index.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //   progressBar.setVisibility(View.GONE);

                        try {
                            Gson gson = new Gson();
                            JSONObject obj = new JSONObject(response);


                            Log.i("RESPONSE",response);

                            String status = obj.optString("status");
                            JSONObject data = obj.getJSONObject("data");

                            String displayMessage = obj.optString("displayMessage");
                            String id = data.optString("id");
                            String username = data.optString("username");

                            sp = getSharedPreferences("LoginDetail", Context.MODE_PRIVATE);
                            SharedPreferences.Editor edit = sp.edit();
                            edit.putString("userid", id).commit();
                            edit.putString("providerName", providerName).commit();
                            edit.putString("username", username).commit();
                            edit.commit();

                            if (status.equals("1")) {
                                Intent i1 = new Intent(LoginActivity.this, HomeActivity.class);
                                startActivity(i1);
                                progressDialog.dismiss();
                            }
                            else {

                                if(providerName.equalsIgnoreCase("Gmail"))
                                {
                                    session.signOut();
                                }
                                if(providerName.equalsIgnoreCase("Facebook"))
                                {
                                    LoginManager.getInstance().logOut();
                                } if(providerName.equalsIgnoreCase("Twitter"))
                                {
                                    LoginManager.getInstance().logOut();
                                }
                                if(providerName.equalsIgnoreCase("Instagram"))
                                {
                                    LoginManager.getInstance().logOut();
                                }

                                Toast.makeText(getApplicationContext(),displayMessage,Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Unable to process this request, please try again later", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "social_login");
                params.put("username", userName);
                params.put("oauth_provider", providerName);
                params.put("oauth_uid", userId);



                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
        stringRequest.setShouldCache(false);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                300000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(session.mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.

            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        //    hideProgressDialog();

        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                 //   hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }


    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }


    private void sendMessage(String userid,String username,String avatar,String dob) {
        Log.d("sender", "Broadcasting message");
        Intent intent = new Intent("custom-event-name");
        // You can also include some extra data.
        intent.putExtra("userid", userid);
        intent.putExtra("username", username);
        intent.putExtra("avatar", avatar);
        intent.putExtra("dob", dob);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


}
