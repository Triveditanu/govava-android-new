package com.yesitlabs.govava_daniyal.activity;

import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.Utils.ScreenSlidePagerAdapter;
import com.yesitlabs.govava_daniyal.fragment.PersonStyleFragment;
import com.yesitlabs.govava_daniyal.fragment.WhatToBuyFRagment0;
import com.yesitlabs.govava_daniyal.fragment.WhatToBuyFragment;
import com.yesitlabs.govava_daniyal.fragment.WhatToBuyFragment2;
import com.yesitlabs.govava_daniyal.fragment.WhatToBuyFragment3;
import com.yesitlabs.govava_daniyal.fragment.WhatToBuyFragment4;

import java.util.ArrayList;

public class GiftSelectionActivity extends AppCompatActivity {

    private ViewPager mPager;


    public ArrayList<Fragment> fragmentArrayList;
    public WhatToBuyFragment whatToBuyFragment;
    public WhatToBuyFragment2 whatToBuyFragment2;
    public WhatToBuyFragment3 whatToBuyFragment3;
    public WhatToBuyFragment4 whatToBuyFragment4;
    public WhatToBuyFRagment0 whatToBuyFRagment0;
    public PersonStyleFragment personStyleFragment;
    public String s1 = "";
    public String s2 = "";
    public String s3_to = "";
    public String s3_from = "";
    public String s4 = "";

    private PagerAdapter mPagerAdapter;
    private String phoneNumber = "";
    private String phoneNames ="";
    private ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gift_selection);

        mPager = (ViewPager)findViewById(R.id.pager);

        imgBack = findViewById(R.id.img_back);

        try
        {
            phoneNames = getIntent().getStringExtra("Names");
            phoneNumber = getIntent().getStringExtra("Number");

            Log.i("phoneNames",phoneNames);
            Log.i("phoneNumber",phoneNumber);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        personStyleFragment = new PersonStyleFragment();
        whatToBuyFRagment0 = new WhatToBuyFRagment0();
        whatToBuyFragment = new WhatToBuyFragment();
        whatToBuyFragment2 = new WhatToBuyFragment2();
        whatToBuyFragment3 = new WhatToBuyFragment3();
        whatToBuyFragment4 = new WhatToBuyFragment4();

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });



        mPager.setVisibility(View.VISIBLE);
        fragmentArrayList = new ArrayList<>();
        fragmentArrayList.add(personStyleFragment);
        fragmentArrayList.add(whatToBuyFRagment0);
        fragmentArrayList.add(whatToBuyFragment);
        fragmentArrayList.add(whatToBuyFragment2);
        fragmentArrayList.add(whatToBuyFragment3);
        fragmentArrayList.add(whatToBuyFragment4);

        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(),fragmentArrayList);
        mPager.setAdapter(mPagerAdapter);

    }
}
