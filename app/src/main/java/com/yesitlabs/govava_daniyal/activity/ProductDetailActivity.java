package com.yesitlabs.govava_daniyal.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.BulletSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.yesitlabs.govava_daniyal.Permissions.PermissionUtility;
import com.yesitlabs.govava_daniyal.Permissions.PermissionsUtils;
import com.yesitlabs.govava_daniyal.Permissions.RuntimePermission;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.URLS.Url_List;
import com.yesitlabs.govava_daniyal.fragment.FindGift;
import com.yesitlabs.govava_daniyal.fragment.HomeFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ProductDetailActivity extends AppCompatActivity {

    private TextView txt_need_frd;
    private RelativeLayout container_toolbar;
    private ConstraintLayout main_constraint;
    private TextView btn_buyNow;
    private LinearLayout mainLayout;
    public LinearLayout layout;
    public Snackbar snackbar;
    LinearLayout ll;
    // this is an array that holds the IDs of the drawables ...

    private View cell;
    private TextView text;
    private ImageView img_forward;
    private String thumb = "";
    private ImageView image ;
    private TextView txt;
    private String product_id = "";
    private String market_type = "";
    private String medium = "";

    private RuntimePermission runtimePermission;
    private String prodocut_title = "";
    private String prodocut_id = "",pro_image_default = "";
    private String product_price ="";
    private String large = "";
    private String prodocut_detail_url = "";
    private TextView txt_discription;
    private ImageView imageView;
    private View alertView;
    private TextView txt_price;
//    private RelativeLayout layout_need_help;
    private ScrollView scroll;

    private String name ="";
    private String phoneNumber = "";
    private SharedPreferences sp;

    private ImageView imgSearch,imgUser,imgBack;
    static final int REQUEST_SELECT_PHONE_NUMBER = 1;
    private FrameLayout frame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        scroll = findViewById(R.id.scroll);
        imgSearch = findViewById(R.id.imgSearch);
        imgUser = findViewById(R.id.imgUser);
        imgBack = findViewById(R.id.img_back);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        imgUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectContact();
            }
        });

        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductDetailActivity.this,SearchActivity.class);
                startActivity(intent);
            }
        });

        ll=(LinearLayout)findViewById(R.id.layout_table);

        try
        {
            product_id = getIntent().getStringExtra("product_id");
            market_type = getIntent().getStringExtra("market_type");
            product_price = getIntent().getStringExtra("product_price");

            Log.i("product_id",product_id);
            Log.i("market_type",market_type);
            Log.i("product_price",product_price);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        layout = findViewById(R.id.footerAd);
        runtimePermission = new RuntimePermission(ProductDetailActivity.this, layout);

        image = findViewById(R.id.image);

        mainLayout = findViewById(R.id._linearLayout);

        img_forward = findViewById(R.id.img_forward);
        txt = findViewById(R.id.txt);

        txt_discription =findViewById(R.id.txt_discription);

        main_constraint = (ConstraintLayout)findViewById(R.id.main_constraint);

        txt_need_frd = (TextView)findViewById(R.id.txt_need_frd);

        txt_price = findViewById(R.id.txt_price);

        btn_buyNow = (TextView)findViewById(R.id.btn_buyNow);
        frame = findViewById(R.id.frame);


        container_toolbar = (RelativeLayout) findViewById(R.id.container_toolbar);

        productDetail();

        txt_need_frd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (PermissionUtility.hasMarshmallow() && PermissionsUtils.checkSelfForContactPermission(ProductDetailActivity.this)) {
                    runtimePermission.requestContactPermission();
                } else {
                    Intent intentContactPick = new Intent(ProductDetailActivity.this,MultipleContactPickerActivity.class);

                    intentContactPick.putExtra("prodocut_title", txt.getText().toString());
                    intentContactPick.putExtra("productId", product_id);
                    intentContactPick.putExtra("product_img", thumb);
                    intentContactPick.putExtra("market_type", market_type);
                    intentContactPick.putExtra("product_price", product_price);

                    startActivityForResult(intentContactPick,1000);
                }

            }
        });

        btn_buyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentContactPick = new Intent(ProductDetailActivity.this,WebViewActivity.class);
                intentContactPick.putExtra("product_url", prodocut_detail_url);
                startActivity(intentContactPick);

                Log.i("prodocut_detail_url",prodocut_detail_url);
            }
        });




    }

    public void selectContact() {
        // Start an activity for the user to pick a phone number from contacts
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_SELECT_PHONE_NUMBER);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode == REQUEST_SELECT_PHONE_NUMBER && resultCode == RESULT_OK) {
            // Get the URI and query the content provider for the phone number
            Uri contactData = data.getData();

            Cursor cur =  getContentResolver().query(contactData, null, null, null, null);
            if (cur.getCount() > 0) {// thats mean some resutl has been found
                if(cur.moveToNext()) {
                    String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                    name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    phoneNumber = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    Log.e("Number", phoneNumber);
                    Log.e("Names", name);


                    sp = getSharedPreferences("PhoneNumberPre", Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit = sp.edit();
                    edit.putString("Number", phoneNumber);
                    edit.putString("Names", name);

                    edit.apply();

                    FindGift homeTabFrag = new FindGift();
                    Bundle args = new Bundle();
                    args.putString("Names", name);
                    args.putString("Number", phoneNumber);
                    homeTabFrag.setArguments(args);
                    FragmentTransaction fragTransaction1 = getSupportFragmentManager().beginTransaction();
                    fragTransaction1.replace(R.id.frame, homeTabFrag).addToBackStack("Gift Fragment");
                    fragTransaction1.commit();

                    scroll.setVisibility(View.GONE);
                    btn_buyNow.setVisibility(View.GONE);
                    frame.setVisibility(View.VISIBLE);
//                    if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0)
//                    {
//
//                        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ id,null, null);
//                        while (phones.moveToNext()) {
//                            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
//                            Log.e("Number", phoneNumber);
//                        }
//                        phones.close();
//                    }

                }
            }
            cur.close();
        }else {
            HomeFragment homeTabFrag = new HomeFragment();
            Bundle args1 = new Bundle();
            homeTabFrag.setArguments(args1);
            FragmentTransaction fragTransaction1 = getSupportFragmentManager().beginTransaction();
            fragTransaction1.replace(R.id.frame, homeTabFrag);
            fragTransaction1.commit();
        }

    }



    private void productDetail() {

        final ProgressDialog progressDialog = new ProgressDialog(ProductDetailActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Log.i("Market Buy Now",market_type);
        Log.i("Product Buy Now",product_id);

        String URL = Url_List.URL_PRODUCT_DETAIL+"?access=true&action=newget_product_detail";
// Post params to be sent to the server
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", product_id);

        JsonObjectRequest request_json = new JsonObjectRequest(URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        progressDialog.dismiss();
                        scroll.setVisibility(View.VISIBLE);
                        btn_buyNow.setVisibility(View.VISIBLE);

                        JSONObject obj = response;
                        String status = obj.optString("status");
                        if(status.equalsIgnoreCase("1"))
                        {
                            try {
                                JSONObject dataJson = obj.getJSONObject("data");


                                Log.i("ProductDetail",obj.toString());

                                prodocut_title = dataJson.optString("title");
                                prodocut_id = dataJson.optString("prodocut_id");
                                String prodocut_price =dataJson.optString("price");
                                prodocut_detail_url = dataJson.optString("url");
                                String prodocut_description = dataJson.optString("description");
                                String market_type = dataJson.optString("merchantName");
                                String productImage = dataJson.optString("imageUrl");
                                thumb = productImage;
                                if(!dataJson.isNull("title")) {

                                    txt.setText(prodocut_title);
                                }

                                if(!dataJson.isNull("price")) {

                                    txt_price.setText(prodocut_price);
                                }

                                if(!dataJson.isNull("description"))
                                {
//                                txt_discription.setText(prodocut_description);

                                    String s= ".";
                                    SpannableString ss1=  new SpannableString(s);
                                    ss1.setSpan(new RelativeSizeSpan(2f), 0,1, 0); // set size
                                    ss1.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 1, 0);// set color
                                    if(prodocut_description.contains("."))
                                    {
                                        String [] values_comma = prodocut_description.split("\\.");

                                        for(int i=0;i<values_comma.length;i++)
                                        {
                                            CharSequence cs = values_comma[i];

                                            SpannableString ss = new SpannableString(cs);
                                            // ss.setSpan(new RelativeSizeSpan(1.5f), 0, StreetType.length(), 0); // set size

                                            ss.setSpan(new BulletSpan(15), 0, cs.length(), 0);

                                            TextView tv = new TextView(ProductDetailActivity.this);
                                            if(i%2==0)
                                            {
                                                tv.setBackgroundColor(Color.parseColor("#F1E5CA"));
                                            }else
                                            {
                                                tv.setBackgroundColor(Color.parseColor("#EEC995"));
                                            }

                                            tv.setPadding(15, 15, 10, 15);
                                            tv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                                            tv.setText(" "+ss);
                                            ll.addView(tv);
                                        }
                                    }

                                    else
                                    {
                                        txt_discription.setVisibility(View.VISIBLE);
                                        txt_discription.setText(prodocut_description);

                                    }


                                }else {

                                    txt_discription.setText("");
                                }

                                Glide.with(ProductDetailActivity.this)
                                        .load(productImage)
                                        .thumbnail(0.5f)
                                        .into(image);


                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();

                            }
                        }
                        else
                        {
                            Toast.makeText(ProductDetailActivity.this, obj.optString("displayMessage"), Toast.LENGTH_SHORT).show();

                            progressDialog.dismiss();
                        }




                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                progressDialog.dismiss();

            }
        });

// add the request object to the queue to be executed
        RequestQueue queue = Volley.newRequestQueue(ProductDetailActivity.this);
        request_json.setShouldCache(false);

        request_json.setRetryPolicy(new DefaultRetryPolicy(
                300000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request_json);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == PermissionsUtils.REQUEST_CONTACT) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showSnackBar(R.string.contact_permission_granted);

                Intent intentContactPick = new Intent(ProductDetailActivity.this,MultipleContactPickerActivity.class);
                startActivityForResult(intentContactPick,1000);

            } else {
                showSnackBar(R.string.contact_permission_not_granted);
            }
        }
        else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void showSnackBar(int resId) {
        snackbar = Snackbar.make(layout, resId,
                Snackbar.LENGTH_SHORT);
        snackbar.show();
    }


}
