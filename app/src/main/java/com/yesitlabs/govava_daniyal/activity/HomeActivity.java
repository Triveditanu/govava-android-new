package com.yesitlabs.govava_daniyal.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.URLS.Url_List;
import com.yesitlabs.govava_daniyal.Utils.Constants;
import com.yesitlabs.govava_daniyal.fragment.AboutUsFragment;
import com.yesitlabs.govava_daniyal.fragment.CategoriesFragment;
import com.yesitlabs.govava_daniyal.fragment.ChatListFragment;
import com.yesitlabs.govava_daniyal.fragment.ContactUsFragment;
import com.yesitlabs.govava_daniyal.fragment.FavriotFragment;
import com.yesitlabs.govava_daniyal.fragment.FindGift;
import com.yesitlabs.govava_daniyal.fragment.HomeFragment;
import com.yesitlabs.govava_daniyal.fragment.PetsFragment;
import com.yesitlabs.govava_daniyal.fragment.SettingFragment;
import com.yesitlabs.govava_daniyal.fragment.TermsandConditionFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.yesitlabs.govava_daniyal.Utils.Constants.Profile_Image;

public class HomeActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout drawer;

    ActionBarDrawerToggle toggle;

    private TextView txtTitle,txt_username;

    private String avatar = "",username = "",email = "";
    private CircleImageView imgUserPic;
    static final int REQUEST_SELECT_PHONE_NUMBER = 1;
    private static final int STORAGE_PERMISSION_CODE = 123;
    private String name ="";
    private String phoneNumber = "";
    private SharedPreferences sp;
    private String dob = "";
    private NavigationView navigationView;
    private int Age;
    private String userid = "";
    private FrameLayout frame;


    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        txtTitle = findViewById(R.id.txtTitle);
        frame = findViewById(R.id.frame);




        requestStoragePermission();
        drawer = findViewById(R.id.drawer_layout);

        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("custom-event-name"));


        try {
            SharedPreferences prefs = getSharedPreferences("LoginDetail", Context.MODE_PRIVATE);
            avatar = prefs.getString("avatar", "");
            username = prefs.getString("username", "");
            userid = prefs.getString("userid", "");
            dob = prefs.getString("dob", "");

            if(prefs.getString("dob", "").equals("")){
                profileDetail();
            }
            if (dob == (null)||dob.equals("null")){
                profileDetail();


            }


        String formattedDate[] = dob.split("-");

         Age = Constants.getAge(Integer.valueOf(formattedDate[0]),Integer.valueOf(formattedDate[1]),Integer.valueOf(formattedDate[2]));

     //   Toast.makeText(this,"Age : "+String.valueOf(Age),Toast.LENGTH_SHORT).show();
        }catch (Exception e){

        }




        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        txtTitle.setText("HomeFragment");


        imgUserPic =navigationView.getHeaderView(0).findViewById(R.id.imgUserPic);

        txt_username = navigationView.getHeaderView(0).findViewById(R.id.txt_username);


        if( avatar == (null)||avatar.equalsIgnoreCase(""))
        {
            imgUserPic.setImageDrawable(HomeActivity.this.getResources().getDrawable(R.drawable.dummy_pic));

        } else {
            Glide.with(HomeActivity.this)
                    .load("https://govava.yesitlabs.com/assets/front/uploads/" + avatar)
                    .thumbnail(0.5f)
                    .into(imgUserPic);

        }


        if(username ==(null))
        {
            txt_username.setText("");

        } else {
            txt_username.setText(username);
        }

        imgUserPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawers();
                Intent intent = new Intent(HomeActivity.this, EditProfileActivity.class);
                startActivity(intent);

            }
        });

        HomeFragment homeTabFrag = new HomeFragment();
        Bundle args1 = new Bundle();
        homeTabFrag.setArguments(args1);
        FragmentTransaction fragTransaction1 = getSupportFragmentManager().beginTransaction();
        fragTransaction1.replace(R.id.frame, homeTabFrag);
        fragTransaction1.commit();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        }

        else if(txtTitle.getText().toString().equalsIgnoreCase("HomeFragment"))
        {
            new AlertDialog.Builder(this)
                    .setMessage("Are you sure you want to exit?")
                    .setCancelable(true)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("EXIT", true);
                            startActivity(intent);
                            dialog.dismiss();
                            finish();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        }
        else {
            super.onBackPressed();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.home, menu);

        return true;
    }



    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if(Build.VERSION.SDK_INT > 11) {
            invalidateOptionsMenu();
            if(Age<18){
                hideItem();
            }else {
                showItem();
            }
         //   menu.findItem(R.id.nav_fav).setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_search) {
            txtTitle.setText("Search");
//            SearchFragment homeTabFrag = new SearchFragment();
//            Bundle args1 = new Bundle();
//            homeTabFrag.setArguments(args1);
//            FragmentTransaction fragTransaction1 = getSupportFragmentManager().beginTransaction();
//            fragTransaction1.replace(R.id.frame, homeTabFrag).addToBackStack("Search");
//            fragTransaction1.commit();

            Intent intent = new Intent(HomeActivity.this,SearchActivity.class);
            startActivity(intent);

            return true;
        }
        if (id == R.id.action_gift) {
            txtTitle.setText("Gift");
            selectContact();
//            FindGift homeTabFrag = new FindGift();
//            Bundle args = new Bundle();
//            args.putString("Names", name);
//            args.putString("Number", phoneNumber);
//            homeTabFrag.setArguments(args);
//            FragmentTransaction fragTransaction1 = getSupportFragmentManager().beginTransaction();
//            fragTransaction1.replace(R.id.frame, homeTabFrag).addToBackStack("Gift Fragment");
//            fragTransaction1.commit();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();



        if (id == R.id.nav_Home) {
            txtTitle.setText("HomeFragment");

            HomeFragment homeTabFrag = new HomeFragment();
            Bundle args1 = new Bundle();
            homeTabFrag.setArguments(args1);
            FragmentTransaction fragTransaction1 = getSupportFragmentManager().beginTransaction();
            fragTransaction1.replace(R.id.frame, homeTabFrag).addToBackStack("Home");
            fragTransaction1.commit();

        } else if (id == R.id.nav_gift) {
            txtTitle.setText("Gift");

            selectContact();

//            FindGift homeTabFrag = new FindGift();
//            Bundle args1 = new Bundle();
//            homeTabFrag.setArguments(args1);
//            FragmentTransaction fragTransaction1 = getSupportFragmentManager().beginTransaction();
//            fragTransaction1.replace(R.id.frame, homeTabFrag).addToBackStack("Find");
//            fragTransaction1.commit();

        } else if (id == R.id.nav_cat) {
            txtTitle.setText("Category");

            CategoriesFragment homeTabFrag = new CategoriesFragment();
            Bundle args1 = new Bundle();
            homeTabFrag.setArguments(args1);
            FragmentTransaction fragTransaction1 = getSupportFragmentManager().beginTransaction();
            fragTransaction1.replace(R.id.frame, homeTabFrag).addToBackStack("Categories");
            fragTransaction1.commit();

        } else if (id == R.id.nav_pet) {
            txtTitle.setText("Pets");

            PetsFragment homeTabFrag = new PetsFragment();
            Bundle args1 = new Bundle();
            homeTabFrag.setArguments(args1);
            FragmentTransaction fragTransaction1 = getSupportFragmentManager().beginTransaction();
            fragTransaction1.replace(R.id.frame, homeTabFrag).addToBackStack("Pets");
            fragTransaction1.commit();

        } else if (id == R.id.nav_fav) {
            txtTitle.setText("Favorite");

            FavriotFragment homeTabFrag = new FavriotFragment();
            Bundle args1 = new Bundle();
            homeTabFrag.setArguments(args1);
            FragmentTransaction fragTransaction1 = getSupportFragmentManager().beginTransaction();
            fragTransaction1.replace(R.id.frame, homeTabFrag).addToBackStack("Favriot");
            fragTransaction1.commit();

        } else if (id == R.id.nav_wish) {
            txtTitle.setText("WishList");

            FavriotFragment homeTabFrag = new FavriotFragment();
            Bundle args1 = new Bundle();
            homeTabFrag.setArguments(args1);
            FragmentTransaction fragTransaction1 = getSupportFragmentManager().beginTransaction();
            fragTransaction1.replace(R.id.frame, homeTabFrag).addToBackStack("Favriot");
            fragTransaction1.commit();

        }


        else if (id == R.id.nav_chat) {
            txtTitle.setText("Chat");

            ChatListFragment homeTabFrag = new ChatListFragment();
            Bundle args1 = new Bundle();
            homeTabFrag.setArguments(args1);
            FragmentTransaction fragTransaction1 = getSupportFragmentManager().beginTransaction();
            fragTransaction1.replace(R.id.frame, homeTabFrag).addToBackStack("Chat");
            fragTransaction1.commit();

        } else if (id == R.id.nav_setting) {
            txtTitle.setText("Settings");

            SettingFragment homeTabFrag = new SettingFragment();
            Bundle args1 = new Bundle();
            homeTabFrag.setArguments(args1);
            FragmentTransaction fragTransaction1 = getSupportFragmentManager().beginTransaction();
            fragTransaction1.replace(R.id.frame, homeTabFrag).addToBackStack("Setting");
            fragTransaction1.commit();

        } else if (id == R.id.nav_contact) {
            txtTitle.setText("Contact");

            ContactUsFragment homeTabFrag = new ContactUsFragment();
            Bundle args1 = new Bundle();
            homeTabFrag.setArguments(args1);
            FragmentTransaction fragTransaction1 = getSupportFragmentManager().beginTransaction();
            fragTransaction1.replace(R.id.frame, homeTabFrag).addToBackStack("Contact");
            fragTransaction1.commit();

        } else if (id == R.id.nav_term) {
            txtTitle.setText("Terms");

            TermsandConditionFragment homeTabFrag = new TermsandConditionFragment();
            Bundle args1 = new Bundle();
            homeTabFrag.setArguments(args1);
            FragmentTransaction fragTransaction1 = getSupportFragmentManager().beginTransaction();
            fragTransaction1.replace(R.id.frame, homeTabFrag).addToBackStack("Terms");
            fragTransaction1.commit();
        } else if (id == R.id.nav_about) {
            txtTitle.setText("About");

            AboutUsFragment homeTabFrag = new AboutUsFragment();
            Bundle args1 = new Bundle();
            homeTabFrag.setArguments(args1);
            FragmentTransaction fragTransaction1 = getSupportFragmentManager().beginTransaction();
            fragTransaction1.replace(R.id.frame, homeTabFrag).addToBackStack("About");
            fragTransaction1.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(HomeActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }


    public void selectContact() {
        // Start an activity for the user to pick a phone number from contacts
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_SELECT_PHONE_NUMBER);

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode == REQUEST_SELECT_PHONE_NUMBER && resultCode == RESULT_OK) {
            // Get the URI and query the content provider for the phone number
            Uri contactData = data.getData();

            Cursor cur =  getContentResolver().query(contactData, null, null, null, null);
            if (cur.getCount() > 0) {// thats mean some resutl has been found
                if(cur.moveToNext()) {
                    String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                     name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                     phoneNumber = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    Log.e("Number", phoneNumber);
                    Log.e("Names", name);


                    sp = getSharedPreferences("PhoneNumberPre", Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit = sp.edit();
                    edit.putString("Number", phoneNumber).commit();
                    edit.putString("Names", name).commit();

                    edit.commit();

                    FindGift homeTabFrag = new FindGift();
                    Bundle args = new Bundle();
                    args.putString("Names", name);
                    args.putString("Number", phoneNumber);
                    homeTabFrag.setArguments(args);
                    FragmentTransaction fragTransaction1 = getSupportFragmentManager().beginTransaction();
                    fragTransaction1.replace(R.id.frame, homeTabFrag).addToBackStack("Gift Fragment");
                    fragTransaction1.commit();


//                    if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0)
//                    {
//
//                        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ id,null, null);
//                        while (phones.moveToNext()) {
//                            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
//                            Log.e("Number", phoneNumber);
//                        }
//                        phones.close();
//                    }

                }
            }
            cur.close();
        }else {
            HomeFragment homeTabFrag = new HomeFragment();
            Bundle args1 = new Bundle();
            homeTabFrag.setArguments(args1);
            FragmentTransaction fragTransaction1 = getSupportFragmentManager().beginTransaction();
            fragTransaction1.replace(R.id.frame, homeTabFrag);
            fragTransaction1.commit();
        }

    }


    private void hideItem()
    {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();

        nav_Menu.findItem(R.id.nav_fav).setVisible(false);
        nav_Menu.findItem(R.id.nav_wish).setVisible(true);
    }

    private void showItem()
    {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();

        nav_Menu.findItem(R.id.nav_fav).setVisible(true);
        nav_Menu.findItem(R.id.nav_wish).setVisible(false);
    }

    private void profileDetail() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //   progressBar.setVisibility(View.GONE);

                        try {
                            Gson gson = new Gson();
                            JSONObject obj = new JSONObject(response);


                            Log.i("RESPONSE",response);

                            String status = obj.optString("status");
                            String message = obj.optString("message");
                            JSONObject data = obj.getJSONObject("data");
                            String id = data.optString("id");
                            String username = data.optString("username");
                            String firstname = data.optString("firstname");
                            String email = data.optString("email");
                            String mobile = data.optString("mobile");
                            String avatar = data.optString("avatar");
                            String address = data.optString("address");
                            String dob = data.optString("dob");

                            txt_username.setText(username);

                            Glide.with(HomeActivity.this)
                                    .load(Profile_Image + avatar)
                                    .thumbnail(0.5f)
                                    .into(imgUserPic);

                            if(data.isNull("dob")) {

                                final Dialog dialog = new Dialog(HomeActivity.this);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setCancelable(false);
                                dialog.setContentView(R.layout.alert_box);

                                TextView text = (TextView) dialog.findViewById(R.id.txt_msg);


                                text.setText("Update your Profile");

                                Button btn_dialogYes = (Button) dialog.findViewById(R.id.btn_dialogYes);
                                btn_dialogYes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(HomeActivity.this, EditProfileActivity.class);
                                        startActivity(intent);
                                    }
                                });


                                Button btn_dialogNo = (Button) dialog.findViewById(R.id.btn_dialogNo);
                                btn_dialogNo.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                    }
                                });

                                try {
                                    dialog.show();
                                }
                                catch (Exception e)
                                {
                                    e.printStackTrace();
                                }


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Unable to process this request Please try again later", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "profile_byid");
                params.put("user_id", userid);


                return params;
            }
        };


        RequestQueue queue = Volley.newRequestQueue(HomeActivity.this);
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                200000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    @Override
    protected void onResume() {


        try {
            SharedPreferences prefs = getSharedPreferences("LoginDetail", Context.MODE_PRIVATE);
            avatar = prefs.getString("avatar", "");
            username = prefs.getString("username", "");
            userid = prefs.getString("userid", "");
            dob = prefs.getString("dob", "");
            profileDetail();

//            if(prefs.getString("dob", "").equals("")&&prefs.getString("dob", "").contains(null)){
//                profileDetail();
//            } else{
//
//            }


            String formattedDate[] = dob.split("-");

            Age = Constants.getAge(Integer.valueOf(formattedDate[0]),Integer.valueOf(formattedDate[1]),Integer.valueOf(formattedDate[2]));

            //   Toast.makeText(this,"Age : "+String.valueOf(Age),Toast.LENGTH_SHORT).show();
        }catch (Exception e){

        }
        super.onResume();
    }


    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
             username = intent.getStringExtra("username");
             email = intent.getStringExtra("email");
             avatar = intent.getStringExtra("avatar");
            dob = intent.getStringExtra("dob");
            Log.d("receiver", "Got message: " + avatar);



            if( avatar == (null)||avatar.equalsIgnoreCase(""))
            {
                imgUserPic.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.dummy_pic));

            } else {
                Glide.with(getApplicationContext())
                        .load(Profile_Image + avatar)
                        .thumbnail(0.5f)
                        .into(imgUserPic);

            }


            if(username ==(null))
            {
                txt_username.setText("");

            } else {
                txt_username.setText(username);
            }


            try {

                if(dob.equals("")){
                    profileDetail();
                }
                if (dob == (null)||dob.equals("null")){
                    profileDetail();
                }

                String formattedDate[] = dob.split("-");

                Age = Constants.getAge(Integer.valueOf(formattedDate[0]),Integer.valueOf(formattedDate[1]),Integer.valueOf(formattedDate[2]));

                //   Toast.makeText(this,"Age : "+String.valueOf(Age),Toast.LENGTH_SHORT).show();
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    };

}
