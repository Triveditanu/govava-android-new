package com.yesitlabs.govava_daniyal.activity;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.yesitlabs.govava_daniyal.ContactHelper.Contact;
import com.yesitlabs.govava_daniyal.Permissions.PermissionUtility;
import com.yesitlabs.govava_daniyal.Permissions.PermissionsUtils;
import com.yesitlabs.govava_daniyal.Permissions.RuntimePermission;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.URLS.Url_List;
import com.yesitlabs.govava_daniyal.adapter.ChatAdapter;
import com.yesitlabs.govava_daniyal.model.ChatModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import me.grantland.widget.AutofitTextView;

public class ChatActivity extends BaseActivity {

    private Uri uriContact;
    private long contactID;
    private ImageView CoverImage;
    private ImageView continueNext;
    private TextView ed_contact_name;
    private SharedPreferences.Editor sEditor;
    private String display_name= "";
    private String display_image ="";
    private String phone_no= "";
    private TextView txt_number;
    private LinearLayout layout_main;
    private String revisedContactName="",revisedContactNumber="";
    private String userid = "";
    private String prodocut_title = "";
    private String group_id = "";
    private AutofitTextView title_txt;
    private ImageView add_btn;
    private EditText edt_message;
    private ImageView send_msg_btn;
    private ArrayList<ChatModel> chatList;
    private RecyclerView chatrecyclerView;
    private NestedScrollView nest_scroll;
    private ChatAdapter chatAdapter;
    static int endPage;
    Boolean isScrolling = false;
    LinearLayoutManager manager;
    private String group_name = "";
    private String username = "",productId="",product_img = "",product_price = "",group_id_group = "";
    private String market_type = "";
    private String user_id = "";
    private String navigation = "";
    private String avatar = "";
    private RuntimePermission runtimePermission;
    public LinearLayout layout;


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);


     //   ed_contact_name = findViewById(R.id.txt_name);
    //    txt_number = findViewById(R.id.txt_numb);
        CoverImage = findViewById(R.id.imgUserPic);

        edt_message = findViewById(R.id.edt_message);

        send_msg_btn = findViewById(R.id.send_msg_btn);

        title_txt = findViewById(R.id.title_txt);

        add_btn = findViewById(R.id.add_btn);

        nest_scroll = findViewById(R.id.nest_scroll);
        chatrecyclerView = findViewById(R.id.chat_recycler);

        chatList = new ArrayList<>();
        chatAdapter = new ChatAdapter(ChatActivity.this,chatList);
        manager = new LinearLayoutManager(this);

        layout = findViewById(R.id.footerAd);

        runtimePermission = new RuntimePermission(ChatActivity.this, layout);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ChatActivity.this);
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        chatrecyclerView.setLayoutManager(mLayoutManager);

        chatrecyclerView.setHasFixedSize(true);

        //we can now set adapter to recyclerView;
        chatrecyclerView.setAdapter(chatAdapter);

        add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (PermissionUtility.hasMarshmallow() && PermissionsUtils.checkSelfForContactPermission(ChatActivity.this)) {
                    runtimePermission.requestContactPermission();
                } else {
                    Intent intentContactPick = new Intent(ChatActivity.this,MultipleContactPickerActivity.class);
                    intentContactPick.putExtra("prodocut_title",prodocut_title);
                    intentContactPick.putExtra("productId",productId);
                    intentContactPick.putExtra("add_member","add_member");
                    startActivityForResult(intentContactPick,1000);
                }

            }
        });



        SharedPreferences prefs = getSharedPreferences("LoginDetail", Context.MODE_PRIVATE);
        userid = prefs.getString("userid", "");
        username = prefs.getString("username", "");
        avatar = prefs.getString("avatar", "");


        try
        {
            revisedContactName = getIntent().getStringExtra("SelectedContactName");
            Log.i("Name",revisedContactName);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        try
        {
            revisedContactNumber = getIntent().getStringExtra("SelectedContactNumber");
            Log.i("Number",revisedContactNumber);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        try
        {
            prodocut_title = getIntent().getStringExtra("prodocut_title");
            title_txt.setText(prodocut_title);
            Log.i("prodocut_title",prodocut_title);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


        try
        {
            product_img = getIntent().getStringExtra("product_img");
            productId = getIntent().getStringExtra("productId");

            Log.i("product_img",product_img);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        try
        {
            product_price = getIntent().getStringExtra("product_price");


            Log.i("product_price",product_price);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        try
        {
            group_id_group = getIntent().getStringExtra("group_id_group");
            navigation = getIntent().getStringExtra("navigation");

            if(!navigation.equalsIgnoreCase("")){
                chat_byGroup_firstload(group_id_group);
            }


            Log.i("group_id_group",group_id_group);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


        try
        {
            market_type = getIntent().getStringExtra("market_type");


            Log.i("market_type",market_type);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        try {


            if (!revisedContactName.equalsIgnoreCase("")) {

                title_txt.setText(prodocut_title);
                createGroup(userid, prodocut_title, revisedContactNumber, revisedContactName,productId);
                Log.i("CREATE GROUP", userid + prodocut_title + revisedContactNumber + revisedContactName+productId+product_img);

            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        layout_main = findViewById(R.id.layout_main);


        title_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                container_toolbar.setVisibility(View.VISIBLE);

                Intent i = new Intent(ChatActivity.this, ChatMemberListActivity.class);
                i.putExtra("group_id", group_id);
                i.putExtra("prodocut_title", prodocut_title);
                i.putExtra("product_img", product_img);
                i.putExtra("product_price", product_price);
                startActivity(i);

//                ChatFragment homeTabFrag = new ChatFragment();
//                Bundle args = new Bundle();
//                args.putString("group_id", group_id);
//                homeTabFrag.setArguments(args);
//                FragmentTransaction fragTransaction1 = getSupportFragmentManager().beginTransaction().addToBackStack("About Fragment");
//                fragTransaction1.replace(R.id.frameContainer, homeTabFrag);
//                fragTransaction1.commit();
            }
        });

        send_msg_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(edt_message.length()==0)
                {

                    edt_message.requestFocus();
                    return;
                }
                if(edt_message.getText().toString().trim().isEmpty())
                {

                    edt_message.requestFocus();
                    return;
                }else {

                    String messageStr = edt_message.getText().toString().trim();
                    send_message(messageStr);
                }

            }
        });



    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_person_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.add_nav) {




            Intent intentContactPick = new Intent(ChatActivity.this,MultipleContactPickerActivity.class);
            intentContactPick.putExtra("prodocut_title",prodocut_title);
            intentContactPick.putExtra("add_member","add_member");
            startActivityForResult(intentContactPick,1000);

            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode,int resultCode,Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1000 && resultCode == RESULT_OK){

            ArrayList<Contact> selectedContacts = data.getParcelableArrayListExtra("SelectedContacts");

            String contactName="";
            String contactNumber="";

            for(int i=0;i<selectedContacts.size();i++){

                contactName += selectedContacts.get(i).name+",";
                contactNumber += selectedContacts.get(i).phone+",";
            }

            int lastCommaIndex = contactName.lastIndexOf(",");
            String revisedContactName = contactName.substring(0, lastCommaIndex);
            int lastCommaIndex1 = contactNumber.lastIndexOf(",");
            String revisedContactNumber = contactNumber.substring(0, lastCommaIndex1);

            System.out.println("Selected Contacts :"+revisedContactName+"\n");
            System.out.println("Selected Contacts :"+revisedContactNumber.replaceAll(" ","")+"\n");


            add_member(group_id,revisedContactNumber,revisedContactName);

        }

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void createGroup(String userid, String prodocut_title, String revisedContactNumber, String revisedContactName,final String productId)
    {
        final ProgressDialog proDialog = ProgressDialog.show(ChatActivity.this, "Processing", "Please Wait...");
        StringRequest request = new StringRequest(Request.Method.POST, Url_List.URL_LOGIN+"?access=true&action=make_group", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (!response.equals(null)) {
                    Log.e("Your Array Response", response);

                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response);
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("1")) {

                            Log.i("Create Group RESPONSE",response);

                            String message = jsonObject.optString("message");

                                 group_id = jsonObject.optString("group_id");
                                 group_id_group = group_id;

                                chat_byGroup_firstload(group_id);

//                            Toast.makeText(ChatActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                            proDialog.dismiss();
                        }
                        else
                        {
                            Toast.makeText(ChatActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            proDialog.dismiss();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        proDialog.dismiss();

                    }

                } else {
                    Log.e("Your Array Response", "Data Null");
                    proDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error is ", "" + error);
                Toast.makeText(ChatActivity.this, "Unable to process this request , please try again later.", Toast.LENGTH_SHORT).show();
                proDialog.dismiss();
            }
        }) {

            //Pass Your Parameters here
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", userid);
                params.put("product_id", productId);
                params.put("price", product_price);
                params.put("product_name",prodocut_title.replace("'",""));
                params.put("product_img", product_img);
                params.put("phone", revisedContactNumber);
                params.put("name",revisedContactName);

                System.out.println("Sending: "+userid);
                System.out.println("Sending: "+productId);
                System.out.println("Sending: "+product_price);
                System.out.println("Sending: "+prodocut_title.replace("'",""));
                System.out.println("Sending: "+product_img);
                System.out.println("Sending: "+revisedContactNumber);
                System.out.println("Sending: "+revisedContactName);



                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(ChatActivity.this);
        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }

    private void add_member(final String group_id,final String revisedContactNumber,final String revisedContactName) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.URL_SIGNUP+"?",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //   progressBar.setVisibility(View.GONE);

                        try {
                            Gson gson = new Gson();
                            JSONObject obj = new JSONObject(response);


                            Log.i("Add Member RESPONSE",response);

                            String status = obj.optString("status");
                            String message = obj.optString("message");



                            if (status.contains("1")) {

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Unable to process this request Please try again later", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "make_group_member");
                params.put("group_id", group_id);
                params.put("phone", revisedContactNumber);
                params.put("name", revisedContactName);



                return params;
            }
        };


        RequestQueue queue = Volley.newRequestQueue(ChatActivity.this);
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }



    private void send_message( String messageStr) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.URL_SIGNUP,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //   progressBar.setVisibility(View.GONE);

                        try {
                            Gson gson = new Gson();
                            JSONObject obj = new JSONObject(response);


                            Log.i("Add Member RESPONSE",response);

                            String status = obj.optString("status");
                            String message = obj.optString("message");

                            if (status.contains("1")) {


                                chatList.add(new ChatModel(username, messageStr,group_id,user_id));
                                edt_message.setText("");
                                chatAdapter.notifyItemInserted(chatAdapter.getItemCount()+1);
                                chatAdapter.notifyDataSetChanged();
                                chatrecyclerView.smoothScrollToPosition(chatAdapter.getItemCount());

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Unable to process this request Please try again later", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "chat");
                params.put("group_id", group_id_group);
                params.put("user_id", userid);
                params.put("message", messageStr);



                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(ChatActivity.this);
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }



    private void chat_byGroup_firstload(String group_id) {

        final ProgressDialog progressDialog = new ProgressDialog(ChatActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.URL_CHAT_LIST,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressDialog.dismiss();

                        try {
                            JSONObject obj = new JSONObject(response);

                            Log.i("CHAT LIST RESPONSE",response);

                            String status = obj.optString("status");
                            if(status.equalsIgnoreCase("1"))
                            {
                                JSONArray dataList = obj.getJSONArray("data");
                                for (int i = 0; i < dataList.length(); i++) {
                                    JSONObject jsonObject = dataList.getJSONObject(i);
                                    String username = jsonObject.optString("username");
                                    String messages = jsonObject.optString("messages");
                                    String chat_groupid = jsonObject.optString("chat_groupid");
                                     user_id = jsonObject.optString("user_id");
                                    String product_image = jsonObject.optString("product_image");
                                    String price = jsonObject.optString("price");

                                    if(price.contains("$"))
                                    {
                                        price = price.replace("$","");
                                    }
                                    else
                                    {
                                        price = price;
                                    }

                                    String product_id = jsonObject.optString("product_id");
                                    String product_name = jsonObject.optString("product_name");

                                    if(!jsonObject.isNull("product_id"))
                                    {
                                        chatList.add(new ChatModel(username, messages,chat_groupid,user_id,product_id,product_image,product_name,price));
                                    }
                                    else
                                    {
                                        chatList.add(new ChatModel(username, messages,chat_groupid,user_id));
                                    }


                                    ViewCompat.setNestedScrollingEnabled(chatrecyclerView,false);

                                }

                                chatrecyclerView.setAdapter(chatAdapter);
                                chatAdapter.notifyDataSetChanged();
                                isScrolling=true;
                            }
                            else
                            {
                                // no data available
                                Toast.makeText(getApplicationContext(),obj.optString("message"),Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                     //   Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        isScrolling = true;
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Unable to process this request Please try again later", Toast.LENGTH_SHORT).show();
//                        new AlertDialog.Builder(ChatActivity.this)
//                                .setMessage("Unable to process this request Please try again later")
//                                .show();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "get_chat_bygroup");
                params.put("group_id", group_id);
                params.put("start", "0");
                params.put("end", "2000");

                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(ChatActivity.this);
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }


    private void loadMore(int start,int end) {

        final ProgressWheel progressWheel = (ProgressWheel) findViewById(R.id.progress_wheel);
        progressWheel.setVisibility(View.VISIBLE);


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.URL_PRODUCT_LIST,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressWheel.setVisibility(View.GONE);

                        try {

                            JSONObject obj = new JSONObject(response);
                            Log.i("CHAT LIST RESPONSE",response);
                            String status = obj.optString("status");

                            if (status.equalsIgnoreCase("true")) {
                                // we need to check this, to make sure, our dataStructure JSonArray contains
                                JSONArray dataList = obj.getJSONArray("data");
                                for (int i = 0; i < dataList.length(); i++) {
                                    JSONObject jsonObject = dataList.getJSONObject(i);
                                    String username = jsonObject.optString("username");
                                    String messages = jsonObject.optString("messages");
                                    String chat_groupid = jsonObject.optString("chat_groupid");


                                    chatList.add(new ChatModel(username, messages,chat_groupid,user_id));
                                    chatAdapter.notifyDataSetChanged();

                                }
                                chatrecyclerView.setAdapter(chatAdapter);
                                chatAdapter.notifyDataSetChanged();

                                isScrolling = true;
                            }
                            else
                            {
                                // something
                                Toast.makeText(getApplicationContext(), obj.optString("displayMessage"), Toast.LENGTH_SHORT).show();
                                isScrolling=false;
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                      //  Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        progressWheel.setVisibility(View.GONE);
                        // volley finished and returned network error, update and unlock  isScrolling
//                        isScrolling = true;
                        Toast.makeText(getApplicationContext(), "Failed to load more, network error", Toast.LENGTH_SHORT).show();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "get_chat_bygroup");
                params.put("group_id", "82");
                params.put("start", String.valueOf(start));
                params.put("end", String.valueOf(end));

                return params;
            }
        };


        RequestQueue queue = Volley.newRequestQueue(ChatActivity.this);
        stringRequest.setShouldCache(false);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                300000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }

}
