package com.yesitlabs.govava_daniyal.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import com.android.volley.toolbox.Volley;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.URLS.Url_List;
import com.yesitlabs.govava_daniyal.adapter.CategoriesProductAdapter;
import com.yesitlabs.govava_daniyal.adapter.ProductAdapter;
import com.yesitlabs.govava_daniyal.model.CatagoriesModel;
import com.yesitlabs.govava_daniyal.model.LIstOfFavriote;
import com.yesitlabs.govava_daniyal.model.ProductModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private CategoriesProductAdapter categoriesProductAdapter;
    private RecyclerView horizontal_recycler_view;
    private ProductAdapter productAdapter;
    LinearLayoutManager manager;
    LinearLayoutManager horizontal_manager;
    static int endPage;
    Boolean isScrolling = false;

    private ArrayList<CatagoriesModel> categoriesList;


    private View rootView;
    private JSONArray dataList;
    private String image1 = "", userid = "";


    private ArrayList<ProductModel> productList;
    private RecyclerView recyclerView;
    private NestedScrollView nest_scroll;
    private ArrayList<LIstOfFavriote> listOfFavriote;
    private String favoriteProductId = "", favtype = "";
    private JSONArray favoriteArray;
    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        nest_scroll = rootView.findViewById(R.id.nest_scroll);
        TextView txtTitle = getActivity().findViewById(R.id.txtTitle);
        txtTitle.setText("HomeFragment");

        setHasOptionsMenu(true);
        categoriesList = new ArrayList<>();
        recyclerView = rootView.findViewById(R.id.home_grid_recycler);
        listOfFavriote = new ArrayList<LIstOfFavriote>();

        try {
            SharedPreferences prefs = getActivity().getSharedPreferences("LoginDetail", Context.MODE_PRIVATE);

            userid = prefs.getString("userid", "");

            //   Toast.makeText(this,"Age : "+String.valueOf(Age),Toast.LENGTH_SHORT).show();
        } catch (Exception e) {

        }


        int numberOfColumns = 3;

        firstLoad();

        horizontal_recycler_view = (RecyclerView) rootView.findViewById(R.id.rv_horizontal);

        categoriesList();

        productList = new ArrayList<>();
        favoriteArray = new JSONArray();


        //   productAdapter = new ProductAdapter(productList);
        manager = new GridLayoutManager(getActivity(), numberOfColumns);

        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        recyclerView.setHasFixedSize(true);

        //  recyclerView.setAdapter(productAdapter);
        recyclerView.setLayoutManager(manager);

        categoriesProductAdapter = new CategoriesProductAdapter(getContext(), categoriesList, horizontal_recycler_view);
        horizontal_manager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        horizontal_recycler_view.setLayoutManager(horizontal_manager);


        nest_scroll.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (v.getChildAt(v.getChildCount() - 1) != null) {
                if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                        scrollY > oldScrollY) {
                    //code to fetch more data for endless scrolling
                    if (isScrolling) {
                        loadMore(productList.size() + 1, 30);
                    } else {
                        Toast.makeText(getActivity(), "No more Products to show", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        return rootView;

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.


            case R.id.action_search:

                SearchFragment homeTabFrag = new SearchFragment();
                Bundle args1 = new Bundle();
                homeTabFrag.setArguments(args1);
                FragmentTransaction fragTransaction1 = getActivity().getSupportFragmentManager().beginTransaction().addToBackStack("Pets Fragment");
                fragTransaction1.replace(R.id.frameContainer, homeTabFrag);
                fragTransaction1.commit();


                // Not implemented here
                return true;

            case R.id.action_gift:

                FindGift findGift = new FindGift();
                Bundle args12 = new Bundle();
                findGift.setArguments(args12);
                FragmentTransaction fragTransaction12 = getActivity().getSupportFragmentManager().beginTransaction().addToBackStack("Pets Fragment");
                fragTransaction12.replace(R.id.frameContainer, findGift);
                fragTransaction12.commit();

            default:
                break;
        }

        return false;
    }

    private void categoriesList() {

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Url_List.URL_CATEGORY_LIST + "?access=true&action=get_Categories_list&start=0&end=100",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {

                            JSONObject obj = new JSONObject(response);
                            Log.i("CategoryResponse", response);
                            String status = obj.optString("status");

                            if (status.equalsIgnoreCase("1")) {
                                dataList = obj.getJSONArray("data");
                                for (int i = 0; i < dataList.length(); i++) {
                                    JSONObject jsonObject = dataList.getJSONObject(i);
                                    String id = jsonObject.optString("id");
                                    String parent_id = jsonObject.optString("parent_id");
                                    String name = jsonObject.optString("name");
                                    image1 = jsonObject.optString("image");
//                                ImageArrayList.add("https://govava.yesitlabs.com/"+image);
//                                nameArrayList.add(name);

                                    CatagoriesModel categoriesProduct = new CatagoriesModel();
                                    categoriesProduct.setCategoryId(id);
                                    categoriesProduct.setCategoryParentId(parent_id);
                                    categoriesProduct.setCategoryName(name);
                                    categoriesProduct.setCategoryImage(image1);
                                    categoriesList.add(categoriesProduct);
                                }

                                ViewCompat.setNestedScrollingEnabled(horizontal_recycler_view, false);
                                horizontal_recycler_view.setAdapter(categoriesProductAdapter);
                            } else {
                                   Toast.makeText(getContext(), obj.optString("displaymessage"), Toast.LENGTH_SHORT).show();
                                   progressDialog.dismiss();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        isScrolling = true;
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), "Unable to process this request, please try again later", Toast.LENGTH_SHORT).show();
//                        new AlertDialog.Builder(getContext())
//                                .setMessage("Unable to process this request Please try again later")
//                                .show();

                    }
                }) {

        };

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }

    public void firstLoad() {
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest request = new StringRequest(Request.Method.GET, Url_List.URL_PRODUCT_LIST +"?access=true&action=newget_product_list&start=0&end=18&user_id=" + userid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (!response.equals(null)) {
                    Log.e("Your Array Response", response);

                    try {
                        JSONObject obj = new JSONObject(response);

                        Log.i("ProducutResponse", response);

                        String status = obj.optString("status");
                        if (status.equalsIgnoreCase("1")) {

                            String totalPage = obj.optString("Total_page");
                            endPage = Integer.valueOf(totalPage);

                            JSONArray dataList = obj.getJSONArray("data");

                            for (int i = 0; i < dataList.length(); i++) {
                                JSONObject jsonObject = dataList.getJSONObject(i);
                                String prodocut_title = jsonObject.optString("prodocut_title");
                                String prodocut_description = jsonObject.optString("prodocut_description");
                                String prodouct_image = jsonObject.optString("prodouct_image");
                                String prodocut_price = jsonObject.optString("prodocut_price");
                                String review = jsonObject.optString("review");
                                String market_type = jsonObject.optString("market_type");
                                String product_id = jsonObject.optString("product_id");
                                String favourite_id = jsonObject.optString("favourite_id");


                                productList.add(new ProductModel(prodocut_title, prodocut_description, prodouct_image, prodocut_price, review, market_type, product_id, favourite_id));
                                ViewCompat.setNestedScrollingEnabled(recyclerView, false);

                            }
                            favoriteArray = obj.optJSONArray("favourite");

                            productAdapter = new ProductAdapter(getContext(), productList, listOfFavriote,favoriteArray);

                            recyclerView.setAdapter(productAdapter);
                            productAdapter.notifyDataSetChanged();
                            isScrolling = true;
                            progressDialog.dismiss();
                        } else {
                            // no data available
                            Toast.makeText(getContext(), obj.optString("displayMessage"), Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }

                } else {
                    Log.e("Your Array Response", "Data Null");
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error is ", "" + error);
                progressDialog.dismiss();
            }
        }) {


        };
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }


    private void loadMore(int start, int end) {

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest request = new StringRequest(Request.Method.GET, Url_List.URL_PRODUCT_LIST +"?access=true&action=newget_product_list&start="+String.valueOf(start)+"&end=" + String.valueOf(end)+ "&user_id="+ userid, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                progressDialog.dismiss();
                try {

                    JSONObject obj = new JSONObject(response);
                    Log.i("ProductPagination", response);
                    String status = obj.optString("status");

                    if (status.equalsIgnoreCase("1")) {
                        // we need to check this, to make sure, our dataStructure JSonArray contains
                        JSONArray dataList = obj.getJSONArray("data");
                        for (int i = 0; i < dataList.length(); i++) {
                            JSONObject jsonObject = dataList.getJSONObject(i);
                            String prodocut_title = jsonObject.optString("prodocut_title");
                            String prodocut_description = jsonObject.optString("prodocut_description");
                            String prodouct_image = jsonObject.optString("prodouct_image");
                            String prodocut_price = jsonObject.optString("prodocut_price");
                            String review = jsonObject.optString("review");
                            String market_type = jsonObject.optString("market_type");
                            String product_id = jsonObject.optString("product_id");
                            String favourite_id = jsonObject.optString("favourite_id");
                            String feedMerchantId = jsonObject.optString("feedMerchantId");
                            String category = jsonObject.optString("category");


                            productList.add(new ProductModel(prodocut_title, prodocut_description, prodouct_image, prodocut_price, review, market_type, product_id, favourite_id));
                            productAdapter.notifyDataSetChanged();

                        }

                        favoriteArray = obj.optJSONArray("favourite");

                        recyclerView.setAdapter(productAdapter);
                        productAdapter.notifyDataSetChanged();

                        isScrolling = true;

                    } else {
                        // something
                        Toast.makeText(getContext(), obj.optString("displayMessage"), Toast.LENGTH_SHORT).show();
                        isScrolling = false;
                        progressDialog.dismiss();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    progressDialog.dismiss();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //    Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        // volley finished and returned network error, update and unlock  isScrolling
//                        isScrolling = true;
                        Toast.makeText(getContext(), "Unable to process this request, please try again later", Toast.LENGTH_SHORT).show();

                    }
                }) {

        };

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }

}
