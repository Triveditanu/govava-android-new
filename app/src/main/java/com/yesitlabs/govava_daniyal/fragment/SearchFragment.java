package com.yesitlabs.govava_daniyal.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.adapter.SearchAdapter;
import com.yesitlabs.govava_daniyal.model.LIstOfFavriote;
import com.yesitlabs.govava_daniyal.model.SearchModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SearchFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private ImageView img_back;
    private ArrayList<SearchModel> searchModelList;

    private SearchAdapter searchAdapter;
    private RecyclerView recyclerView;
    private LinearLayoutManager manager;
    private View view;
    Boolean isScrolling = false;
    private AutoCompleteTextView  edtSearch;
    private ArrayList<String> searchList;
    String searchText = "";
    private SharedPreferences prefs;
    List<String> sample;
    ArrayAdapter<String> adapterSearch;
    Context mContext;
    private TextView clear_history_txt;
    private ImageView img_cross;
    private ArrayList<LIstOfFavriote> listOfFavriote;
    private JSONArray favoriteArray;

    public SearchFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchFragment newInstance(String param1, String param2) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        view = inflater.inflate(R.layout.fragment_search, container, false);

        img_back = (ImageView)view.findViewById(R.id.img_back);
        edtSearch = view.findViewById(R.id.edtSearch);

        img_cross =view.findViewById(R.id.img_cross);

        clear_history_txt = view.findViewById(R.id.clear_history_txt);
        listOfFavriote = new ArrayList<LIstOfFavriote>();
        favoriteArray = new JSONArray();

        recyclerView = view.findViewById(R.id.cat_grid_recycler);
        int numberOfColumns = 3;

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                firstLoad();
//                Intent i = new Intent(getContext(), HomeActivity.class);
//                startActivity(i);
                getActivity().onBackPressed();
            }
        });


        img_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtSearch.setText("");
            }
        });



        searchList = new ArrayList<>();
        searchModelList = new ArrayList<>();
//
        searchAdapter = new SearchAdapter(getContext(),searchModelList,listOfFavriote,favoriteArray);


        //   productAdapter = new ProductAdapter(productModel);
        manager = new GridLayoutManager(getActivity(), numberOfColumns);

        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        recyclerView.setHasFixedSize(true);

        //  recyclerView.setAdapter(productAdapter);
        recyclerView.setLayoutManager(manager);


        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    searchText = edtSearch.getText().toString().trim();
                    searchList.add(searchText);

                    adapterSearch = new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_dropdown_item, loadArray());
                    //Find TextView control
                    adapterSearch.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);
                    //Set the number of characters the user must type before the drop down list is shown
                    edtSearch.setThreshold(1);
                    //Set the adapter
                    edtSearch.setAdapter(adapterSearch);

                    saveArrayList();
                    Log.i("List_Search", String.valueOf(searchList));

                    InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                    firstLoad(searchText);
                    return true;
                }
                return false;
            }
        });

        edtSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                adapterSearch = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, loadArray());
                //Find TextView control
                adapterSearch.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                //Set the number of characters the user must type before the drop down list is shown
                edtSearch.setThreshold(1);
                //Set the adapter
                edtSearch.setAdapter(adapterSearch);
                return false;
            }
        }) ;

        clear_history_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(searchList == null){

                }else {
                    showAlertDialog();
                }


            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void firstLoad(String searchText) {
        String url = "https://govava.yesitlabs.com/api/product.php?access=true&action=get_product_list_bycat&cat_name="+searchText+"&start=0&end=100" ;

//        isScrolling = false; // lock this guy,(isScrolling) to make sure,
        // user will not load more when volley is processing another request
        // only load more when  volley is free

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,



                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressDialog.dismiss();
                        // remember here we are in the main thread, that means,
                        //volley has finished processing request, and we have our response.
                        // What else are you waiting for? update isScrolling = true;
//                        isScrolling = true;

                        //   progressBar.setVisibility(View.GONE);

                        try {

                            JSONObject obj = new JSONObject(response);
                            searchModelList = new ArrayList<>();


                            Log.i("RESPONSE",response);

                            String status = obj.optString("status");

                            if(status.equalsIgnoreCase("true"))
                            {
                                String totalPage = obj.optString("Total_page");


                                JSONArray dataList = obj.getJSONArray("data");
                                for (int i = 0; i < dataList.length(); i++) {
                                    JSONObject jsonObject = dataList.getJSONObject(i);
                                    String prodocut_title = jsonObject.optString("prodocut_title");
                                    String prodocut_description = jsonObject.optString("prodocut_description");
                                    String prodouct_image = jsonObject.optString("prodouct_image");
                                    String prodocut_price = jsonObject.optString("prodocut_price");
                                    String review = jsonObject.optString("review");
                                    String market_type = jsonObject.optString("market_type");
                                    String product_id = jsonObject.optString("product_id");

                                    searchModelList.add(new SearchModel(prodocut_title, prodocut_description, prodouct_image, prodocut_price, review, market_type, product_id));
                                    ViewCompat.setNestedScrollingEnabled(recyclerView, false);

                                }
                                favoriteArray = obj.optJSONArray("favourite");

                                searchAdapter = new SearchAdapter(getContext(), searchModelList, listOfFavriote,favoriteArray);

                                recyclerView.setAdapter(searchAdapter);
                                searchAdapter.notifyDataSetChanged();
                                isScrolling = true;
                                progressDialog.dismiss();

                            }
                            else {
                                Toast.makeText(getContext(),obj.optString("displayMessage"),Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    //    Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        isScrolling = true;
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), "Unable to process this request Please try again later", Toast.LENGTH_SHORT).show();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();




                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }


    private void loadMore(int start,int end,String searchText) {
        String url = "https://govava.yesitlabs.com/api/product.php?access=true&action=get_product_list_bycat&cat_name="+searchText+start+"&end="+end;

//        isScrolling = false; // lock this until volley completes processing

        // progressWheel is just a loading spinner, please see the content_main.xml
        final ProgressWheel progressWheel = (ProgressWheel) view.findViewById(R.id.progress_wheel);
        progressWheel.setVisibility(View.VISIBLE);


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,



                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressWheel.setVisibility(View.GONE);
                        // remember here we are in the main thread, that means,
                        //volley has finished processing request, and we have our response.
                        // What else are you waiting for? update isScrolling = true;
//                        isScrolling = true;

                        if (response.length() <= 0) {
                            // we need to check this, to make sure, our dataStructure JSonArray contains
                            // something
                            Toast.makeText(getContext(), "no data available", Toast.LENGTH_SHORT).show();
                            return; // return will end the program at this point
                        }

                        //   progressBar.setVisibility(View.GONE);

                        try {

                            JSONObject obj = new JSONObject(response);


                            Log.i("RESPONSE",response);

                            String status = obj.optString("status");

                            if(status.equalsIgnoreCase("true"))
                            {
                                String totalPage = obj.optString("Total_page");


                                JSONArray dataList = obj.getJSONArray("data");
                                for (int i = 0; i < dataList.length(); i++) {
                                    JSONObject jsonObject = dataList.getJSONObject(i);
                                    String prodocut_title = jsonObject.optString("prodocut_title");
                                    String prodocut_description = jsonObject.optString("prodocut_description");
                                    String prodouct_image = jsonObject.optString("prodouct_image");
                                    String prodocut_price = jsonObject.optString("prodocut_price");
                                    String review = jsonObject.optString("review");
                                    String market_type = jsonObject.optString("market_type");
                                    String product_id = jsonObject.optString("product_id");


                                    searchModelList.add(new SearchModel(prodocut_title, prodocut_description,prodouct_image,prodocut_price,review,market_type,product_id));
                                    ViewCompat.setNestedScrollingEnabled(recyclerView,false);
                                    searchAdapter.notifyDataSetChanged();


//                                ImageArrayList.add("https://govava.yesitlabs.com/"+image);
//                                nameArrayList.add(name);


                                }

                                recyclerView.setAdapter(searchAdapter);

                            }
                            else {
                                Toast.makeText(getContext(),obj.optString("displayMessage"),Toast.LENGTH_SHORT).show();
                            }




//                            Glide.with(getContext())
//                                    .load("https://govava.yesitlabs.com/"+image)
//                                    .thumbnail(0.5f)
//                                    .into();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "Unable to process this request Please try again later", Toast.LENGTH_SHORT).show();
                        progressWheel.setVisibility(View.GONE);
                        // volley finished and returned network error, update and unlock  isScrolling
//                        isScrolling = true;


                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();




                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        stringRequest.setShouldCache(false);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                300000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }





    public void saveArrayList(){
         prefs = getActivity().getSharedPreferences("searchPrefsKey",Context.MODE_PRIVATE);
        SharedPreferences.Editor edit=prefs.edit();

        Set<String> set = new HashSet<String>();
        set.addAll(searchList);
        edit.putStringSet("searchKey", set);
        edit.commit();    // This line is IMPORTANT !!!
    }

    public List<String> loadArray() {
        prefs = getActivity().getSharedPreferences("searchPrefsKey",Context.MODE_PRIVATE);
        Set<String> set = prefs.getStringSet("searchKey", null);
        assert set != null;
        if(set==(null))
        {
            Log.i("Check","Check Invoked");
        }
        else
        {
            sample = new ArrayList<String>(set);
            Log.i("Save Search", String.valueOf(sample));
        }


        return sample;
    }


    public void showAlertDialog() {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage("Are you sure you want to clear search history");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SharedPreferences preferences = getActivity().getSharedPreferences("searchPrefsKey", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear()
                        .apply();

                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

}
