package com.yesitlabs.govava_daniyal.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.URLS.Url_List;
import com.yesitlabs.govava_daniyal.adapter.FilterAdapter;
import com.yesitlabs.govava_daniyal.model.FilterFindGiftModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FilterFindGiftFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FilterFindGiftFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FilterFindGiftFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String age = "";
//    private String gender = "";
    private String price = "";
    private String occassion = "";
    private String relation = "";
    private String style = "";

    private OnFragmentInteractionListener mListener;
    private JSONArray dataList;
    private ArrayList<FilterFindGiftModel> filterModel;
    private View view;
    private FilterAdapter filterAdapter;
    private RecyclerView recyclerView;
    private LinearLayoutManager manager;
    Boolean isScrolling = false;
    private String phoneNumber = "";
    private String phonenames = "";

    public FilterFindGiftFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FilterFindGiftFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FilterFindGiftFragment newInstance(String param1, String param2) {
        FilterFindGiftFragment fragment = new FilterFindGiftFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         view  = inflater.inflate(R.layout.fragment_filter_find_gift, container, false);



        recyclerView = view.findViewById(R.id.grid_recycler);
        int numberOfColumns = 3;
        SharedPreferences prefs = getActivity().getSharedPreferences("PhoneNumberPre", Context.MODE_PRIVATE);
        phoneNumber = prefs.getString("Number", "");
        phonenames = prefs.getString("Names", "");


        Bundle args = getArguments();
        if (args != null) {
            age = args.getString("age");
//            gender = getArguments().getString("gender");
            price = getArguments().getString("price");
            occassion = getArguments().getString("occassion");
            relation = getArguments().getString("relation");
            style = getArguments().getString("style");
         //   Toast.makeText(getActivity(), "Value" + age + "value2" + gender + "Value3" + price + "Value4" + occassion + "Value5" + relation + "Value6" + style, Toast.LENGTH_SHORT).show();
        }

        firstLoad_personStyle();
        filterModel = new ArrayList<>();

        filterAdapter = new FilterAdapter(getContext(),filterModel,recyclerView);

        //   productAdapter = new ProductAdapter(productModel);
        manager = new GridLayoutManager(getActivity(), numberOfColumns);

        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        recyclerView.setHasFixedSize(true);

        //  recyclerView.setAdapter(productAdapter);
        recyclerView.setLayoutManager(manager);

        filterAdapter.setOnLoadMoreListener(new FilterAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.i("LoadMore", "onLoadMore called");
                //add progress item
                filterModel.add(null);
                filterAdapter.notifyDataSetChanged();

                Log.i("LoadMore", "Loading new data... (" + 5 + ") posts");
                //remove progress item
                filterModel.remove(filterModel.size() - 1);

                loadMore(filterModel.size()+1,10);

                filterAdapter.notifyDataSetChanged();
                //add items one by one

                filterAdapter.setLoaded();
            }
        });

return view;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void firstLoad_personStyle() {
        String url = Url_List.URL_PRODUCT_LIST+"?access=true&action=get_trending_giftbyuser&age="+age+"&price"+price+"&style="+"Casual"+"&start=0&end=5";

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,



                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressDialog.dismiss();
                        // remember here we are in the main thread, that means,
                        //volley has finished processing request, and we have our response.
                        // What else are you waiting for? update isScrolling = true;
//                        isScrolling = true;

                        //   progressBar.setVisibility(View.GONE);

                        try {

                            JSONObject obj = new JSONObject(response);


                            Log.i("RESPONSE",response);

                            String status = obj.optString("status");

                            if(status.equalsIgnoreCase("true"))
                            {
                                String totalPage = obj.optString("Total_page");


                                JSONArray dataList = obj.getJSONArray("data");
                                for (int i = 0; i < dataList.length(); i++) {
                                    JSONObject jsonObject = dataList.getJSONObject(i);
                                    String prodocut_title = jsonObject.optString("prodocut_title");
                                    String prodocut_description = jsonObject.optString("prodocut_description");
                                    String prodouct_image = jsonObject.optString("prodouct_image");
                                    String prodocut_price = jsonObject.optString("prodocut_price");
                                    String review = jsonObject.optString("review");
                                    String market_type = jsonObject.optString("market_type");
                                    String product_id = jsonObject.optString("product_id");


                                    filterModel.add(new FilterFindGiftModel(prodocut_title, prodocut_description,prodouct_image,prodocut_price,review,market_type,product_id));
                                    ViewCompat.setNestedScrollingEnabled(recyclerView,false);
                                    filterAdapter.notifyDataSetChanged();


//                                ImageArrayList.add("https://govava.yesitlabs.com/"+image);
//                                nameArrayList.add(name);


                                }

                                recyclerView.setAdapter(filterAdapter);

                            }else {

                                final Dialog dialog = new Dialog(getContext());
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setCancelable(false);
                                dialog.setContentView(R.layout.custom_alert_layout);

                                TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
                                ImageView imgLogo = dialog.findViewById(R.id.imgLogo);

                                text.setText(obj.optString("displayMessage"));
                                imgLogo.getResources().getDrawable(R.drawable.ic_warning_black_24dp);
                                Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
                                dialogButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.cancel();

                                        HomeFragment homeTabFrag = new HomeFragment();
                                        Bundle args1 = new Bundle();
                                        homeTabFrag.setArguments(args1);
                                        FragmentTransaction fragTransaction1 = getActivity().getSupportFragmentManager().beginTransaction();
                                        fragTransaction1.replace(R.id.frame, homeTabFrag);
                                        fragTransaction1.commit();

                                    }
                                });

                                dialog.show();

                            }




//                            Glide.with(getContext())
//                                    .load("https://govava.yesitlabs.com/"+image)
//                                    .thumbnail(0.5f)
//                                    .into();


                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                     //   Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        isScrolling = true;
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), "Unable to process this request Please try again later", Toast.LENGTH_SHORT).show();
//                        new AlertDialog.Builder(getContext())
//                                .setMessage("Unable to process this request Please try again later")
//                                .show();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();




                return params;
            }
        };



        RequestQueue queue = Volley.newRequestQueue(getActivity());
        stringRequest.setShouldCache(false);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                300000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }


    private void loadMore(int start,int end) {
        String url = Url_List.URL_PRODUCT_LIST+"?access=true&action=get_trending_giftbyuser&age="+age+"&price"+price+"&style="+"Casual"+"&start="+start+"&end="+end;

//        isScrolling = false; // lock this until volley completes processing

        // progressWheel is just a loading spinner, please see the content_main.xml
        final ProgressWheel progressWheel = (ProgressWheel) view.findViewById(R.id.progress_wheel);
        progressWheel.setVisibility(View.VISIBLE);


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,



                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressWheel.setVisibility(View.GONE);
                        // remember here we are in the main thread, that means,
                        //volley has finished processing request, and we have our response.
                        // What else are you waiting for? update isScrolling = true;
//                        isScrolling = true;

                        if (response.length() <= 0) {
                            // we need to check this, to make sure, our dataStructure JSonArray contains
                            // something
                            Toast.makeText(getContext(), "no data available", Toast.LENGTH_SHORT).show();
                            return; // return will end the program at this point
                        }

                        //   progressBar.setVisibility(View.GONE);

                        try {

                            JSONObject obj = new JSONObject(response);


                            Log.i("RESPONSE",response);

                            String status = obj.optString("status");

                            JSONArray dataList = obj.getJSONArray("data");
                            for (int i = 0; i < dataList.length(); i++) {
                                JSONObject jsonObject = dataList.getJSONObject(i);
                                String prodocut_title = jsonObject.optString("prodocut_title");
                                String prodocut_description = jsonObject.optString("prodocut_description");
                                String prodouct_image = jsonObject.optString("prodouct_image");
                                String prodocut_price = jsonObject.optString("prodocut_price");
                                String review = jsonObject.optString("review");
                                String market_type = jsonObject.optString("market_type");
                                String product_id = jsonObject.optString("product_id");


                                filterModel.add(new FilterFindGiftModel(prodocut_title, prodocut_description,prodouct_image,prodocut_price,review,market_type,product_id));
                                filterAdapter.notifyDataSetChanged();

//                                ImageArrayList.add("https://govava.yesitlabs.com/"+image);
//                                nameArrayList.add(name);
                            }
                            recyclerView.setAdapter(filterAdapter);

//                            Glide.with(getContext())
//                                    .load("https://govava.yesitlabs.com/"+image)
//                                    .thumbnail(0.5f)
//                                    .into();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                  //      Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        progressWheel.setVisibility(View.GONE);
                        // volley finished and returned network error, update and unlock  isScrolling
//                        isScrolling = true;
                        Toast.makeText(getContext(), "Failed to load more, network error", Toast.LENGTH_SHORT).show();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();




                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        stringRequest.setShouldCache(false);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                300000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }
}
