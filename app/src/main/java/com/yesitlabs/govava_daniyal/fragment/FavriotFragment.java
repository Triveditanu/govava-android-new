package com.yesitlabs.govava_daniyal.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.URLS.Url_List;
import com.yesitlabs.govava_daniyal.Utils.Constants;
import com.yesitlabs.govava_daniyal.activity.HomeActivity;
import com.yesitlabs.govava_daniyal.adapter.FavoriteAdapter;
import com.yesitlabs.govava_daniyal.model.FavrioteModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FavriotFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FavriotFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FavriotFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ArrayList<FavrioteModel> favrioteList;

    private FavoriteAdapter adapter;
    private Context mContext;


    String[] data = {"JustVH Women at Walmart", "JustVH Women at Walmart", "JustVH Women at Walmart", "JustVH Women at Walmart", "JustVH Women at Walmart", "JustVH Women at Walmart", "JustVH Women at Walmart", "JustVH Women at Walmart", "JustVH Women at Walmart", "JustVH Women at Walmart",
            "JustVH Women at Walmart", "JustVH Women at Walmart", "JustVH Women at Walmart" };

    private OnFragmentInteractionListener mListener;
    private ImageView img_back;
    private String userid ="";
    private RecyclerView recyclerView;
    LinearLayoutManager manager;
    private String dob = "";
    private TextView txt_title;
    private int Age;

    public FavriotFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FavriotFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FavriotFragment newInstance(String param1, String param2) {
        FavriotFragment fragment = new FavriotFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_favriot, container, false);
        txt_title = view.findViewById(R.id.txt_title);

        try {
            SharedPreferences prefs = getActivity().getSharedPreferences("LoginDetail", Context.MODE_PRIVATE);

            dob = prefs.getString("dob", "");
            String formattedDate[] = dob.split("-");

            Age = Constants.getAge(Integer.valueOf(formattedDate[0]),Integer.valueOf(formattedDate[1]),Integer.valueOf(formattedDate[2]));


            if(Age<18){
                txt_title.setText("Wishlist");
            }else {
                txt_title.setText("Favorite");
            }

        }catch (Exception e){
            e.printStackTrace();
        }


        favrioteList();


        img_back = (ImageView)view.findViewById(R.id.img_back);




        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), HomeActivity.class);
                startActivity(i);
            }
        });
        SharedPreferences prefs = getActivity().getSharedPreferences("LoginDetail", Context.MODE_PRIVATE);
        userid = prefs.getString("userid", "");

         recyclerView = view.findViewById(R.id.fav_grid_recycler);
        int numberOfColumns = 3;
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), numberOfColumns));
        favrioteList = new ArrayList<>();

        adapter = new FavoriteAdapter(getContext(),favrioteList);

        //   productAdapter = new ProductAdapter(productList);
        manager = new GridLayoutManager(getActivity(), numberOfColumns);

        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        recyclerView.setHasFixedSize(true);

        //  recyclerView.setAdapter(productAdapter);
        recyclerView.setLayoutManager(manager);






        // Inflate the layout for this fragment
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void favrioteList() {


//        isScrolling = false; // lock this guy,(isScrolling) to make sure,
        // user will not load more when volley is processing another request
        // only load more when  volley is free

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.URL_PRODUCT_DETAIL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //   progressBar.setVisibility(View.GONE);
                        progressDialog.dismiss();


                        try {

                            JSONObject obj = new JSONObject(response);

                            Log.i("Favriote List Response",response);

                            String status = obj.optString("status");

                            if(status.equalsIgnoreCase("1"))
                            {
                                JSONArray dataList = obj.getJSONArray("data");
                                for (int i = 0; i < dataList.length(); i++) {
                                    JSONObject jsonObject = dataList.getJSONObject(i);
                                    String prodocut_title = jsonObject.optString("prodocut_title");
                                    String prodocut_description = jsonObject.optString("product_description");
                                    String prodouct_image = jsonObject.optString("prodouct_image");
                                    String prodocut_price = jsonObject.optString("prodocut_price");
                                    String review = jsonObject.optString("review");
                                    String market_type = jsonObject.optString("market_type");
                                    String product_id = jsonObject.optString("product_id");
                                    String wishlist_id = jsonObject.optString("wishlist_id");

                                    favrioteList.add(new FavrioteModel(prodocut_title, prodocut_description,prodouct_image,prodocut_price,review,market_type,product_id,wishlist_id));
                                    ViewCompat.setNestedScrollingEnabled(recyclerView,false);

                                }

                                recyclerView.setAdapter(adapter);
                                adapter.notifyDataSetChanged();


//                                ViewCompat.setNestedScrollingEnabled(recyclerView,false);
//
//                                recyclerView.setAdapter(categoriesProductAdapter);
                            }
                            else
                            {

                                final Dialog dialog = new Dialog(getContext());
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setCancelable(false);
                                dialog.setContentView(R.layout.custom_alert_layout);

                                TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
                                ImageView imgLogo = dialog.findViewById(R.id.imgLogo);

                                text.setText(obj.optString("message"));
                                imgLogo.getResources().getDrawable(R.drawable.ic_warning_black_24dp);
                                Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
                                dialogButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.cancel();
                                        getActivity().onBackPressed();
                                    }
                                });

                                dialog.show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getContext(), "Unable to process this request Please try again later", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //    Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();

                        progressDialog.dismiss();
                        Toast.makeText(getContext(), "Unable to process this request Please try again later", Toast.LENGTH_SHORT).show();
//                        new AlertDialog.Builder(getContext())
//                                .setMessage("Unable to process this request Please try again later")
//                                .show();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "newget_favourite_wishlist_byuser");
                params.put("userid", userid);

                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);


    }

}
