package com.yesitlabs.govava_daniyal.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.Utils.ScreenSlidePagerAdapter;
import com.yesitlabs.govava_daniyal.model.ContactDetails;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FindGift.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FindGift#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FindGift extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private ViewPager mPager;


    public ArrayList<Fragment> fragmentArrayList;
    public WhatToBuyFragment whatToBuyFragment;
    public WhatToBuyFragment2 whatToBuyFragment2;
    public WhatToBuyFragment3 whatToBuyFragment3;
    public WhatToBuyFragment4 whatToBuyFragment4;
    public WhatToBuyFRagment0 whatToBuyFRagment0;
    public PersonStyleFragment personStyleFragment;
    public String s1 = "";
    public String s2 = "";
    public String s3_to = "";
    public String s3_from = "";
    public String s4 = "";

    private PagerAdapter mPagerAdapter;
    private String phoneNumber = "";
    private String phoneNames ="";

    public FindGift() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FindGift.
     */
    // TODO: Rename and change types and number of parameters
    public static FindGift newInstance(String param1, String param2) {
        FindGift fragment = new FindGift();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_find_gift2, container, false);
        mPager = (ViewPager)view.findViewById(R.id.pager);


        try
        {
            phoneNames = getArguments().getString("Names");
            phoneNumber = getArguments().getString("Number");

            Log.i("phoneNames",phoneNames);
            Log.i("phoneNumber",phoneNumber);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        personStyleFragment = new PersonStyleFragment();
        whatToBuyFRagment0 = new WhatToBuyFRagment0();
        whatToBuyFragment = new WhatToBuyFragment();
        whatToBuyFragment2 = new WhatToBuyFragment2();
        whatToBuyFragment3 = new WhatToBuyFragment3();
        whatToBuyFragment4 = new WhatToBuyFragment4();




        mPager.setVisibility(View.VISIBLE);
        fragmentArrayList = new ArrayList<>();
        fragmentArrayList.add(personStyleFragment);
        fragmentArrayList.add(whatToBuyFRagment0);
        fragmentArrayList.add(whatToBuyFragment);
        fragmentArrayList.add(whatToBuyFragment2);
        fragmentArrayList.add(whatToBuyFragment3);
        fragmentArrayList.add(whatToBuyFragment4);

        mPagerAdapter = new ScreenSlidePagerAdapter(getChildFragmentManager(),fragmentArrayList);
        mPager.setAdapter(mPagerAdapter);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
