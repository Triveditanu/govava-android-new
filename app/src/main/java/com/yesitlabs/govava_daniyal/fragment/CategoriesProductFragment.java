package com.yesitlabs.govava_daniyal.fragment;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.adapter.SelectedCategoriesAdapter;
import com.yesitlabs.govava_daniyal.model.SelectedCategoriesModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CategoriesProductFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CategoriesProductFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CategoriesProductFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private ArrayList<SelectedCategoriesModel> selectCategories;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private RecyclerView cat_grid_recycler;
    private View view;
    private SelectedCategoriesAdapter selectedCatAdapter;

    LinearLayoutManager manager;
    Boolean isScrolling = false;
    private NestedScrollView nestedScrollViewl;
    private String cat_name = "";
    private JSONArray favoriteArray;
    private String userid="";
    public CategoriesProductFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CategoriesProductFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CategoriesProductFragment newInstance(String param1, String param2) {
        CategoriesProductFragment fragment = new CategoriesProductFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);


            TextView txtTitle = getActivity().findViewById(R.id.txtTitle);
            txtTitle.setText("Categories product");

            cat_name = getArguments().getString("cat_name");
//            Toast.makeText(getContext(),"categoreis"+cat_name,Toast.LENGTH_SHORT).show();

        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        int numberOfColumns = 3;
        // Inflate the layout for this fragment
         return inflater.inflate(R.layout.fragment_categories_deatil, container, false);

         }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        try {
            SharedPreferences prefs = getActivity().getSharedPreferences("LoginDetail", Context.MODE_PRIVATE);

            userid = prefs.getString("userid", "");

            //   Toast.makeText(this,"Age : "+String.valueOf(Age),Toast.LENGTH_SHORT).show();
        } catch (Exception e) {

        }


        firstLoad(cat_name);
        cat_grid_recycler = getView().findViewById(R.id.cat_grid_recycler);
        nestedScrollViewl = getView().findViewById(R.id.nest_scroll);
        selectCategories = new ArrayList<>();


        //   productAdapter = new ProductAdapter(productModel);
        manager = new GridLayoutManager(getActivity(), 3);

        cat_grid_recycler.setLayoutManager(new GridLayoutManager(getContext(), 3));
        cat_grid_recycler.setHasFixedSize(true);

        //  recyclerView.setAdapter(productAdapter);
        cat_grid_recycler.setLayoutManager(manager);

        nestedScrollViewl.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if(v.getChildAt(v.getChildCount() - 1) != null) {
                if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                        scrollY > oldScrollY) {
                    //code to fetch more data for endless scrolling
                    if(isScrolling)
                    {
                        loadMore(selectCategories.size()+1,90);
                    }
                    else
                    {
                        Toast.makeText(getActivity(),"No more data to show",Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void firstLoad(String cat_name) {
        String url = "https://govava.yesitlabs.com/api/product.php?access=true&action=newget_product_list_bycat&start=0&end=30&user_id="+userid ;

//        isScrolling = false; // lock this guy,(isScrolling) to make sure,
        // user will not load more when volley is processing another request
        // only load more when  volley is free

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,


                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressDialog.dismiss();
                        // remember here we are in the main thread, that means,
                        //volley has finished processing request, and we have our response.
                        // What else are you waiting for? update isScrolling = true;
//                        isScrolling = true;
                        try {

                            JSONObject obj = new JSONObject(response);


                            Log.i("RESPONSE",response);

                            String status = obj.optString("status");

                            if(status.equalsIgnoreCase("1"))
                            {

                                String totalPage = obj.optString("Total_page");

                                JSONArray dataList = obj.getJSONArray("data");
                                for (int i = 0; i < dataList.length(); i++) {
                                    JSONObject jsonObject = dataList.getJSONObject(i);
                                    String prodocut_title = jsonObject.optString("prodocut_title");
                                    String prodocut_description = jsonObject.optString("prodocut_description");
                                    String prodouct_image = jsonObject.optString("prodouct_image");
                                    String prodocut_price = jsonObject.optString("prodocut_price");
                                    String review = jsonObject.optString("review");
                                    String market_type = jsonObject.optString("market_type");
                                    String product_id = jsonObject.optString("product_id");


                                    selectCategories.add(new SelectedCategoriesModel(prodocut_title, prodocut_description,prodouct_image,prodocut_price,review,market_type,product_id));
                                    ViewCompat.setNestedScrollingEnabled(cat_grid_recycler,false);

                                }

                                favoriteArray = obj.optJSONArray("favourite");

                                selectedCatAdapter = new SelectedCategoriesAdapter(getContext(),selectCategories, favoriteArray);
                                selectedCatAdapter.notifyDataSetChanged();
                                isScrolling = true;

                                cat_grid_recycler.setAdapter(selectedCatAdapter);

                            }
                            else {
                                Toast.makeText(getContext(),obj.optString("displayMessage"),Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                      //  Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        isScrolling = true;
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), "Unable to process this request Please try again later", Toast.LENGTH_SHORT).show();
//                        new AlertDialog.Builder(getContext())
//                                .setMessage("Unable to process this request Please try again later")
//                                .show();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cat_name",cat_name);
                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }


    private void loadMore(int start,int end) {
        String url = "https://govava.yesitlabs.com/api/product.php?access=true&action=newget_product_list_bycat&start="+start+"&end="+end+"&user_id="+userid ;

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressDialog.dismiss();

                        try {

                            JSONObject obj = new JSONObject(response);
                            Log.i("RESPONSE",response);
                            String status = obj.optString("status");

                        if (status.equalsIgnoreCase("1")) {
                            // we need to check this, to make sure, our dataStructure JSonArray contains
                            // something

                            JSONArray dataList = obj.getJSONArray("data");
                            for (int i = 0; i < dataList.length(); i++) {
                                JSONObject jsonObject = dataList.getJSONObject(i);
                                String prodocut_title = jsonObject.optString("prodocut_title");
                                String prodocut_description = jsonObject.optString("prodocut_description");
                                String prodouct_image = jsonObject.optString("prodouct_image");
                                String prodocut_price = jsonObject.optString("prodocut_price");
                                String review = jsonObject.optString("review");
                                String market_type = jsonObject.optString("market_type");
                                String product_id = jsonObject.optString("product_id");


                                selectCategories.add(new SelectedCategoriesModel(prodocut_title, prodocut_description,prodouct_image,prodocut_price,review,market_type,product_id));
                                selectedCatAdapter.notifyDataSetChanged();

                            }

                            favoriteArray = obj.optJSONArray("favourite");

                            cat_grid_recycler.setAdapter(selectedCatAdapter);
                            isScrolling=true;
                        }
                        else
                        {
                            Toast.makeText(getContext(), obj.optString("displayMessage"), Toast.LENGTH_SHORT).show();
                            isScrolling=false;
                            progressDialog.dismiss();

                        }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "Unable to process this request Please try again later", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        // volley finished and returned network error, update and unlock  isScrolling
//                        isScrolling = true;
                      //  Toast.makeText(getContext(), "Failed to load more, network error", Toast.LENGTH_SHORT).show();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cat_name",cat_name);
                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }

}
