package com.yesitlabs.govava_daniyal.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.URLS.Url_List;
import com.yesitlabs.govava_daniyal.Utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class WhatToBuyFragment3 extends Fragment {


    ImageView continueNext,continueback;
    RelativeLayout rl_QuestionCross;
    public ArrayAdapter<String> adapter;
    public Spinner Question3Edit;

    private Spinner spinnerPrice;
    private String age = "";
    private String gender = "";
    private String price = "";
    List<String> radiusList;
    List<String> listOfPrice;
    private String style = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_question_3,
                container, false);

        Bundle args = getArguments();
//        if (args != null) {
//            age = args.getString("age");
//            gender = getArguments().getString("gender");
//           // Toast.makeText(getActivity(),"Value"+age+"value2"+gender,Toast.LENGTH_SHORT).show();
//        }

        listOfPrice = new ArrayList<>();
        price_list();

        try{
            age = args.getString("age");
            style = getArguments().getString("style");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            age="";
            gender="";
            Log.i("Select Price",age);
        }

        continueNext = (ImageView) view.findViewById(R.id.continueNext);
        continueback = (ImageView)view.findViewById(R.id.continueback);

        continueback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WhatToBuyFragment nextFrag= new WhatToBuyFragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frame, nextFrag,"findThisFragment")
                        .addToBackStack("whatToBuy2")
                        .commit();
            }
        });



        spinnerPrice = (Spinner) view.findViewById(R.id.spinnerPrice);
//        adapter = new ArrayAdapter<String>(getActivity(),R.layout.custom_text,range);
//        spinnerPrice.setAdapter(adapter);
        radiusList = new ArrayList<>();





        spinnerPrice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

//                        if(position==0)
//                        {
                            price = spinnerPrice.getItemAtPosition(position).toString().trim();

                            //write your code here
                            Log.i("Price Fragment", price);

                    }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        rl_QuestionCross = (RelativeLayout) view.findViewById(R.id.rl_QuestionCross);

        rl_QuestionCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //getActivity().getFragmentManager().popBackStack();
                HomeFragment homeTabFrag = new HomeFragment();
                Bundle args1 = new Bundle();
                homeTabFrag.setArguments(args1);
                FragmentTransaction fragTransaction1 = getActivity().getSupportFragmentManager().beginTransaction();
                fragTransaction1.replace(R.id.frame, homeTabFrag);
                fragTransaction1.commit();

            }
        });

        continueNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (price.contains("Select Price")) {
                    Constants.showDialog(getResources().getDrawable(R.drawable.ic_warning_black_24dp),getActivity(),
                            "Plese select Price");
                } else {

                WhatToBuyFragment4 fragment = new WhatToBuyFragment4();
                Bundle args = new Bundle();
                args.putString("age", age);
                args.putString("style", style);
                args.putString("price", price);
                fragment.setArguments(args);
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame, fragment);
                fragmentTransaction.addToBackStack("whatToBuy4");
                fragmentTransaction.commit();
            }
        }
        });

        // Inflate the layout for this fragment
        return view;
    }

    public String getText(){

        return Question3Edit.getSelectedItem().toString();
    }

    private void price_list() {

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //   progressBar.setVisibility(View.GONE);

                        try {
                            Gson gson = new Gson();
                            JSONObject obj = new JSONObject(response);


                            Log.i("OCCASSION LIST RESPONSE",response);

                            String status = obj.optString("status");

                            if(status.equalsIgnoreCase("1")) {
                                String message = obj.optString("message");
                                JSONArray data = obj.getJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {

                                    if (i == 0) {
                                        listOfPrice.add(i, "Select Price");
                                        JSONObject obj1 = data.getJSONObject(i);
                                        String id = obj1.optString("id");
                                        String fromprice = obj1.optString("fromprice");
                                        String toprice = obj1.optString("toprice");
                                        listOfPrice.add(fromprice + " To " + toprice);
                                    } else {
                                        JSONObject obj1 = data.getJSONObject(i);
                                        String id = obj1.optString("id");
                                        String fromprice = obj1.optString("fromprice");
                                        String toprice = obj1.optString("toprice");
                                        listOfPrice.add(fromprice + " To " + toprice);
                                    }
                                }
                                Constants.setServices(listOfPrice, getActivity(), spinnerPrice);
                                progressDialog.dismiss();

                            }  else
                            {
                                Toast.makeText(getContext(),obj.optString("displayMessage"),Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "Unable to process this request, please try again later", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "get_price_range");

                return params;
            }
        };


        RequestQueue queue = Volley.newRequestQueue(getActivity());
        stringRequest.setShouldCache(false);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                300000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }
}
