package com.yesitlabs.govava_daniyal.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.URLS.Url_List;
import com.yesitlabs.govava_daniyal.Utils.Constants;
import com.yesitlabs.govava_daniyal.model.ContactDetails;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;


public class WhatToBuyFRagment0 extends Fragment implements View.OnClickListener {

    RelativeLayout rl_openContactList;
    CircleImageView CoverImage;
    ImageView continueNext;
    EditText ed_contact_name;

    SharedPreferences.Editor sEditor;
    String display_image = "";
    String display_name = "";
    String phone_no = "";
    public static final int CONTACT_PICKER_RESULT = 1234;
    ArrayList<ContactDetails> contactDetailsArrayList = new ArrayList<>();
    ContactDetails contactDetails;
    private RelativeLayout rl_QuestionCross;
    private Spinner Question1Edit;
    private String age = "";
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    private Context mContext;
    Boolean checkspinner = false;
    public ArrayAdapter<String> adapter;
    public String[] old = {
         "Select Age",
      "2-9",
    "10-12",
        "13-15",
       "16-24",
       "25-32",
       "33-45",
        "46-55",
 "56-65"

    };
    List<String> radiusList;
    private String style = "",styleIcon = "";
    private List<String> listOfAge;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_what_to_buy, container, false);
        CoverImage = view.findViewById(R.id.CoverImage);

        listOfAge = new ArrayList<>();

        age_list();



        Bundle args = getArguments();
        try{
            style = args.getString("style");
            styleIcon = args.getString("styleIcon");

        }
        catch (Exception e)
        {
            e.printStackTrace();
            style="";

            Log.i("Person style",style);
        }


        Glide.with(getContext())
                .load(Constants.Styles_Image_Url +styleIcon)
                .thumbnail(0.5f)
                .into(CoverImage);

        sEditor = getActivity().getSharedPreferences("code", MODE_PRIVATE).edit();

        ed_contact_name = view.findViewById(R.id.ed_contact_name);
        //mPager.setCurrentItem(1);
        continueNext = view.findViewById(R.id.continueNext);



        Question1Edit = (Spinner) view.findViewById(R.id.spinnerAge);
//        adapter = new ArrayAdapter<String>(getActivity(),R.layout.custom_text,old);
//        Question1Edit.setAdapter(adapter);

        Question1Edit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

//                if (position == 0) {
                    age = Question1Edit.getSelectedItem().toString();


                    //write your code here
                    Log.i("Age Fragment", age);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        rl_QuestionCross = (RelativeLayout) view.findViewById(R.id.rl_QuestionCross);

        continueNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (age.contains("Select Age")) {

                    Constants.showDialog(getResources().getDrawable(R.drawable.ic_warning_black_24dp),getActivity(),
                            "Plese select Age");

                } else {


//                WhatToBuyFragment nextFrag= new WhatToBuyFragment();
//                Bundle args = new Bundle();
//                args.putString("age", "10");
//                nextFrag.setArguments(args);
//                getActivity().getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.frame, nextFrag,"findThisFragment")
//                        .addToBackStack("whatToBuy")
//                        .commit();


                    WhatToBuyFragment3 fragment = new WhatToBuyFragment3();
                    Bundle args = new Bundle();
                    args.putString("age", age);
                    args.putString("style", style);
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frame, fragment);
                    fragmentTransaction.addToBackStack("whatToBuy");
                    fragment.setArguments(args);
                    fragmentTransaction.commit();
                }
            }
        });

        rl_QuestionCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //getActivity().getFragmentManager().popBackStack();
                HomeFragment homeTabFrag = new HomeFragment();
                Bundle args1 = new Bundle();
                homeTabFrag.setArguments(args1);
                FragmentTransaction fragTransaction1 = getActivity().getSupportFragmentManager().beginTransaction();
                fragTransaction1.replace(R.id.frame, homeTabFrag);
                fragTransaction1.commit();


            }
        });

//        rl_openContactList = view.findViewById(R.id.rl_openContactList);
//        ed_contact_name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(Intent.ACTION_PICK);
//                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
//                startActivityForResult(intent, CONTACT_PICKER_RESULT);
//
//            }
//        });


        return view;

    }


    public static String encodeTobase64(Bitmap image) {
        Bitmap immage = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immage.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        Log.d("Image Log:", imageEncoded);
        return imageEncoded;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CONTACT_PICKER_RESULT:
                    //  final EditText phoneInput = (EditText) findViewById(R.id.phoneNumberInput);
                    Cursor cursor = null;
                    String phoneNumber = "";
                    String photo = "";

                    int photo_uri = 0;
                    int contact_name = 0;
                    List<String> allNumbers = new ArrayList<String>();
                    int phoneIdx = 0;
                    try {
                        Uri result = data.getData();
                        String id = result.getLastPathSegment();
                        cursor = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", new String[]{id}, null);
                        phoneIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA);
                        photo_uri = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI);
                        contact_name = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                        if (cursor.moveToFirst()) {
                            while (cursor.isAfterLast() == false) {
                                phoneNumber = cursor.getString(phoneIdx);
                                photo = cursor.getString(photo_uri);
                                display_name = cursor.getString(contact_name);
                                allNumbers.add(phoneNumber);
                                cursor.moveToNext();
                            }
                        } else {
                            //no results actions
                        }
                    } catch (Exception e) {
                        //error actions
                    } finally {
                        if (cursor != null) {
                            cursor.close();
                        }

                        if (display_name != null) {
                            ed_contact_name.setText(display_name);
                            // sEditor.putString("displayNamePreference",display_name);

                            ed_contact_name.setError(null);
                        }
                        Bitmap bitmap;

                        if(photo == null){
                            Glide.with(getActivity()).load(R.drawable.profile_pic).into(CoverImage);


                        }
                        /*else
                            ed_contact_name.setText("");*/
                        if (photo != null) {
                            Glide.with(getActivity()).load(photo).into(CoverImage);

                            Uri myUri = Uri.parse(photo);

                            try {
                                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), myUri);
                                display_image = BitMapToString(bitmap);

                                //sEditor.putString("imagePreference", encodeTobase64(bitmap));

                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        } /*else{
                            Glide.with(getActivity()).load(R.drawable.profile_pic).into(CoverImage);
                           // CoverImage.setImageDrawable(getResources().getDrawable(R.drawable.profile_pic));
                        }*/

                        //ArrayList<ContactDetails> arrayList = new ArrayList<>();


                      /*  Gson gson = new Gson();
                        String jsonFavorites = gson.toJson(contactDetails);
                        sEditor.putString("contactscode", jsonFavorites);
                        sEditor.commit();*/

                        //sEditor.commit();

                        final CharSequence[] items = allNumbers.toArray(new String[allNumbers.size()]);
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle("Choose a number");
                        builder.setItems(items, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                String selectedNumber = items[item].toString();
                                selectedNumber = selectedNumber.replace("-", "");
                                phone_no = selectedNumber;
                                // phoneInput.setText(selectedNumber);
                            }
                        });
                        AlertDialog alert = builder.create();
                        if (allNumbers.size() > 1) {
                            alert.show();
                        } else {
                            String selectedNumber = phoneNumber.toString();
                            selectedNumber = selectedNumber.replace("-", "");
                            phone_no = selectedNumber;
                            // phoneInput.setText(selectedNumber);
                        }

                        if (phoneNumber.length() == 0) {
                            //no numbers found actions
                        }

                        contactDetails = new ContactDetails(display_name, display_image,phone_no);

                    }
                    break;
            }
        } else {
            //activity result error actions
        }
    }

    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

//    public String getText(){
//
//        return Question1Edit.getSelectedItem().toString();
//
//
//
//
//    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
//        if (isVisible)
//            list_of_coupon(); //your request method
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // for the first page.

          Log.i("Age Fragment","Visible");
        }
    }


//  if (getUserVisibleHint()) {
//        list_of_coupon();
//    }

    private void age_list() {

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //   progressBar.setVisibility(View.GONE);

                        try {
                            Gson gson = new Gson();
                            JSONObject obj = new JSONObject(response);


                            Log.i("OCCASSION LIST RESPONSE",response);

                            String status = obj.optString("status");

                            if(status.equalsIgnoreCase("1"))
                            {
                                String message = obj.optString("message");
                                JSONArray data = obj.getJSONArray("data");
                                for(int i = 0; i<data.length();i++) {

                                    if(i==0)
                                    {
                                        listOfAge.add(i,"Select Age");
                                        JSONObject obj1 = data.getJSONObject(i);
                                        String id = obj1.optString("id");
                                        String from_age = obj1.optString("from_age");
                                        String to_age = obj1.optString("to_age");
                                        listOfAge.add(from_age+" To "+to_age);
                                    }
                                    else
                                    {
                                        JSONObject obj1 = data.getJSONObject(i);
                                        String id = obj1.optString("id");
                                        String from_age = obj1.optString("from_age");
                                        String to_age = obj1.optString("to_age");
                                        listOfAge.add(from_age+" To "+to_age);
                                    }

                                }

                                Constants.setServices(listOfAge,getActivity(),Question1Edit);
                                progressDialog.dismiss();

                            }
                            else
                            {
                                Toast.makeText(getContext(),obj.optString("displayMessage"),Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                            }




                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();


                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "Unable to process this request, please try again later", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "get_age");



                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        stringRequest.setShouldCache(false);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                300000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }

    @Override
    public void onClick(View view) {

    }
}
