package com.yesitlabs.govava_daniyal.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.URLS.Url_List;
import com.yesitlabs.govava_daniyal.Utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class WhatToBuyFragment4 extends Fragment {

    ImageView continueNext,continueback;
    RelativeLayout rl_QuestionCross;

    public Spinner Question4Edit;
    public ArrayAdapter<String> adapter;
    private Spinner spinnerOccasion;
    private String age = "";
//    private String gender = "";
    private String price = "";
    private String occassions = "";
    List<String> radiusList;
    private String style = "";
    private List<String> listOfOccassion;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_question_4,
                container, false);
        Bundle args = getArguments();


        listOfOccassion = new ArrayList<>();
        occcassion_list();

        try{
            age = args.getString("age");
//            gender = getArguments().getString("gender");
            price = getArguments().getString("price");
            style = getArguments().getString("style");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            age="";
//            gender="";
            price="";
//            Log.i("Gender Fragment",age);
        }
        listOfOccassion = new ArrayList<>();

        continueNext = (ImageView) view.findViewById(R.id.continueNext);
        continueback = (ImageView)view.findViewById(R.id.continueback);
        continueback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WhatToBuyFragment3 nextFrag= new WhatToBuyFragment3();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frame, nextFrag,"findThisFragment")
                        .addToBackStack("whatToBuy3")
                        .commit();
            }
        });

        spinnerOccasion = (Spinner)view.findViewById(R.id.spinnerOccassion);
//        adapter = new ArrayAdapter<String>(getActivity(),R.layout.custom_text,occasions);
//        spinnerOccasion.setAdapter(adapter);

        radiusList = new ArrayList<>();


        spinnerOccasion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


//                if(position==0)
//                {
                    occassions = spinnerOccasion.getItemAtPosition(position).toString().trim();
                    //write your code here
                    Log.i("Occassion Fragment", occassions);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        rl_QuestionCross = (RelativeLayout) view.findViewById(R.id.rl_QuestionCross);

        rl_QuestionCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // getActivity().getFragmentManager().popBackStack();
                HomeFragment homeTabFrag = new HomeFragment();
                Bundle args1 = new Bundle();
                homeTabFrag.setArguments(args1);
                FragmentTransaction fragTransaction1 = getActivity().getSupportFragmentManager().beginTransaction();
                fragTransaction1.replace(R.id.frame, homeTabFrag);
                fragTransaction1.commit();
            }
        });



        continueNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(occassions.contains("Select Occasions")) {

                    Constants.showDialog(getResources().getDrawable(R.drawable.ic_warning_black_24dp),getActivity(),
                            "Plese select Occasions");
                }else {


                    WhatToBuy5 fragment = new WhatToBuy5();
                    Bundle args = new Bundle();
//                    args.putString("gender", gender);
                    args.putString("age", age);
                    args.putString("price", price);
                    args.putString("style", style);
                    args.putString("occassion", occassions);
                    fragment.setArguments(args);
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frame, fragment);
                    fragmentTransaction.addToBackStack("whatToBuy5");
                    fragment.setArguments(args);
                    fragmentTransaction.commit();
                }
            }
        });


        // Inflate the layout for this fragment
        return view;
    }

    public String getText(){

        return Question4Edit.getSelectedItem().toString();
    }


    private void occcassion_list() {


        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //   progressBar.setVisibility(View.GONE);

                        try {
                            Gson gson = new Gson();
                            JSONObject obj = new JSONObject(response);


                            Log.i("OCCASSION LIST RESPONSE",response);

                            String status = obj.optString("status");

                            if(status.equalsIgnoreCase("1"))
                            {
                                String message = obj.optString("message");
                                JSONArray data = obj.getJSONArray("data");
                                for(int i = 0; i<data.length();i++) {
                                    if(i==0) {
                                        listOfOccassion.add(i,"Select Occasions");
                                        JSONObject obj1 = data.getJSONObject(i);
                                        String id = obj1.optString("id");
                                        String occacion_name = obj1.optString("occacion_name");
                                        listOfOccassion.add(occacion_name);
                                    }else {
                                        JSONObject obj1 = data.getJSONObject(i);
                                        String id = obj1.optString("id");
                                        String occacion_name = obj1.optString("occacion_name");
                                        listOfOccassion.add(occacion_name);
                                    }
                                }

                                Constants.setServices(listOfOccassion,getActivity(),spinnerOccasion);
                                progressDialog.dismiss();
                            }
                            else
                            {
                                Toast.makeText(getContext(),obj.optString("displayMessage"),Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "Unable to process this request, please try again later", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "get_occasion");



                return params;
            }
        };


        RequestQueue queue = Volley.newRequestQueue(getActivity());
        stringRequest.setShouldCache(false);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                300000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }


}
