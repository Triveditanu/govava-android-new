package com.yesitlabs.govava_daniyal.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.Utils.UserSessionManager;
import com.yesitlabs.govava_daniyal.activity.EditProfileActivity;
import com.yesitlabs.govava_daniyal.activity.HomeActivity;
import com.yesitlabs.govava_daniyal.activity.LoginActivity;

import net.londatiga.android.instagram.Instagram;
import net.londatiga.android.instagram.InstagramSession;

import static com.yesitlabs.govava_daniyal.Utils.Constants.CALLBACK_URL;
import static com.yesitlabs.govava_daniyal.Utils.Constants.CLIENT_ID;
import static com.yesitlabs.govava_daniyal.Utils.Constants.CLIENT_SECRET;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SettingFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SettingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private ImageView img_back;
    private TextView txt_editProfile;
    private TextView txt_clear_history;
    private TextView txt_logout;

    private UserSessionManager sessionManager;
    private String providerName = "";
    InstagramSession instagramSession;
    private Instagram mInstagram;
    public SettingFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SettingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SettingFragment newInstance(String param1, String param2) {
        SettingFragment fragment = new SettingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {




        View view = inflater.inflate(R.layout.fragment_setting, container, false);

        sessionManager = new UserSessionManager(getActivity());
        SharedPreferences prefs = getActivity().getSharedPreferences("LoginDetail", Context.MODE_PRIVATE);
        providerName = prefs.getString("providerName", "");

        txt_editProfile = view.findViewById(R.id.txt_editProfile);


        txt_clear_history = view.findViewById(R.id.txt_clear_history);

        txt_logout = view.findViewById(R.id.txt_logout);

        img_back = (ImageView)view.findViewById(R.id.img_back);



        txt_editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Edit_Profile_Fragment homeTabFrag = new Edit_Profile_Fragment();
//                Bundle args1 = new Bundle();
//                homeTabFrag.setArguments(args1);
//                FragmentTransaction fragTransaction1 = getActivity().getSupportFragmentManager().beginTransaction();
//                fragTransaction1.replace(R.id.frame, homeTabFrag).addToBackStack("Edit Profile");
//                fragTransaction1.commit();

                Intent intent = new Intent(getContext(), EditProfileActivity.class);
                startActivity(intent);




            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              getActivity().onBackPressed();
            }
        });

        txt_clear_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDialog();
            }
        });


        txt_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlert_logoutDialog();
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }



    public void showAlertDialog() {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage("Are you sure you want to clear search history");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SharedPreferences preferences = getActivity().getSharedPreferences("searchPrefsKey", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear()
                                .apply();



                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }



    public void showAlert_logoutDialog() {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage("Are you sure you want to Logout");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SharedPreferences preferences = getActivity().getSharedPreferences("LoginDetail", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear()
                                .apply();



                        if(providerName.equalsIgnoreCase("Gmail"))
                        {
                            sessionManager.signOut();

                        }
                        if(providerName.equalsIgnoreCase("Facebook"))
                        {
                            LoginManager.getInstance().logOut();
                        }

                        if(providerName.equalsIgnoreCase("Instagram"))
                        {
//                            instagramSession	= mInstagram.getSession();

                         //   mInstagram.resetSession();
                        }

                        if(providerName.equalsIgnoreCase("Twitter"))
                        {
                            LoginManager.getInstance().logOut();
                        }

                        Intent couponsIntent = new Intent(getContext(), LoginActivity.class);
                        couponsIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        couponsIntent.putExtra("LOGOUT", true);
                        startActivity(couponsIntent);
                        getActivity().finish();

                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


}
