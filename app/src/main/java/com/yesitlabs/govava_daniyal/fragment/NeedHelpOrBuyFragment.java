package com.yesitlabs.govava_daniyal.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.BulletSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.yesitlabs.govava_daniyal.Permissions.PermissionUtility;
import com.yesitlabs.govava_daniyal.Permissions.PermissionsUtils;
import com.yesitlabs.govava_daniyal.Permissions.RuntimePermission;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.URLS.Url_List;
import com.yesitlabs.govava_daniyal.activity.MultipleContactPickerActivity;
import com.yesitlabs.govava_daniyal.activity.WebViewActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NeedHelpOrBuyFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NeedHelpOrBuyFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NeedHelpOrBuyFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    private TextView txt_need_frd;
    private RelativeLayout container_toolbar;
    private ConstraintLayout main_constraint;
    private TextView btn_buyNow;
    private LinearLayout mainLayout;
    public LinearLayout layout;
    public Snackbar snackbar;
    LinearLayout ll;
    // this is an array that holds the IDs of the drawables ...

    private View cell;
    private TextView text;
    private ImageView img_forward;
    private String thumb = "";
    private ImageView image ;
    private TextView txt;
    private String product_id = "";
    private String market_type = "";
    private String medium = "";

    private RuntimePermission runtimePermission;
    private String prodocut_title = "";
    private String prodocut_id = "",pro_image_default = "";
    private String product_price ="";
    private String large = "";
    private String prodocut_detail_url = "";
    private TextView txt_discription;
    private ImageView imageView;
    private View alertView;
    private TextView txt_price;
    private RelativeLayout layout_need_help;


    public NeedHelpOrBuyFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *

     * @return A new instance of fragment NeedHelpOrBuyFragment.
     */
    // TODO: Rename and change types and number of parameters
//    public NeedHelpOrBuyFragment newInstance(String param1, String param2) {
//        NeedHelpOrBuyFragment fragment = new NeedHelpOrBuyFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
//        return fragment;
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         return inflater.inflate(R.layout.activity_buy_now, container, false);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        layout_need_help = getView().findViewById(R.id.layout_need_help);

        ll=(LinearLayout)getView().findViewById(R.id.layout_table);

        TextView txtTitle = getActivity().findViewById(R.id.txtTitle);
        txtTitle.setText("Need Help");
        try
        {
            product_id = getArguments().getString("product_id");
            market_type = getArguments().getString("market_type");
            product_price = getArguments().getString("product_price");

            Log.i("product_id",product_id);
            Log.i("market_type",market_type);
            Log.i("product_price",product_price);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        layout = getView().findViewById(R.id.footerAd);
        runtimePermission = new RuntimePermission(getActivity(), layout);

        image = getView().findViewById(R.id.image);

        mainLayout = getView().findViewById(R.id._linearLayout);

        img_forward = getView().findViewById(R.id.img_forward);
        txt = getView().findViewById(R.id.txt);

        txt_discription =getView().findViewById(R.id.txt_discription);

        main_constraint = (ConstraintLayout)getView().findViewById(R.id.main_constraint);

        txt_need_frd = (TextView)getView().findViewById(R.id.txt_need_frd);

        txt_price = getView().findViewById(R.id.txt_price);

        btn_buyNow = (TextView)getView().findViewById(R.id.btn_buyNow);


        container_toolbar = (RelativeLayout) getView().findViewById(R.id.container_toolbar);

        productDetail();

        txt_need_frd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (PermissionUtility.hasMarshmallow() && PermissionsUtils.checkSelfForContactPermission(getActivity())) {
                    runtimePermission.requestContactPermission();
                } else {
                    Intent intentContactPick = new Intent(getActivity(),MultipleContactPickerActivity.class);

                    intentContactPick.putExtra("prodocut_title", txt.getText().toString());
                    intentContactPick.putExtra("productId", product_id);
                    intentContactPick.putExtra("product_img", thumb);
                    intentContactPick.putExtra("market_type", market_type);
                    intentContactPick.putExtra("product_price", product_price);

                    startActivityForResult(intentContactPick,1000);
                }

            }
        });

        btn_buyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentContactPick = new Intent(getActivity(),WebViewActivity.class);
                intentContactPick.putExtra("product_url", prodocut_detail_url);
                startActivity(intentContactPick);

                Log.i("prodocut_detail_url",prodocut_detail_url);
            }
        });





    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void productDetail() {

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Log.i("Market Buy Now",market_type);
        Log.i("Product Buy Now",product_id);

        String URL = Url_List.URL_PRODUCT_DETAIL+"?access=true&action=newget_product_detail";
// Post params to be sent to the server
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", product_id);

        JsonObjectRequest request_json = new JsonObjectRequest(URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        progressDialog.dismiss();
                        layout_need_help.setVisibility(View.VISIBLE);


                        JSONObject obj = response;
                        String status = obj.optString("status");
                        if(status.equalsIgnoreCase("1"))
                        {
                            try {
                                JSONObject dataJson = obj.getJSONObject("data");


                                Log.i("ProductDetail",obj.toString());

                                prodocut_title = dataJson.optString("title");
                                prodocut_id = dataJson.optString("prodocut_id");
                                String prodocut_price =dataJson.optString("price");
                                prodocut_detail_url = dataJson.optString("url");
                                String prodocut_description = dataJson.optString("description");
                                String market_type = dataJson.optString("merchantName");
                                String productImage = dataJson.optString("imageUrl");
                                thumb = productImage;
                                if(!dataJson.isNull("title")) {

                                    txt.setText(prodocut_title);
                                }

                                if(!dataJson.isNull("price")) {

                                    txt_price.setText(prodocut_price);
                                }

                                if(!dataJson.isNull("description"))
                                {
//                                txt_discription.setText(prodocut_description);

                                    String s= ".";
                                    SpannableString ss1=  new SpannableString(s);
                                    ss1.setSpan(new RelativeSizeSpan(2f), 0,1, 0); // set size
                                    ss1.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 1, 0);// set color
                                    if(prodocut_description.contains("."))
                                    {
                                        String [] values_comma = prodocut_description.split("\\.");

                                        for(int i=0;i<values_comma.length;i++)
                                        {
                                            CharSequence cs = values_comma[i];

                                            SpannableString ss = new SpannableString(cs);
                                            // ss.setSpan(new RelativeSizeSpan(1.5f), 0, StreetType.length(), 0); // set size

                                            ss.setSpan(new BulletSpan(15), 0, cs.length(), 0);

                                            TextView tv = new TextView(getActivity());
                                            if(i%2==0)
                                            {
                                                tv.setBackgroundColor(Color.parseColor("#F1E5CA"));
                                            }else
                                            {
                                                tv.setBackgroundColor(Color.parseColor("#EEC995"));
                                            }

                                            tv.setPadding(15, 15, 10, 15);
                                            tv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                                            tv.setText(" "+ss);
                                            ll.addView(tv);
                                        }
                                    }

                                    else
                                    {
                                        txt_discription.setVisibility(View.VISIBLE);
                                        txt_discription.setText(prodocut_description);

                                    }


                                }else {

                                    txt_discription.setText("");
                                }

                                Glide.with(getActivity())
                                        .load(productImage)
                                        .thumbnail(0.5f)
                                        .into(image);


                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();

                            }
                        }
                        else
                        {
                            Toast.makeText(getContext(), obj.optString("displayMessage"), Toast.LENGTH_SHORT).show();

                            progressDialog.dismiss();
                        }




                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                progressDialog.dismiss();

            }
        });

// add the request object to the queue to be executed
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        request_json.setShouldCache(false);

        request_json.setRetryPolicy(new DefaultRetryPolicy(
                300000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request_json);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == PermissionsUtils.REQUEST_CONTACT) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showSnackBar(R.string.contact_permission_granted);

                Intent intentContactPick = new Intent(getActivity(),MultipleContactPickerActivity.class);
                startActivityForResult(intentContactPick,1000);

            } else {
                showSnackBar(R.string.contact_permission_not_granted);
            }
        }
       else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void showSnackBar(int resId) {
        snackbar = Snackbar.make(layout, resId,
                Snackbar.LENGTH_SHORT);
        snackbar.show();
    }


}
