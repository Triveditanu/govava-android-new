package com.yesitlabs.govava_daniyal.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.URLS.Url_List;
import com.yesitlabs.govava_daniyal.Utils.GridAutofitLayoutManager;
import com.yesitlabs.govava_daniyal.adapter.CategoriesAdapter;
import com.yesitlabs.govava_daniyal.adapter.NavigationCategoryAdapter;
import com.yesitlabs.govava_daniyal.model.Arraylist;
import com.yesitlabs.govava_daniyal.model.CatagoriesModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CategoriesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CategoriesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CategoriesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private ArrayList<Arraylist> horizontalList;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
private CategoriesAdapter adapter;

    private JSONArray dataList;
private Context mContext;
    private OnFragmentInteractionListener mListener;
    String[] data = {"JustVH Women at Walmart", "JustVH Women at Walmart", "JustVH Women at Walmart", "JustVH Women at Walmart", "JustVH Women at Walmart", "JustVH Women at Walmart", "JustVH Women at Walmart", "JustVH Women at Walmart", "JustVH Women at Walmart", "JustVH Women at Walmart",
            "JustVH Women at Walmart", "JustVH Women at Walmart"};
    private String image1 = "";

    LinearLayoutManager horizontal_manager;
    private ArrayList<CatagoriesModel> categoriesList;
    private RecyclerView recyclerView;
    private NavigationCategoryAdapter categoriesProductAdapter;
    Boolean isScrolling = false;
    private View view;

    public CategoriesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CategoriesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CategoriesFragment newInstance(String param1, String param2) {
        CategoriesFragment fragment = new CategoriesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_categories, container, false);

        categoriesList();

        recyclerView = view.findViewById(R.id.cat_grid_recycler);

        horizontalList = new ArrayList<Arraylist>();
        categoriesList = new ArrayList<>();

        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.margin_10);
        recyclerView.addItemDecoration(new GridAutofitLayoutManager(2, spacingInPixels, true, 0));
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));

        categoriesProductAdapter = new NavigationCategoryAdapter(getContext(),categoriesList,recyclerView);

        recyclerView.setHasFixedSize(true);




        return view;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }



    private void categoriesList() {


//        isScrolling = false; // lock this guy,(isScrolling) to make sure,
        // user will not load more when volley is processing another request
        // only load more when  volley is free

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.ROOT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //   progressBar.setVisibility(View.GONE);
                        progressDialog.dismiss();
                        // remember here we are in the main thread, that means,
                        //volley has finished processing request, and we have our response.
                        // What else are you waiting for? update isScrolling = true;
//                        isScrolling = true;

                        try {

                            JSONObject obj = new JSONObject(response);

                            Log.i("CategoryResponse",response);

                            String status = obj.optString("status");

                            if(status.equalsIgnoreCase("1"))
                            {
                                dataList = obj.getJSONArray("data");
                                for (int i = 0; i < dataList.length(); i++) {
                                    JSONObject jsonObject = dataList.getJSONObject(i);
                                    String id = jsonObject.optString("id");
                                    String parent_id = jsonObject.optString("parent_id");
                                    String name = jsonObject.optString("name");
                                    image1 = jsonObject.optString("image");
//                                ImageArrayList.add("https://govava.yesitlabs.com/"+image);
//                                nameArrayList.add(name);

                                    CatagoriesModel categoriesProduct = new CatagoriesModel();
                                    categoriesProduct.setCategoryId(id);
                                    categoriesProduct.setCategoryParentId(parent_id);
                                    categoriesProduct.setCategoryName(name);
                                    categoriesProduct.setCategoryImage(image1);
                                    categoriesList.add(categoriesProduct);
                                }


                                ViewCompat.setNestedScrollingEnabled(recyclerView,false);

                                recyclerView.setAdapter(categoriesProductAdapter);
                            }
                            else
                            {
                                Toast.makeText(getContext(), obj.optString("displaymessage"), Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    //    Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        isScrolling = true;
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), "Unable to process this request Please try again later", Toast.LENGTH_SHORT).show();
//                        new AlertDialog.Builder(getContext())
//                                .setMessage("Unable to process this request Please try again later")
//                                .show();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "get_Categories_list");
                params.put("start", "0");
                params.put("end", "200");


                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }
}
