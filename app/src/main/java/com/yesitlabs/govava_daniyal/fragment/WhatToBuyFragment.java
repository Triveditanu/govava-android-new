package com.yesitlabs.govava_daniyal.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.Utils.Constants;
import com.yesitlabs.govava_daniyal.activity.HomeActivity;
import com.yesitlabs.govava_daniyal.activity.LoginActivity;

import java.util.ArrayList;
import java.util.List;


public class WhatToBuyFragment extends Fragment {


    ImageView continueNext,abc;
    RelativeLayout rl_QuestionCross;
    public Spinner Question1Edit;
    public String[] old = {  "Select Gender",
        "Male",
            "Female",
            "Relative"

    };
    private ImageView continueback;
    private Spinner spinnerGender;
    private String age = "";
    private String gender = "";
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    List<String> radiusList;
    private String style = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_question_2,
                container, false);

      //  age = getArguments().getString("age");
        Bundle args = getArguments();
        if (args != null) {
            age = args.getString("age");
            style = args.getString("style");
           // Toast.makeText(getActivity(),"Value"+age,Toast.LENGTH_SHORT).show();
        }

        continueNext = (ImageView) view.findViewById(R.id.continueNext);

        continueback = (ImageView)view.findViewById(R.id.continueback);

        spinnerGender= (Spinner) view.findViewById(R.id.spinnerGender);
        radiusList = new ArrayList<>();


        for(int i=0;i<old.length;i++)
        {
            if(i==0)
            {
                radiusList.add(i,"Select Gender");
            }
            else
            {
                radiusList.add(i,old[i]);
            }
        }

        Constants.setServices(radiusList,getActivity(),spinnerGender);


        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


//                if(position==0)
//                {
                    gender = spinnerGender.getItemAtPosition(position).toString().trim();

                    //write your code here
                    Log.i("Age Fragment", age);


            }



            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


//        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),R.layout.custom_text, old);
//        Question1Edit.setAdapter(arrayAdapter);
        rl_QuestionCross = (RelativeLayout) view.findViewById(R.id.rl_QuestionCross);


        continueback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WhatToBuyFRagment0 nextFrag= new WhatToBuyFRagment0();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frame, nextFrag,"findThisFragment")
                        .addToBackStack("whatToBuy0")
                        .commit();



            }
        });

//        abc = (ImageView) view.findViewById(R.id.abc);
//        abc.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ViewPager viewPager = HomeActivity.mPager;
//                viewPager.setCurrentItem(viewPager.getCurrentItem()-1);
//            }
//        });

        rl_QuestionCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



//                getActivity().onBackPressed();

                    HomeFragment homeTabFrag = new HomeFragment();
                    Bundle args1 = new Bundle();
                    homeTabFrag.setArguments(args1);
                    FragmentTransaction fragTransaction1 = getActivity().getSupportFragmentManager().beginTransaction();
                    fragTransaction1.replace(R.id.frame, homeTabFrag);
                    fragTransaction1.commit();


            }
        });


        continueNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (gender.contains("Select Gender")) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                    builder1.setMessage("Plese select gender");
                    builder1.setCancelable(true);

                    builder1.setNegativeButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                } else {

                    WhatToBuyFragment3 fragment = new WhatToBuyFragment3();
                    Bundle args = new Bundle();
                    args.putString("gender", gender);
                    args.putString("age", age);
                    args.putString("style", style);
                    fragment.setArguments(args);
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frame, fragment);
                    fragmentTransaction.addToBackStack("findThisFragment");
                    fragment.setArguments(args);
                    fragmentTransaction.commit();
                }
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    public String getText(){

        return Question1Edit.getSelectedItem().toString();


    }
    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
//        if (isVisible)
//            list_of_coupon(); //your request method
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // for the first page.

            try{
                age = getArguments().getString("age");
            }
            catch (Exception e)
            {
                e.printStackTrace();
                age="";
                Log.i("Gender Fragment",age);
            }


          // age = getArguments().getString("age");

        }
    }

}
