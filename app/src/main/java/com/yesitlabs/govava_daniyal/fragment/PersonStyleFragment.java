package com.yesitlabs.govava_daniyal.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.URLS.Url_List;
import com.yesitlabs.govava_daniyal.adapter.PersonStyleAdapter;
import com.yesitlabs.govava_daniyal.model.PersonStyleModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PersonStyleFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PersonStyleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PersonStyleFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    Boolean isScrolling = false;
    private PersonStyleAdapter adapter;

    private OnFragmentInteractionListener mListener;
    private RecyclerView recyclerView;
    private ImageView img_back;
    private JSONArray dataList;
    private String image1 = "";
    private ArrayList<PersonStyleModel> personModel;
    int numberOfColumns = 3;
    LinearLayoutManager manager;
    private View view;
    private String age = "";
    private String gender = "";
    private String price = "";
    private String occassion = "";
    private String relation = "";
    private NestedScrollView nest_scroll;
    private ProgressDialog progressDialog;

    public PersonStyleFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PersonStyleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PersonStyleFragment newInstance(String param1, String param2) {
        PersonStyleFragment fragment = new PersonStyleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
         view = inflater.inflate(R.layout.fragment_person_style, container, false);
        Bundle args = getArguments();
        if (args != null) {
            age = args.getString("age");
            gender = getArguments().getString("gender");
            price = getArguments().getString("price");
            occassion = getArguments().getString("occassion");
            relation = getArguments().getString("relation");
        //    Toast.makeText(getActivity(),"Value"+age+"value2"+gender+"Value3"+price+"Value4"+occassion+"Value5"+relation,Toast.LENGTH_SHORT).show();
        }


        personModel = new ArrayList<>();


        img_back = (ImageView)view.findViewById(R.id.img_back);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeFragment homeTabFrag = new HomeFragment();
                Bundle args1 = new Bundle();
                homeTabFrag.setArguments(args1);
                FragmentTransaction fragTransaction1 = getActivity().getSupportFragmentManager().beginTransaction();
                fragTransaction1.replace(R.id.frame, homeTabFrag).addToBackStack("Find");
                fragTransaction1.commit();
            }
        });



         recyclerView = view.findViewById(R.id.style_rv);
        nest_scroll = view.findViewById(R.id.nest_scroll);

        personModel = new ArrayList<>();

        adapter = new PersonStyleAdapter(getContext(),personModel,age,gender,price,occassion,relation);

        //   productAdapter = new ProductAdapter(productList);
        manager = new GridLayoutManager(getActivity(), numberOfColumns);

        firstLoadStyle();
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        recyclerView.setHasFixedSize(true);

        //  recyclerView.setAdapter(productAdapter);
        recyclerView.setLayoutManager(manager);



        nest_scroll.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if(v.getChildAt(v.getChildCount() - 1) != null) {
                if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                        scrollY > oldScrollY) {
                    //code to fetch more data for endless scrolling
                    if(isScrolling)
                    {
                        loadMoreStyle(personModel.size()+1,20);
                    }
                    else
                    {
                        Toast.makeText(getActivity(),"No more data to show",Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });


        return view;




    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void firstLoadStyle() {

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.URL_STYLE_LIST,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressDialog.dismiss();

                        try {
                            JSONObject obj = new JSONObject(response);


                            Log.i("RESPONSE",response);

                            String status = obj.optString("status");
                            if(status.equalsIgnoreCase("1"))
                            {

                            dataList = obj.getJSONArray("data");
                            for (int i = 0; i < dataList.length(); i++) {
                                JSONObject jsonObject = dataList.getJSONObject(i);
                                String id = jsonObject.optString("id");
                                String about = jsonObject.optString("about");
                                String name = jsonObject.optString("title");
                                image1 = jsonObject.optString("icon");


                                personModel.add(new PersonStyleModel(id, about,name,image1));
                                ViewCompat.setNestedScrollingEnabled(recyclerView,false);
                                adapter.notifyDataSetChanged();


//                                ImageArrayList.add("https://govava.yesitlabs.com/"+image);
//                                nameArrayList.add(name);


                            }

                            recyclerView.setAdapter(adapter);
                            isScrolling = true;
                            }
                            else {
                                Toast.makeText(getContext(),obj.optString("displayMessage"),Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    //    Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        isScrolling = true;
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), "Unable to process this request Please try again later", Toast.LENGTH_SHORT).show();
//                        new AlertDialog.Builder(getContext())
//                                .setMessage("Unable to process this request Please try again later")
//                                .show();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "get_style");
                params.put("start", "0");
                params.put("end", "10");

                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        stringRequest.setShouldCache(false);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                300000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }


    private void loadMoreStyle(int start,int end) {

//        final ProgressWheel progressWheel = (ProgressWheel) view.findViewById(R.id.progress_wheel);
//        progressWheel.setVisibility(View.VISIBLE);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();



        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.URL_STYLE_LIST,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

 //                       progressWheel.setVisibility(View.GONE);
                        progressDialog.dismiss();

                        try {

                            JSONObject obj = new JSONObject(response);


                            Log.i("RESPONSE",response);

                            String status = obj.optString("status");
                            if (status.equalsIgnoreCase("1")) {
                            dataList = obj.getJSONArray("data");
                            for (int i = 0; i < dataList.length(); i++) {
                                JSONObject jsonObject = dataList.getJSONObject(i);
                                String id = jsonObject.optString("id");
                                String about = jsonObject.optString("about");
                                String name = jsonObject.optString("title");
                                image1 = jsonObject.optString("icon");


                                personModel.add(new PersonStyleModel(id, about,name,image1));
                                ViewCompat.setNestedScrollingEnabled(recyclerView,false);
                                adapter.notifyDataSetChanged();


//                                ImageArrayList.add("https://govava.yesitlabs.com/"+image);
//                                nameArrayList.add(name);


                            }

                            recyclerView.setAdapter(adapter);

                            adapter.notifyDataSetChanged();


                            isScrolling = true;
                        }
                            else
                        {
                            // something
                            Toast.makeText(getContext(), obj.optString("displayMessage"), Toast.LENGTH_SHORT).show();
                            isScrolling=false;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
    },
            new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Toast.makeText(getContext(), "Unable to process this request Please try again later", Toast.LENGTH_SHORT).show();
       progressDialog.dismiss();
            // volley finished and returned network error, update and unlock  isScrolling
//                        isScrolling = true;
        //    Toast.makeText(getContext(), "Failed to load more, network error", Toast.LENGTH_SHORT).show();

        }
    }) {
        @Override
        protected Map<String, String> getParams() throws AuthFailureError {
            Map<String, String> params = new HashMap<>();

            params.put("access", "true");
            params.put("action", "get_style");
            params.put("start", String.valueOf(start));
            params.put("end", String.valueOf(end));


            return params;
        }
    };

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        stringRequest.setShouldCache(false);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                300000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

}

}
