package com.yesitlabs.govava_daniyal.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.URLS.Url_List;
import com.yesitlabs.govava_daniyal.activity.HomeActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.yesitlabs.govava_daniyal.Utils.Constants.emailPattern;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ContactUsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ContactUsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ContactUsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private ImageView img_back;
    private TextView txt_contact;
    private TextView call_txt;
    private TextView emial_txt;
    private EditText edt_name,edt_email,edt_phone,edt_message;
    private Button btn_submit;
    private String email = "",name= "",message = "",mobile = "";

    public ContactUsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ContactUsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ContactUsFragment newInstance(String param1, String param2) {
        ContactUsFragment fragment = new ContactUsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);

        edt_name = view.findViewById(R.id.edt_name);
        edt_email = view.findViewById(R.id.edt_email);
        edt_phone = view.findViewById(R.id.edt_phone);
        edt_message = view.findViewById(R.id.edt_message);

        btn_submit = view.findViewById(R.id.btn_submit);

        img_back = (ImageView)view.findViewById(R.id.img_back);

        contact_us();


         call_txt = view. findViewById(R.id.call_txt);
        emial_txt = view. findViewById(R.id.emial_txt);
        String text = "<font color=#000>Call Us</font> "+
                "<font color=#999999></font>";


        String emailText = "<font color=#000>Email Us</font> "+
                "<font color=#999999></font>";

        call_txt.setText(Html.fromHtml(text));
        emial_txt.setText(Html.fromHtml(emailText));

        txt_contact = view.findViewById(R.id.txt_contact);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), HomeActivity.class);
                startActivity(i);
            }
        });


        edt_email .addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                if (!edt_email.getText().toString().matches(emailPattern) && s.length() > 0)
                {
                    edt_email.setError("invalid email");
                }
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // other stuffs
            }
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // other stuffs
            }
        });



        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(edt_name.length()==0)
                {
                    edt_name.setError("Field Can't be Empty.");
                    edt_name.requestFocus();
                    return;
                }

                if(edt_email.length()==0)
                {
//                    edtEmail.setError("Field Can't be Empty.");
//                    edtEmail.requestFocus();
                    email = " ";
                }
                if(edt_email.getText().toString().trim().isEmpty())
                {
                    email = " ";

                }
                else
                {
                    email = edt_email.getText().toString();
                }

                if(edt_message.length()==0)
                {
                    edt_message.setError("Field Can't be Empty.");
                    edt_message.requestFocus();

                    return;
                }

                if(edt_message.getText().toString().trim().isEmpty())
                {
                    edt_message.setError("Space Not Allowed");
                    edt_message.requestFocus();

                    return;
                }


                if(edt_phone.length()<10){
                    edt_phone.setError("Mobile number should be 10 digit ");
                    edt_phone.requestFocus();

                }

                else {
                    name = edt_name.getText().toString().trim();
                    message = edt_message.getText().toString().trim();
                    mobile = edt_phone.getText().toString().trim();
                    contact_form(name,email,mobile,message);


                }
            }
        });




//        txt_contact.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        // Inflate the layout for this fragment
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void contact_us() {

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //   progressBar.setVisibility(View.GONE);
                        progressDialog.dismiss();
                        try {
                            Gson gson = new Gson();
                            JSONObject obj = new JSONObject(response);


                            Log.i("RESPONSE",response);
                            String status = obj.optString("status");

                            JSONObject dataList = obj.getJSONObject("data");

                            String prodocut_title = dataList.optString("id");
                            String prodocut_id = dataList.optString("title");
                            String description = dataList.optString("description");
                            String cphone = dataList.optString("cphone");
                            String cemail = dataList.optString("cemail");

                            txt_contact.setText(Html.fromHtml(description));
                            call_txt.setText(cphone);
                            emial_txt.setText(cemail);


                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
              //          Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), "Unable to process this request Please try again later", Toast.LENGTH_SHORT).show();
//                        new AlertDialog.Builder(getContext())
//                                .setMessage("Unable to process this request Please try again later")
//                                .show();

                    }

                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "contact");


                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        stringRequest.setShouldCache(false);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                300000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }


    private void contact_form(String name,String email,String mobile, String message) {

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //   progressBar.setVisibility(View.GONE);
                        progressDialog.dismiss();
                        try {
                            Gson gson = new Gson();
                            JSONObject obj = new JSONObject(response);

                            Log.i("RESPONSE",response);
                            String status = obj.optString("status");
                            String message = obj.optString("message");

                            if(status.contains("1")){
                                Log.i("ContactForm",message);

                                android.support.v7.app.AlertDialog.Builder builder1 = new android.support.v7.app.AlertDialog.Builder(getContext());
                                builder1.setMessage(message);
                                builder1.setCancelable(true);

                                builder1.setPositiveButton(
                                        "Ok",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                HomeFragment homeTabFrag = new HomeFragment();
                                                Bundle args1 = new Bundle();
                                                homeTabFrag.setArguments(args1);
                                                FragmentTransaction fragTransaction1 = getActivity().getSupportFragmentManager().beginTransaction();
                                                fragTransaction1.replace(R.id.frame, homeTabFrag).addToBackStack("Setting");
                                                fragTransaction1.commit();
                                            }
                                        });

                                android.support.v7.app.AlertDialog alert11 = builder1.create();
                                alert11.show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //          Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), "Unable to process this request Please try again later", Toast.LENGTH_SHORT).show();
//                        new AlertDialog.Builder(getContext())
//                                .setMessage("Unable to process this request Please try again later")
//                                .show();

                    }

                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "contact_form");
                params.put("name", name);
                params.put("email", email);
                params.put("message", message);
                params.put("phone", mobile);


                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        stringRequest.setShouldCache(false);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                300000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }

}
