package com.yesitlabs.govava_daniyal.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.URLS.Url_List;
import com.yesitlabs.govava_daniyal.Utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link WhatToBuy5.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link WhatToBuy5#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WhatToBuy5 extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;



    ImageView continueNext,continueback;
    RelativeLayout rl_QuestionCross;
//    public String occasions[] = {
//            "Select Relationship",
//            "Mother",
//            "Father",
//            "Brother",
//            "Sister",
//            "Son",
//            "Daughter",
//            "Nephew",
//            "Grand Son",
//            "Grand Daughter",
//            "Grand Mother",
//            "Grad Father",
//            "Uncle",
//            "Aunt",
//            "Husband",
//            "Wife",
//            "Girlfriend",
//            "Boyfriend"
//    };
    List<String> radiusList;
    List<String> listOfRelation;

    public Spinner Question4Edit;
    public ArrayAdapter<String> adapter;
    private Spinner spinnerRelation;
    private String age = "";
//    private String gender = "";
    private String price = "";
    private String occassion = "";
    private String relation = "";
    private String style = "";

    public WhatToBuy5() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment WhatToBuy5.
     */
    // TODO: Rename and change types and number of parameters
    public static WhatToBuy5 newInstance(String param1, String param2) {
        WhatToBuy5 fragment = new WhatToBuy5();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_what_to_buy5, container, false);

        Bundle args = getArguments();
        if (args != null) {
            age = args.getString("age");
//            gender = getArguments().getString("gender");
            price = getArguments().getString("price");
            occassion = getArguments().getString("occassion");
            style = getArguments().getString("style");
         //   Toast.makeText(getActivity(),"Value"+age+"value2"+gender+"Value3"+price+"Value4"+occassion,Toast.LENGTH_SHORT).show();
        }

        listOfRelation = new ArrayList<>();

        relation_list();

        continueNext = (ImageView) view.findViewById(R.id.continueNext);
        continueback = (ImageView)view.findViewById(R.id.continueback);
        continueback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WhatToBuyFragment4 nextFrag= new WhatToBuyFragment4();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frame, nextFrag,"findThisFragment")
                        .addToBackStack("whatToBuy4")
                        .commit();
            }
        });

        spinnerRelation = (Spinner)view.findViewById(R.id.spinnerRelation);

        radiusList = new ArrayList<>();




        spinnerRelation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

//                if(position==0)
//                {
                    relation = spinnerRelation.getItemAtPosition(position).toString().trim();
                    //write your code here
                    Log.i("Occassion Fragment", relation);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        rl_QuestionCross = (RelativeLayout) view.findViewById(R.id.rl_QuestionCross);

        rl_QuestionCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // getActivity().getFragmentManager().popBackStack();
                HomeFragment homeTabFrag = new HomeFragment();
                Bundle args1 = new Bundle();
                homeTabFrag.setArguments(args1);
                FragmentTransaction fragTransaction1 = getActivity().getSupportFragmentManager().beginTransaction();
                fragTransaction1.replace(R.id.frame, homeTabFrag);
                fragTransaction1.commit();
            }
        });



        continueNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(relation.contains("Select Relationship")) {

                    Constants.showDialog(getResources().getDrawable(R.drawable.ic_warning_black_24dp),getActivity(),
                            "Plese select Relationship");
                }else {


                    FilterFindGiftFragment fragment = new FilterFindGiftFragment();
                    Bundle args = new Bundle();
                    args.putString("age", age);
//                    args.putString("gender", gender);
                    args.putString("price", price);
                    args.putString("occassion", occassion);
                    args.putString("relation", relation);
                    args.putString("style", style);
                    fragment.setArguments(args);
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frame, fragment);
                    fragmentTransaction.addToBackStack("Home");
                    fragmentTransaction.commit();
                }
            }
        });


        // Inflate the layout for this fragment
        return view;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void relation_list() {

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //   progressBar.setVisibility(View.GONE);

                        try {
                            Gson gson = new Gson();
                            JSONObject obj = new JSONObject(response);


                            Log.i("OCCASSION LIST RESPONSE",response);

                            String status = obj.optString("status");

                            if(status.equalsIgnoreCase("1"))
                            {
                                String message = obj.optString("message");
                                JSONArray data = obj.getJSONArray("data");
                                for(int i = 0; i<data.length();i++) {
                                    if(i==0) {
                                        listOfRelation.add(i,"Select Relationship");
                                        JSONObject obj1 = data.getJSONObject(i);
                                        String id = obj1.optString("id");
                                        String relation_name = obj1.optString("relation_name");

                                        listOfRelation.add(relation_name);
                                    }else {
                                        JSONObject obj1 = data.getJSONObject(i);
                                        String id = obj1.optString("id");
                                        String relation_name = obj1.optString("relation_name");

                                        listOfRelation.add(relation_name);
                                    }
                                }
                                Constants.setServices(listOfRelation,getActivity(),spinnerRelation);
                                progressDialog.dismiss();
                            }
                            else
                            {
                                Toast.makeText(getContext(),obj.optString("displayMessage"),Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "Unable to process this request, please try again later", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "get_relation");



                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        stringRequest.setShouldCache(false);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                300000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }
}
