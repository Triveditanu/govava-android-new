package com.yesitlabs.govava_daniyal.URLS;

public class Url_List {


    public static final String ROOT_URL = "http://govava.yesitlabs.xyz/api/";

    public static final String URL_LOGIN = ROOT_URL+"index.php";
    public static final String URL_SIGNUP = ROOT_URL+"index.php";
    public static final String URL_PRODUCT_DETAIL = ROOT_URL+"product.php";
    public static final String URL_CATEGORY_LIST = ROOT_URL+"index.php";
    public static final String URL_PRODUCT_LIST = ROOT_URL+"product.php";
    public static final String URL_CHAT_LIST = ROOT_URL+"index.php";
    public static final String URL_STYLE_LIST = ROOT_URL+"index.php";

}
