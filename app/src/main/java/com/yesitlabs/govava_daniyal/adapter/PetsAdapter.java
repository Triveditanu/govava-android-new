package com.yesitlabs.govava_daniyal.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.URLS.Url_List;
import com.yesitlabs.govava_daniyal.activity.ProductDetailActivity;
import com.yesitlabs.govava_daniyal.model.PetsModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import me.grantland.widget.AutofitTextView;

public class PetsAdapter extends RecyclerView.Adapter<PetsAdapter.MyViewHolder> {

    private ArrayList<PetsModel> petsList;
    // this data structure carries our title and description
    protected Context context;
    //
//    private int visibleThreshold = 5;
//    private int lastVisibleItem, totalItemCount;
//    private boolean loading;
//    private OnLoadMoreListener onLoadMoreListener;
    private String product_id = "";
    private String market_type = "";
    private String product_price ="";

    public PetsAdapter(Context context, ArrayList<PetsModel> petsList) {

        this.context = context;
        this.petsList = petsList;
//        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
//                totalItemCount = linearLayoutManager.getItemCount();
//                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
//                if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
//                    loading = true;
//                    // End has been reached
//                    // Do something
//                    Log.i("AdapterScrolled", "onScrolled: End reached");
//                    if (onLoadMoreListener != null) {
//                        onLoadMoreListener.onLoadMore();
//                    }
//
//                }
//            }
//        });


    }

    @Override
    public PetsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflate your custom row layout here
        //return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_home, parent, false));


        PetsAdapter.MyViewHolder viewHolder = null;
        if(viewType == 1){
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.pets_items, parent, false);
            viewHolder = new PetsAdapter.MyViewHolder(layoutView);
        }else{
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_item, parent, false);
            viewHolder = new PetsAdapter.MyViewHolder(layoutView);
        }
        return viewHolder;

    }
    //    @SuppressLint("UseSparseArrays")
//    private HashMap<Integer,Boolean> operations = new HashMap<>();
//
//    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(PetsAdapter.MyViewHolder holder, int position) {
        // update your data here
//
//        if (operations.containsKey(position)) {
//
//        } else {
//            operations.put(position, true);

        //    holder.myTextView.setText(petsList.get(position).getTitle());
        holder.txt_petsname.setText(petsList.get(position).getTitle());
        holder.txt_price.setText("$"+petsList.get(position).getPrice());


        String img = petsList.get(position).getProdouct_image();



        String floatRating = (petsList.get(position).getReview());

        try{
            if (!floatRating.equalsIgnoreCase("")||!floatRating.equals(null)) {
                holder.ratingBar.setRating(Float.parseFloat((petsList.get(position).getReview())));
            }else {

                holder.ratingBar.setRating((float) 0.0);
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();

        }



        Glide.with(context)
                .load(img)
                .thumbnail(0.5f)
                .into(holder.pets_img);


        holder.pets_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                product_id = petsList.get(position).getProduct_id();
                market_type = petsList.get(position).getMarket_type();
                product_price = petsList.get(position).getPrice();
                Log.i("Market",market_type);
                Log.i("Product",product_id);
                Log.i("Price",product_price);

//                NeedHelpOrBuyFragment fragment = new NeedHelpOrBuyFragment();
//
//                Bundle args = new Bundle();
//                args.putString("product_id", product_id);
//                args.putString("market_type", market_type);
//                args.putString("product_price", product_price);
//                FragmentTransaction fragmentTransaction = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
//                fragmentTransaction.replace(R.id.frame, fragment);
//                fragmentTransaction.addToBackStack("Need Friend");
//
//                fragment.setArguments(args);
//                fragmentTransaction.commit();


                Intent i = new Intent(context, ProductDetailActivity.class);
                i.putExtra("product_id",product_id);
                i.putExtra("product_price",product_price);
                i.putExtra("market_type",market_type);
                context.startActivity(i);

//                    Intent i = new Intent(context, NeedHelpOrBuyProductActivity.class);
//                    i.putExtra("product_id",product_id);
//                    i.putExtra("market_type",market_type);
//                    context.startActivity(i);
            }
        });

        holder.img_unselected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences prefs = context.getSharedPreferences("LoginDetail", Context.MODE_PRIVATE);
                String userid = prefs.getString("userid", "");
                String productId = petsList.get(position).getProduct_id();
                String marketType = petsList.get(position).getMarket_type();

                add_to_favriote(userid,productId,marketType,holder.img_unselected,position);


            }
        });



//        }

    }

    @Override
    public int getItemCount() {
        return petsList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        // view this our custom row layout, so intialize your variables here
        private TextView title;
        private TextView description;

        AutofitTextView txt_price;
        AutofitTextView txt_petsname;
        ImageView pets_img;
        ImageView img_unselected;
        RatingBar ratingBar;
        private ConstraintLayout rl_click;

        MyViewHolder(View view) {
            super(view);

            img_unselected = itemView.findViewById(R.id.img_unselected);


            txt_petsname =itemView.findViewById(R.id.txt_petsname);
            txt_price =itemView.findViewById(R.id.txt_price);

            ratingBar = itemView.findViewById(R.id.ratingBar);


            pets_img = itemView.findViewById(R.id.pets_img);






//            title = (TextView) view.findViewById(R.id.title);
//            description = (TextView) view.findViewById(R.id.description);

        }
    }


    @Override
    public int getItemViewType(int position) {
        return petsList.get(position) != null ? 1 : 0;
    }
//
//    public void setLoad(){
//        loading = false;
//    }
//
//    public void setOnLoadMoreListener(SelectedCategoriesAdapter.OnLoadMoreListener onLoadMoreListener){
//        this.onLoadMoreListener = onLoadMoreListener;
//    }
//    public interface OnLoadMoreListener {
//        void onLoadMore();
//    }
//    public void setLoaded() {
//        loading = false;
//    }


    private void add_to_favriote(String userId,String product_id,String market_type, final ImageView imgHeart, final int position) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //   progressBar.setVisibility(View.GONE);

                        try {
                            Gson gson = new Gson();
                            JSONObject obj = new JSONObject(response);


                            Log.i("ADD TOFAVRIOTERESPONSE",response);

                            String status = obj.optString("status");
                            String message = obj.optString("message");

                            if(status.contains("true")){

                                imgHeart.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_heart));
                                notifyItemChanged(position);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "Unable to process this request, please try again later", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "wishlist_favorite");
                params.put("user_id", userId);
                params.put("type", market_type);
                params.put("product_id", product_id);


                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(context);
        stringRequest.setShouldCache(false);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                300000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }
}