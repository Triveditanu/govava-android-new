package com.yesitlabs.govava_daniyal.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.activity.HomeActivity;

import java.util.List;
import com.yesitlabs.govava_daniyal.model.Arraylist;

import me.grantland.widget.AutofitTextView;

public class Home_Recycler_Adapter extends RecyclerView.Adapter<Home_Recycler_Adapter.ViewHolder> {

    private  RatingBar ratingBar;
    private  ConstraintLayout rl_click;
    private List<Arraylist> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context mContext;

    // data is passed into the constructor
    public Home_Recycler_Adapter(Context context, List<Arraylist>  data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
    }

    // inflates the cell layout from xml when needed
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.custom_home, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final Arraylist arrayList = mData.get(position);
        holder.myTextView.setText(arrayList.getName());

        holder.iv_rv.setImageResource(arrayList.getImage());

    }

    // total number of cells
    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setClickListener(HomeActivity itemClickListener) {
//        this.mClickListener = (ItemClickListener) itemClickListener;


    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        AutofitTextView myTextView;
        ImageView iv_rv;

        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.tv_name);

            rl_click = itemView.findViewById(R.id.rl_click);
            iv_rv = itemView.findViewById(R.id.Petsimg1);
            ratingBar= itemView.findViewById(R.id.ratingBar);

            ratingBar.setRating(Float.parseFloat("2.0"));

            itemView.setOnClickListener(this);
            iv_rv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                    NeedHelpOrBuyFragment fragment = new NeedHelpOrBuyFragment();
//                    FragmentTransaction fragmentTransaction = ((AppCompatActivity)mContext).getSupportFragmentManager().beginTransaction();
//                    fragmentTransaction.replace(R.id.frame, fragment);
//                    fragmentTransaction.addToBackStack("Need Friend");
//                    fragmentTransaction.commit();

//                    Intent i = new Intent(mContext, NeedHelpOrBuyProductActivity.class);
//                    mContext.startActivity(i);
                }
            });


        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }



    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}