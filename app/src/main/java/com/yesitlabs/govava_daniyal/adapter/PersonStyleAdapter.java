package com.yesitlabs.govava_daniyal.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.Utils.Constants;
import com.yesitlabs.govava_daniyal.fragment.FilterFindGiftFragment;
import com.yesitlabs.govava_daniyal.fragment.WhatToBuyFRagment0;
import com.yesitlabs.govava_daniyal.model.PersonStyleModel;
import com.yesitlabs.govava_daniyal.model.ProductModel;

import java.util.ArrayList;

public class PersonStyleAdapter extends RecyclerView.Adapter<PersonStyleAdapter.MyViewHolder> {

    private  ImageView imageView1;
    private ArrayList<PersonStyleModel> personStyleList;
    private Context context;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private PersonStyleAdapter.OnLoadMoreListener onLoadMoreListener;
    private String product_id = "";
    private String market_type = "";
    private String styleName = "";
    private String ageValue,gendervalue,priceValue,occassionValue,relationValue;

    // this data structure carries our title and description
    public PersonStyleAdapter(Context context, ArrayList<PersonStyleModel> personStyleList,String age,
                              String gender,String price,String occassion, String relation) {

        this.context = context;
        this.personStyleList = personStyleList;
        this.ageValue =age;
        this.gendervalue =gender;
        this.priceValue =price;
        this.occassionValue =occassion;
        this.relationValue =relation;
    }


    @Override
    public PersonStyleAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    //    View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.person_style_item, parent, false);

        PersonStyleAdapter.MyViewHolder viewHolder = null;
        if(viewType == 1){
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.person_style_item, parent, false);
            viewHolder = new PersonStyleAdapter.MyViewHolder(layoutView);
        }else{
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_item, parent, false);
            viewHolder = new PersonStyleAdapter.MyViewHolder(layoutView);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PersonStyleAdapter.MyViewHolder holder, int position) {
        PersonStyleModel movie = personStyleList.get(position);
        holder.title.setText(movie.getTitle());

        String img = personStyleList.get(position).getIcon();

        Glide.with(context)
                .load(Constants.Styles_Image_Url +img)
                .thumbnail(0.5f)
                .into(imageView1);
        Log.i("Image",img);

//        holder.genre.setText(movie.getGenre());
//        holder.year.setText(movie.getYear());


        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                styleName = personStyleList.get(position).getTitle();
                String img1 = personStyleList.get(position).getIcon();

             //   Toast.makeText(context,"Value"+ageValue+"value2"+gendervalue+"Value3"+priceValue+"Value4"+occassionValue
             //           +"Value5"+relationValue,Toast.LENGTH_SHORT).show();
                alertBox(ageValue,gendervalue,priceValue,occassionValue,relationValue,styleName,img1);

            }
        });



    }

    @Override
    public int getItemCount() {
        return personStyleList.size();
    }
    public void alertBox(String ageValue,String gendervalue,String priceValue,String occassionValue,String relationValue,String styleName,String img){

        final Dialog dialog = new Dialog(context);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.setContentView(R.layout.custom_alert_box);
        // Custom Android Allert Dialog Title
        dialog.setTitle("Write a Review");
        Button closeButton = (Button) dialog.findViewById(R.id.btn_cancel);
        ImageView style_img = (ImageView) dialog.findViewById(R.id.style_img);

        Button okButton = (Button) dialog.findViewById(R.id.btn_ok);


        Glide.with(context)
                .load(Constants.Styles_Image_Url +img)
                .thumbnail(0.5f)
                .into(style_img);
        Log.i("Image",img);


        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
//                WhatToBuyFRagment0 nextFrag= new WhatToBuyFRagment0();
//                context.getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.frame, nextFrag,"findThisFragment")
//                        .addToBackStack("whatToBuy0")
//                        .commit();

//                FilterFindGiftFragment fragment = new FilterFindGiftFragment();
//                Bundle args = new Bundle();
//                args.putString("age", ageValue);
//                args.putString("gender", gendervalue);
//                args.putString("price", priceValue);
//                args.putString("occassion", occassionValue);
//                args.putString("relation", relationValue);
//                args.putString("style", styleName);
//                fragment.setArguments(args);
//                FragmentTransaction fragmentTransaction = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
//                fragmentTransaction.replace(R.id.frame, fragment);
//                fragmentTransaction.addToBackStack("Home");
//                fragmentTransaction.commit();


                WhatToBuyFRagment0 fragment = new WhatToBuyFRagment0();
                Bundle args = new Bundle();
                args.putString("style", styleName);
                args.putString("styleIcon", img);
                fragment.setArguments(args);
                FragmentTransaction fragmentTransaction = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame, fragment);
                fragmentTransaction.addToBackStack("Home");
                fragmentTransaction.commit();

            }
        });


        dialog.show();

    }



     class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre;
        CardView card_view;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            imageView1 = (ImageView)view.findViewById(R.id.imageView1);
            card_view = view.findViewById(R.id.card_view);



//            genre = (TextView) view.findViewById(R.id.genre);
//            year = (TextView) view.findViewById(R.id.year);
        }
    }




    @Override
    public int getItemViewType(int position) {
        return personStyleList.get(position) != null ? 1 : 0;
    }

    public void setLoad(){
        loading = false;
    }

    public void setOnLoadMoreListener(PersonStyleAdapter.OnLoadMoreListener onLoadMoreListener){
        this.onLoadMoreListener = onLoadMoreListener;
    }
    public interface OnLoadMoreListener {
        void onLoadMore();
    }
    public void setLoaded() {
        loading = false;
    }




}