package com.yesitlabs.govava_daniyal.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.URLS.Url_List;
import com.yesitlabs.govava_daniyal.fragment.NeedHelpOrBuyFragment;
import com.yesitlabs.govava_daniyal.model.LIstOfFavriote;
import com.yesitlabs.govava_daniyal.model.SearchModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import me.grantland.widget.AutofitTextView;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.MyViewHolder> {

private ArrayList<SearchModel> recyclerModels;
// this data structure carries our title and description
protected Context context;
private String product_id = "";
private String market_type = "";
private String product_price = "";
        ArrayList<LIstOfFavriote> listOfFavriote;
        JSONArray favoriteArray;
        ArrayList<String> listdata;
public SearchAdapter(Context context, ArrayList<SearchModel> recyclerModels, ArrayList<LIstOfFavriote> listOfFavriote,JSONArray favoriteArray) {

        this.context = context;
        this.recyclerModels = recyclerModels;
        this.listOfFavriote = listOfFavriote;
        this.favoriteArray = favoriteArray;


        listdata = new ArrayList<String>();
        JSONArray jArray = this.favoriteArray;
        if (jArray != null) {
        for (int i=0;i<jArray.length();i++){
        try {
        listdata.add(jArray.getString(i));
        } catch (JSONException e) {
        e.printStackTrace();
        }
        }
        }

        }

@Override
public SearchAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        MyViewHolder viewHolder = null;
        if(viewType == 1){
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_home, parent, false);
        viewHolder = new MyViewHolder(layoutView);
        }else{
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_item, parent, false);
        viewHolder = new MyViewHolder(layoutView);
        }


        return viewHolder;

        }
//    @SuppressLint("UseSparseArrays")
//    private HashMap<Integer,Boolean> operations = new HashMap<>();
//
//    @SuppressLint("SetTextI18n")
@Override
public void onBindViewHolder(SearchAdapter.MyViewHolder holder, int position) {
        // update your data here
//
//        if (operations.containsKey(position)) {
//
//        } else {
//            operations.put(position, true);



        holder.myTextView.setText(recyclerModels.get(position).getTitle());
        holder.txt_price.setText("$"+recyclerModels.get(position).getPrice());
        holder.txt_market.setText("At"+" "+recyclerModels.get(position).getMarket_type());

        String img = recyclerModels.get(position).getProdouct_image();

//            String floatRating = (recyclerModels.get(position).getReview());


//        try{
//            if (!floatRating.equalsIgnoreCase("")||!floatRating.equals(null)) {
//                holder.ratingBar.setRating(Float.parseFloat((recyclerModels.get(position).getReview())));
//            }else {
//
//                holder.ratingBar.setRating((float) 0.0);
//            }
//
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//
//        }
        Glide.with(context)
        .load(img)
        .thumbnail(0.5f)
        .into(holder.iv_rv);


        holder.iv_rv.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {

        product_id = recyclerModels.get(position).getProduct_id();
        market_type = recyclerModels.get(position).getMarket_type();
        product_price = recyclerModels.get(position).getPrice();
        Log.i("Market",market_type);
        Log.i("Product",product_id);
        Log.i("Price",product_price);

        NeedHelpOrBuyFragment fragment = new NeedHelpOrBuyFragment();

        Bundle args = new Bundle();
        args.putString("product_id", product_id);
        args.putString("market_type", market_type);
        args.putString("product_price", product_price);
        FragmentTransaction fragmentTransaction = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.addToBackStack("Need Friend");
        fragment.setArguments(args);
        fragmentTransaction.commit();

        }
        });

        String productId = recyclerModels.get(position).getProduct_id();
        System.out.println("Product Id : "+productId);
        if (listdata.contains(recyclerModels.get(position).getProduct_id())) {
        // true
        holder.img_unselected.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_heart));

        holder.img_unselected.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {

        SharedPreferences prefs = context.getSharedPreferences("LoginDetail", Context.MODE_PRIVATE);
        String userid = prefs.getString("userid", "");
        String productId = recyclerModels.get(position).getProduct_id();

        deleteFromFavorite(userid,productId,holder.img_unselected,position);


        }
        });


        }
        else
        {
        holder.img_unselected.setImageDrawable(context.getResources().getDrawable(R.mipmap.unselected_yellow));

        holder.img_unselected.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {

        SharedPreferences prefs = context.getSharedPreferences("LoginDetail", Context.MODE_PRIVATE);
        String userid = prefs.getString("userid", "");
        String productId = recyclerModels.get(position).getProduct_id();

        addToFavorite(userid,productId,holder.img_unselected,position);


        }
        });

        }
        }

@Override
public int getItemCount() {
        return recyclerModels.size();
        }

class MyViewHolder extends RecyclerView.ViewHolder {
    // view this our custom row layout, so intialize your variables here
    private TextView title;
    private TextView description;

    AutofitTextView myTextView;
    AutofitTextView txt_price;
    AutofitTextView txt_market;
    ImageView iv_rv;
    ImageView img_unselected;

    RatingBar ratingBar;
    private ConstraintLayout rl_click;

    MyViewHolder(View view) {
        super(view);


        myTextView = itemView.findViewById(R.id.tv_name);

        txt_price =itemView.findViewById(R.id.txt_price);
        txt_market =itemView.findViewById(R.id.txt_market);

        rl_click = itemView.findViewById(R.id.rl_click);
        iv_rv = itemView.findViewById(R.id.Petsimg1);
        ratingBar= itemView.findViewById(R.id.ratingBar);

        img_unselected = itemView.findViewById(R.id.img_unselected);

//            ratingBar.setRating(Float.parseFloat("2.0"));



//            title = (TextView) view.findViewById(R.id.title);
//            description = (TextView) view.findViewById(R.id.description);

    }
}


    @Override
    public int getItemViewType(int position) {
        return recyclerModels.get(position) != null ? 1 : 0;
    }


    private void addToFavorite(String userId,String product_id, final ImageView imgHeart, final int position) {
//
//        final ProgressDialog progressDialog = new ProgressDialog(context);
//        progressDialog.setMessage("Loading...");
//        progressDialog.setCancelable(false);
//        progressDialog.show();


        String URL = Url_List.URL_SIGNUP+"?access=true&action=wishlist_favorite&user_id="+userId;
// Post params to be sent to the server
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("product_id", product_id);
        params.put("user_id", userId);

        JsonObjectRequest request_json = new JsonObjectRequest(URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

//                        progressDialog.dismiss();


                        JSONObject obj = response;


                        Log.i("Create Group RESPONSE",obj.toString());

                        String status = obj.optString("status");
                        String message = obj.optString("message");

                        if (status.contains("1")) {
                            imgHeart.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_heart));
                            listdata.add(product_id);
                            notifyItemChanged(position);

                        }
                        else
                        {
                            Toast.makeText(context, obj.optString("displayMessage"), Toast.LENGTH_SHORT).show();
//                            progressDialog.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
//                progressDialog.dismiss();

            }
        });

// add the request object to the queue to be executed
        RequestQueue queue = Volley.newRequestQueue(context);
        request_json.setShouldCache(false);

        request_json.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request_json);
    }


    private void deleteFromFavorite(String userId,String product_id, final ImageView imgHeart, final int position) {

        String URL = Url_List.URL_SIGNUP+"?access=true&action=favourite_product_delete";
// Post params to be sent to the server
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("product_id", product_id);
        params.put("user_id", userId);

        JsonObjectRequest request_json = new JsonObjectRequest(URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

//                        progressDialog.dismiss();


                        JSONObject obj = response;


                        Log.i("Create Group RESPONSE",obj.toString());

                        String status = obj.optString("status");
                        String message = obj.optString("message");


                        if (status.contains("1")) {

                            imgHeart.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_heart));
//                            selectedCatList.get(position).setFavoriteProductId(selectedCatList.get(position).getProduct_id());
                            listdata.remove(product_id);
                            notifyItemChanged(position);

                        }
                        else
                        {
                            Toast.makeText(context, obj.optString("displayMessage"), Toast.LENGTH_SHORT).show();

//                            progressDialog.dismiss();
                        }




                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
//                progressDialog.dismiss();

            }
        });

// add the request object to the queue to be executed
        RequestQueue queue = Volley.newRequestQueue(context);
        request_json.setShouldCache(false);

        request_json.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request_json);
    }



}