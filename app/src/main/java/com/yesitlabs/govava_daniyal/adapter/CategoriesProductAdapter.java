package com.yesitlabs.govava_daniyal.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.Utils.Constants;
import com.yesitlabs.govava_daniyal.fragment.CategoriesProductFragment;
import com.yesitlabs.govava_daniyal.model.CatagoriesModel;

import java.util.ArrayList;

public class CategoriesProductAdapter extends RecyclerView.Adapter<CategoriesProductAdapter.RvViewHolder> {

    protected Context context;
    private ListView listView;
    private ArrayAdapter<CharSequence> adapter;
    ArrayList countries_array;

    private ArrayList<CatagoriesModel> couponsList;
    // this data structure carries our title and description


    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private CategoriesProductAdapter.OnLoadMoreListener onLoadMoreListener;
    private String product_id = "";
    private String market_type = "";

    public CategoriesProductAdapter(Context context, ArrayList<CatagoriesModel> couponsList, RecyclerView recyclerView) {
        this.context = context;
        this.couponsList = couponsList;
    }


    View view;

    @Override
    public CategoriesProductAdapter.RvViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        view = inflater.from(parent.getContext()).inflate(R.layout.custom_item, parent, false);
        CategoriesProductAdapter.RvViewHolder rvViewHolder = new CategoriesProductAdapter.RvViewHolder(view);


        RvViewHolder viewHolder = null;
        if(viewType == 1){
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_item, parent, false);
            viewHolder = new CategoriesProductAdapter.RvViewHolder(layoutView);
        }else{
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_item, parent, false);
            viewHolder = new CategoriesProductAdapter.RvViewHolder(layoutView);
        }
        return viewHolder;


    }

    @Override
    public void onBindViewHolder(CategoriesProductAdapter.RvViewHolder holder, final int position) {
        final CatagoriesModel rvdata = couponsList.get(position);
        holder.tv_name.setText(rvdata.getCategoryName());

        String img = rvdata.getCategoryImage();
        Log.i("IMAGE",img);



        holder.img_cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String cat_name = couponsList.get(position).getCategoryName();
                CategoriesProductFragment homeTabFrag = new CategoriesProductFragment();
                Bundle args = new Bundle();
                args.putString("cat_name", cat_name);
                homeTabFrag.setArguments(args);
                FragmentTransaction fragmentTransaction = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction().addToBackStack("Categories product");
                fragmentTransaction.replace(R.id.frame, homeTabFrag);

                homeTabFrag.setArguments(args);
                fragmentTransaction.commit();

            }
        });



        Glide.with(context)
                .load(Constants.Category_Image_Url+img)
                .thumbnail(0.5f)
                .into(holder.img_cat);

//        holder.layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String coupon_id = couponsList.get(position).getCoupon_id();
//                Intent couponsIntent = new Intent(context, CouponsDetailActivity.class);
//                couponsIntent.putExtra("coupon_id", coupon_id);
//                context.startActivity(couponsIntent);
//
////                Intent i = new Intent(context, CouponsDetailActivity.class);
////                i.putExtra("coupon_id", coupon_id);
////                i.setFlags(i.FLAG_ACTIVITY_NEW_TASK | i.FLAG_ACTIVITY_CLEAR_TASK);
////                context.startActivity(i);
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return couponsList.size();
    }

    public class RvViewHolder extends RecyclerView.ViewHolder {
        TextView txt_name;
        TextView tv_name;
        LinearLayout layout;


        ImageView img_cat;

        public RvViewHolder(View itemView) {
            super(itemView);
            img_cat = itemView.findViewById(R.id.image);

            tv_name = itemView.findViewById(R.id.tv_name);



        }
    }


    @Override
    public int getItemViewType(int position) {
        return couponsList.get(position) != null ? 1 : 0;
    }

    public void setLoad(){
        loading = false;
    }

    public void setOnLoadMoreListener(CategoriesProductAdapter.OnLoadMoreListener onLoadMoreListener){
        this.onLoadMoreListener = onLoadMoreListener;
    }
    public interface OnLoadMoreListener {
        void onLoadMore();
    }
    public void setLoaded() {
        loading = false;
    }


}