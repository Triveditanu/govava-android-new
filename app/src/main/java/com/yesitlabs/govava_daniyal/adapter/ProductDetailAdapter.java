package com.yesitlabs.govava_daniyal.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.model.Arraylist;
import com.yesitlabs.govava_daniyal.model.MyData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.AccessControlContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class ProductDetailAdapter extends RecyclerView.Adapter<ProductDetailAdapter.MyViewHolder> {
    ArrayList personNames;
    ArrayList personImages;
    Context context;
    public ProductDetailAdapter(Context context, ArrayList personNames, ArrayList personImages) {
        this.context = context;
        this.personNames = personNames;
        this.personImages = personImages;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_detail_item, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
        return vh;


    }

    @SuppressLint("UseSparseArrays")
    private HashMap<Integer,Boolean> operations = new HashMap<>();

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        if (operations.containsKey(position)) {

        }
        else
        {
            WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();

            int width = display.getWidth();  // deprecated
            int height = display.getHeight();  // deprecated

            int wdthPx = width/3;
            int heightPx = height/5;

            Log.i("size of list", String.valueOf(personImages.size()));
            ImageView image = null;

            if(position<4)
            {
                image = new ImageView(context);
                image.setLayoutParams(new android.view.ViewGroup.LayoutParams(wdthPx,heightPx));
                image.setImageResource(R.mipmap.men);
                image.setMaxHeight(wdthPx);
                image.setMaxWidth(heightPx);
                holder.layout.addView(image);

                // Adds the view to the layout
            }
            if(position==4)
            {
                image = new ImageView(context);
                image.setLayoutParams(new android.view.ViewGroup.LayoutParams(80,60));
                image.setImageResource(R.drawable.ic_keyboard_arrow_right_black_24dp);
                image.setMaxHeight(20);
                image.setMaxWidth(20);
                holder.layout.addView(image);

                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Toast.makeText(context,"jkhkfjghjkgh",Toast.LENGTH_SHORT).show();
                    }
                });

            }


        }




//        if(position==2)
//        {
//            holder.image.setAlpha(.7f);
//            personImages.add(R.drawable.googlplus_icon);
//        }
//        else
//        {
//
//        }



    }

   /* @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        // set the data in items

        holder.image.setImageResource((Integer) personImages.get(position));
        // implement setOnClickListener event on item view.
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // display a toast with person name on item click

            }
        });*/

    @Override
    public int getItemCount() {
        return personNames.size();
    }
    class MyViewHolder extends RecyclerView.ViewHolder {
        // init the item view's
        TextView name;
//        ImageView image;
        LinearLayout layout;
        public MyViewHolder(View itemView) {
            super(itemView);
            // get the reference of item view's

//            image =  itemView.findViewById(R.id.image);
            layout = itemView.findViewById(R.id.imagetype);


        }
    }



}