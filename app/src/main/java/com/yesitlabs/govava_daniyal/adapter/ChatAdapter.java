package com.yesitlabs.govava_daniyal.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.fragment.NeedHelpOrBuyFragment;
import com.yesitlabs.govava_daniyal.model.ChatModel;
import com.yesitlabs.govava_daniyal.model.ProductModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import me.grantland.widget.AutofitTextView;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<ChatModel> recyclerModels;
    // this data structure carries our title and description
    protected Context context;
    private String product_id = "";
    private String market_type = "",avatar = "";
    private String userid = "";

    public ChatAdapter(Context context, ArrayList<ChatModel> recyclerModels) {

        this.context = context;
        this.recyclerModels = recyclerModels;

        SharedPreferences prefs = context.getSharedPreferences("LoginDetail", Context.MODE_PRIVATE);
        userid = prefs.getString("userid", "");
        avatar = prefs.getString("avatar", "");
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder;

        if(viewType==1){
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_item, parent, false);
            viewHolder = new RightChatHolder(view);
        }
        else
        if(viewType==2){
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_item_left, parent, false);
            viewHolder = new LeftChatHolder(view);
        }
        else
        if(viewType==3){
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_item_product, parent, false);
            viewHolder = new ProductChatHolder(view);
        }
        else{
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_item, parent, false);
            viewHolder = new RightChatHolder(view);
        }

        return viewHolder;

    }
    //    @SuppressLint("UseSparseArrays")
//    private HashMap<Integer,Boolean> operations = new HashMap<>();
//
//    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        if (holder instanceof RightChatHolder) {
            final RightChatHolder holder1 = (RightChatHolder) holder;

            holder1.txt_message.setText(recyclerModels.get(position).getMessages());
            holder1.txt_message.setText(recyclerModels.get(position).getMessages());

//            Glide.with(context).load(recyclerModels.get(position).get)

            Glide.with(context).load("https://govava.yesitlabs.com/assets/front/uploads/" +avatar).into(holder1.imgUser);

            holder1.imgUser.setImageDrawable(context.getResources().getDrawable(R.mipmap.contactlist_img_four));

        }

        else if(holder instanceof LeftChatHolder)
        {
            final LeftChatHolder holder1 = (LeftChatHolder) holder;

            holder1.txt_message.setText(recyclerModels.get(position).getMessages());

//            Glide.with(context).load(recyclerModels.get(position).get)

            holder1.imgUser.setImageDrawable(context.getResources().getDrawable(R.mipmap.contactlist_img_four));

        }
        else if(holder instanceof ProductChatHolder)
        {
            final ProductChatHolder holder1 = (ProductChatHolder) holder;

            holder1.txt_message.setText(recyclerModels.get(position).getProductName());

            holder1.txt_price.setText("Price : $"+recyclerModels.get(position).getProductPrice());

            Glide.with(context).load(recyclerModels.get(position).getProductImage()).into(holder1.img_pro);

        }
    }

    @Override
    public int getItemCount() {
        return recyclerModels.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        // view this our custom row layout, so intialize your variables here
        private TextView title;
        private TextView description;

        TextView myTextView;
        AutofitTextView txt_price;
        ImageView iv_rv;
        RatingBar ratingBar;
        private ConstraintLayout rl_click;

        MyViewHolder(View view) {
            super(view);


            myTextView = itemView.findViewById(R.id.txt_message);


        }
    }


    @Override
    public int getItemViewType(int position) {
        int type=0;

        if (recyclerModels.get(position).getProductId()!=null)
        {
            type = 3;
        }
        else if(recyclerModels.get(position).getUser_id().equalsIgnoreCase(userid))
        {
            type=1; // For Sender side Layout
        }
        else if(!recyclerModels.get(position).getUser_id().equalsIgnoreCase(userid))
        {
            type=2;  // For Receiver Side Layout
        }

        if(type==0){
            throw new RuntimeException("Set Message Type ( Message Type is Null )");
        }
        else {
            return type;

        }

    }


    protected class LeftChatHolder extends RecyclerView.ViewHolder {

        public TextView txt_message;
        public CircleImageView imgUser;

        LeftChatHolder(View view) {
            super(view);

            txt_message = view.findViewById(R.id.txt_message);
            imgUser = view.findViewById(R.id.imgUserPic);

        }
    }

    protected class RightChatHolder extends RecyclerView.ViewHolder {

        public TextView txt_message;
        public CircleImageView imgUser;

        RightChatHolder(View view) {
            super(view);

            txt_message = view.findViewById(R.id.txt_message);
            imgUser = view.findViewById(R.id.imgUserPic);

        }
    }

    protected class ProductChatHolder extends RecyclerView.ViewHolder {

        public TextView txt_message;
        public TextView txt_price;
        public ImageView img_pro;

        ProductChatHolder(View view) {
            super(view);

            txt_message = view.findViewById(R.id.txt_message);
            txt_price = view.findViewById(R.id.txt_price);
            img_pro = view.findViewById(R.id.img_pro);


        }
    }

}