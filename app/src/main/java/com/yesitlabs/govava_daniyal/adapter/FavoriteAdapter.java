package com.yesitlabs.govava_daniyal.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.URLS.Url_List;
import com.yesitlabs.govava_daniyal.fragment.NeedHelpOrBuyFragment;
import com.yesitlabs.govava_daniyal.model.FavrioteModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import me.grantland.widget.AutofitTextView;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.MyViewHolder> {

    private ArrayList<FavrioteModel> recyclerModels;
    // this data structure carries our title and description
    protected Context context;
    private String product_id = "";
    private String market_type = "";
    private String product_price = "";


    public FavoriteAdapter(Context context, ArrayList<FavrioteModel> recyclerModels) {

        this.context = context;
        this.recyclerModels = recyclerModels;
    }

    @Override
    public FavoriteAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        FavoriteAdapter.MyViewHolder viewHolder = null;
        if(viewType == 1){
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.favorite_item, parent, false);
            viewHolder = new FavoriteAdapter.MyViewHolder(layoutView);
        }else{
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_item, parent, false);
            viewHolder = new FavoriteAdapter.MyViewHolder(layoutView);
        }
        return viewHolder;

    }
    //    @SuppressLint("UseSparseArrays")
//    private HashMap<Integer,Boolean> operations = new HashMap<>();
//
//    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(FavoriteAdapter.MyViewHolder holder, int position) {
        // update your data here
//
//        if (operations.containsKey(position)) {
//
//        } else {
//            operations.put(position, true);

        holder.myTextView.setText(recyclerModels.get(position).getTitle());
        holder.txt_price.setText("$"+recyclerModels.get(position).getPrice());
        holder.txt_market.setText("At"+" "+recyclerModels.get(position).getMarket_type());


        String img = recyclerModels.get(position).getProdouct_image();



        String floatRating = (recyclerModels.get(position).getReview());


        try{
            if (!floatRating.equalsIgnoreCase("")||!floatRating.equals(null)) {
                holder.ratingBar.setRating(Float.parseFloat((recyclerModels.get(position).getReview())));
            }else {

                holder.ratingBar.setRating((float) 0.0);
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();

        }



        Glide.with(context)
                .load(img)
                .thumbnail(0.5f)
                .into(holder.iv_rv);


        holder.iv_rv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                product_id = recyclerModels.get(position).getProduct_id();
                market_type = recyclerModels.get(position).getMarket_type();
                product_price = recyclerModels.get(position).getPrice();
                Log.i("Market",market_type);
                Log.i("Product",product_id);
                Log.i("Price",product_price);

                NeedHelpOrBuyFragment fragment = new NeedHelpOrBuyFragment();

                Bundle args = new Bundle();
                args.putString("product_id", product_id);
                args.putString("market_type", market_type);
                args.putString("product_price", product_price);
                FragmentTransaction fragmentTransaction = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame, fragment);
                fragmentTransaction.addToBackStack("Need Friend");
                fragment.setArguments(args);
                fragmentTransaction.commit();

//                    Intent i = new Intent(context, NeedHelpOrBuyProductActivity.class);
//                    i.putExtra("product_id",product_id);
//                    i.putExtra("market_type",market_type);
//                    context.startActivity(i);
            }
        });
//        }

        holder.img_unselected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences prefs = context.getSharedPreferences("LoginDetail", Context.MODE_PRIVATE);
                String userid = prefs.getString("userid", "");
                String deleteId = recyclerModels.get(position).getWishlist_id();
                String marketType = recyclerModels.get(position).getMarket_type();

                delete_group(deleteId,userid,position);

            }
        });



    }

    @Override
    public int getItemCount() {
        return recyclerModels.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        // view this our custom row layout, so intialize your variables here
        private TextView title;
        private TextView description;

        AutofitTextView myTextView;
        AutofitTextView txt_price;
        AutofitTextView txt_market;
        ImageView iv_rv;
        ImageView img_unselected;

        RatingBar ratingBar;
        private ConstraintLayout rl_click;

        MyViewHolder(View view) {
            super(view);


            myTextView = itemView.findViewById(R.id.tv_name);

            txt_price =itemView.findViewById(R.id.txt_price);
            txt_market =itemView.findViewById(R.id.txt_market);

            rl_click = itemView.findViewById(R.id.rl_click);
            iv_rv = itemView.findViewById(R.id.Petsimg1);
            ratingBar= itemView.findViewById(R.id.ratingBar);

            img_unselected = itemView.findViewById(R.id.img_unselected);

            ratingBar.setRating(Float.parseFloat("2.0"));



//            title = (TextView) view.findViewById(R.id.title);
//            description = (TextView) view.findViewById(R.id.description);

        }
    }


    @Override
    public int getItemViewType(int position) {
        return recyclerModels.get(position) != null ? 1 : 0;
    }

    private void add_to_favriote(String userId,String product_id,String market_type, final ImageView imgHeart, final int position) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //   progressBar.setVisibility(View.GONE);

                        try {
                            Gson gson = new Gson();
                            JSONObject obj = new JSONObject(response);


                            Log.i("ADD TOFAVRIOTERESPONSE",response);

                            String status = obj.optString("status");
                            String message = obj.optString("message");

                            if(status.contains("1")){

                                imgHeart.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_heart));
                                notifyItemChanged(position);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "Unable to process this request, please try again later", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "wishlist_favorite");
                params.put("user_id", userId);
                params.put("type", market_type);
                params.put("productid", product_id);


                return params;
            }
        };


        RequestQueue queue = Volley.newRequestQueue(context);
        stringRequest.setShouldCache(false);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                300000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }



    private void delete_group(String deleteId,String userid, int position) {


//        isScrolling = false; // lock this guy,(isScrolling) to make sure,
        // user will not load more when volley is processing another request
        // only load more when  volley is free

        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //   progressBar.setVisibility(View.GONE);
                        progressDialog.dismiss();
                        // remember here we are in the main thread, that means,
                        //volley has finished processing request, and we have our response.
                        // What else are you waiting for? update isScrolling = true;
//                        isScrolling = true;

                        try {

                            JSONObject obj = new JSONObject(response);

                            Log.i("CategoryResponse",response);

                            String status = obj.optString("status");

                            if(status.equalsIgnoreCase("1"))
                            {

                                recyclerModels.remove(position);
                                notifyDataSetChanged();

                            }

                            else
                            {
                                Toast.makeText(context, obj.optString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();

                        progressDialog.dismiss();
                        Toast.makeText(context, "network error!", Toast.LENGTH_SHORT).show();
                        new AlertDialog.Builder(context)
                                .setMessage(error.toString())
                                .show();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "favourite_product_delete");
                params.put("wishlist_id", deleteId);
                params.put("userid", userid );


                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(context);
        stringRequest.setShouldCache(false);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                300000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }


}