package com.yesitlabs.govava_daniyal.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.URLS.Url_List;
import com.yesitlabs.govava_daniyal.activity.ChatActivity;
import com.yesitlabs.govava_daniyal.model.ChatListModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.MyViewHolder> {

    private List<ChatListModel> moviesList;
    private  Context mContext;
    private String userid = "";

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre;
        public ImageView img_need_frd;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.txt_personName);
            img_need_frd = view.findViewById(R.id.img_need_frd);
//            genre = (TextView) view.findViewById(R.id.genre);
//            year = (TextView) view.findViewById(R.id.year);
        }
    }


    public ChatListAdapter(List<ChatListModel> moviesList, Context mContext) {
        this.moviesList = moviesList;
        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ChatListModel movie = moviesList.get(position);
        holder.title.setText(movie.getGroup_name());


        holder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String group_name =  moviesList.get(position).getGroup_name();
                String group_id_group =  moviesList.get(position).getCgroup_id();
                Intent i = new Intent(mContext, ChatActivity.class);
                i.putExtra("prodocut_title", group_name);
                i.putExtra("group_id_group", group_id_group);
                i.putExtra("navigation", "navigation");
                mContext.startActivity(i);
            }
        });

        holder.img_need_frd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences prefs = mContext.getSharedPreferences("LoginDetail", Context.MODE_PRIVATE);
                userid = prefs.getString("userid", "");

                String delete_id =  moviesList.get(position).getCgroup_id();
                delete_group(delete_id,userid,position);


            }
        });



//        holder.genre.setText(movie.getGenre());
//        holder.year.setText(movie.getYear());
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }


    private void delete_group(String deleteId,String userid, int position) {


//        isScrolling = false; // lock this guy,(isScrolling) to make sure,
        // user will not load more when volley is processing another request
        // only load more when  volley is free

        final ProgressDialog progressDialog = new ProgressDialog(mContext);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url_List.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //   progressBar.setVisibility(View.GONE);
                        progressDialog.dismiss();
                        // remember here we are in the main thread, that means,
                        //volley has finished processing request, and we have our response.
                        // What else are you waiting for? update isScrolling = true;
//                        isScrolling = true;

                        try {

                            JSONObject obj = new JSONObject(response);

                            Log.i("CategoryResponse",response);

                            String status = obj.optString("status");

                            if(status.equalsIgnoreCase("1"))
                            {

                                moviesList.remove(position);
                                notifyDataSetChanged();

                                }

                            else
                            {
                                Toast.makeText(mContext, obj.optString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();

                        progressDialog.dismiss();
                        Toast.makeText(mContext, "network error!", Toast.LENGTH_SHORT).show();
                        new AlertDialog.Builder(mContext)
                                .setMessage(error.toString())
                                .show();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("access", "true");
                params.put("action", "delete_group_byid");
                params.put("groupid", deleteId);
                params.put("userid", userid );


                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(mContext);
        stringRequest.setShouldCache(false);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

}