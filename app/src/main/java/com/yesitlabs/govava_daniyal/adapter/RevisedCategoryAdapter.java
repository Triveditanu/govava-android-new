package com.yesitlabs.govava_daniyal.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.Utils.Constants;
import com.yesitlabs.govava_daniyal.fragment.CategoriesProductFragment;
import com.yesitlabs.govava_daniyal.model.CatagoriesModel;

import java.util.List;

public class RevisedCategoryAdapter extends RecyclerView.Adapter<RevisedCategoryAdapter.MyViewHolder> {
    private Context context;
    private List<CatagoriesModel> categoryList;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_name;
        TextView tv_name;
        LinearLayout layout;


        ImageView img_cat;

        public MyViewHolder(View itemView) {
            super(itemView);
            img_cat = itemView.findViewById(R.id.image);
            tv_name = itemView.findViewById(R.id.tv_name);
        }
    }

    public RevisedCategoryAdapter(Context context, List<CatagoriesModel> categoryList) {
        this.context = context;
        this.categoryList = categoryList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_category_adapter, parent, false);

        return new MyViewHolder(itemView);
    }
//    @SuppressLint("UseSparseArrays")
//    private HashMap<Integer,Boolean> operations = new HashMap<>();

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {

//        if (operations.containsKey(position)) {
//
//        } else {
//            operations.put(position, true);
        final CatagoriesModel rvdata = categoryList.get(position);
        holder.tv_name.setText(rvdata.getCategoryName());

        String img = rvdata.getCategoryImage();
        Log.i("IMAGE", img);


        holder.img_cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String cat_name = categoryList.get(position).getCategoryName();
                CategoriesProductFragment homeTabFrag = new CategoriesProductFragment();
                Bundle args = new Bundle();
                args.putString("cat_name", cat_name);
                homeTabFrag.setArguments(args);
                FragmentTransaction fragmentTransaction = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame, homeTabFrag);
                homeTabFrag.setArguments(args);
                fragmentTransaction.commit();

            }
        });

        Glide.with(context)
                .load(Constants.Image_Url +img)
                .thumbnail(0.5f)
                .into(holder.img_cat);

    }


    @Override
    public int getItemCount() {
        return categoryList.size();
    }
    
}

