package com.yesitlabs.govava_daniyal.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.bumptech.glide.Glide;
import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.fragment.NeedHelpOrBuyFragment;
import com.yesitlabs.govava_daniyal.model.CatagoriesModel;
import com.yesitlabs.govava_daniyal.model.RevisedHomePageModel;

import java.util.List;

import me.grantland.widget.AutofitTextView;

public class RevisedCustomAdapter extends RecyclerView.Adapter{
    private Context context;
    private List<RevisedHomePageModel> revisedHomePageModelList;
    private List<CatagoriesModel> catagoriesModelList;
    private RecyclerView homeRecycler;


    public RevisedCustomAdapter(Context context, List<RevisedHomePageModel> revisedHomePageModelList,
                                List<CatagoriesModel> catagoriesModelList) {
        this.context = context;
        this.revisedHomePageModelList = revisedHomePageModelList;
        this.catagoriesModelList = catagoriesModelList;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_home_category_header, parent, false);

        View itemView2 = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_home_adapter, parent, false);

        if (viewType==1)
        {
            return new CategoryViewHolder(itemView);
        }
        else if (viewType==2)
        {
            return new ProductViewHolder(itemView2);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {


        final RevisedHomePageModel revisedHomePageModel = revisedHomePageModelList.get(position);

        if (revisedHomePageModel.getItemType()==1) {

            RevisedCategoryAdapter categoriesProductAdapter = new RevisedCategoryAdapter(context, catagoriesModelList);
            LinearLayoutManager horizontal_manager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            ((CategoryViewHolder) holder).categoryRecycler.setLayoutManager(horizontal_manager);

//            ((CategoryViewHolder) holder).categoryRecycler.addItemDecoration(new DividerItemDecoration(context,
//                    DividerItemDecoration.VERTICAL));

            ((CategoryViewHolder) holder).categoryRecycler.setAdapter(categoriesProductAdapter);
            
        }
        else {
            int totalCount = revisedHomePageModelList.size() - 1;
//            ((ProductViewHolder) holder).txtGrandTotal.setText("$" + prefManager.getTotalPrice());

            ((ProductViewHolder) holder).txtProductName.setText(revisedHomePageModel.getProductTitle());
            ((ProductViewHolder) holder).txtPrice.setText(revisedHomePageModel.getProductPrice());


            String img = revisedHomePageModelList.get(position).getProdouctImage();



            String floatRating = (revisedHomePageModelList.get(position).getProductReview());

            if (floatRating != null) {
                //    holder.ratingBar.setRating(Float.parseFloat(floatRating));
            }

            Glide.with(context)
                    .load(img)
                    .thumbnail(0.5f)
                    .into(((ProductViewHolder) holder).imgProduct);


            ((ProductViewHolder) holder).imgProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    NeedHelpOrBuyFragment fragment = new NeedHelpOrBuyFragment();
                    Bundle args = new Bundle();
                    args.putString("product_id", revisedHomePageModelList.get(position).getProductId());
                    args.putString("market_type", revisedHomePageModelList.get(position).getProductMarketType());
                    FragmentTransaction fragmentTransaction = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frame, fragment);
                    fragmentTransaction.addToBackStack("Need Friend");
                    fragment.setArguments(args);
                    fragmentTransaction.commit();

//                    Intent i = new Intent(context, NeedHelpOrBuyProductActivity.class);
//                    context.startActivity(i);
                }
            });
            
        }
    }

    public int getItemCount() {
        return revisedHomePageModelList.size();
    }


    public class CategoryViewHolder extends RecyclerView.ViewHolder{

      RecyclerView categoryRecycler;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            categoryRecycler = itemView.findViewById(R.id.categoryRecycler);
        }
    }


    public class ProductViewHolder extends RecyclerView.ViewHolder{

        AutofitTextView txtProductName;
        AutofitTextView txtPrice;
        ImageView imgProduct;
        RatingBar ratingBar;

        public ProductViewHolder(View itemView) {
            super(itemView);
            txtPrice = itemView.findViewById(R.id.txtPrice);

            txtProductName =itemView.findViewById(R.id.txtProductName);
            imgProduct = itemView.findViewById(R.id.imgProduct);
            ratingBar= itemView.findViewById(R.id.ratingBar);


        }
    }


    @Override
    public int getItemViewType(int position) {

        switch (revisedHomePageModelList.get(position).getItemType())
        {
            case 1: return 1;

            case 2: return 2;
        }

        return super.getItemViewType(position);
    }



}