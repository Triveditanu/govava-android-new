package com.yesitlabs.govava_daniyal.Utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionManager {
    private static final String prefsfile = "MyPrefsName";
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context context;

    public SessionManager(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(prefsfile,
                Context.MODE_PRIVATE);
        editor = preferences.edit();
    }
    public void saveInstagramAccessToken(String stringToken)
    {
        editor.putString(AppConstants.INSTAGRAM_ACCESS_TOKEN,stringToken);
        editor.commit();
    }

    public String getInstagramAccessToken() {
        return preferences.getString(AppConstants.INSTAGRAM_ACCESS_TOKEN,"");
    }


}
