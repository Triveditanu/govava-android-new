package com.yesitlabs.govava_daniyal.Utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class AppConstants {

    public static String INSTAGRAM_CALLBACK_URL = "Your callback url";
    // replace below credentials with yours own
    public static String INSTAGRAM_CLIENT_ID = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    public static String INSTAGRAM_CLIENT_SECRET = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    public static final String INSTAGRAM_ACCESS_TOKEN = "INSTAGRAM_ACCESS_TOKEN";

    public static boolean isInternetIsAvailable(Context mContext) {
        ConnectivityManager connectivity = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }

}
