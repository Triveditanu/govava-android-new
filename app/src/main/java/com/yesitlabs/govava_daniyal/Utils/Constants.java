package com.yesitlabs.govava_daniyal.Utils;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.yesitlabs.govava_daniyal.R;
import com.yesitlabs.govava_daniyal.activity.EditProfileActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Constants {


    public static String Language = "EN";
    public static String ApiKey = "VK_USER_007";
    public static String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";


    public static String FacebookAccount = "364415154371125";
    public static String InstagramAccount = "7e72c2c02d4b4c1e94a3c0876b91ebf5";

    public static final String CLIENT_ID = "7e72c2c02d4b4c1e94a3c0876b91ebf5";
    public static final String CLIENT_SECRET = "your client secret";
    public static final String CALLBACK_URL = "redirect uri here";

    public static final String Image_Url = "http://govava.yesitlabs.xyz/assets/front/";
    public static final String Styles_Image_Url = "http://govava.yesitlabs.xyz/assets/front/styles/";
    public static final String Category_Image_Url = "http://govava.yesitlabs.xyz/assets/front/category/";
    public static final String Profile_Image = "https://govava.yesitlabs.xyz/assets/front/uploads/";

    public static String selectedItem="";

    public static void showDialog(Drawable icon, Context context, String msg){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_alert_layout);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        ImageView imgLogo = dialog.findViewById(R.id.imgLogo);

        text.setText(msg);
        imgLogo.setImageDrawable(icon);
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    public static boolean isValidPassword ( final String password){

        Pattern pattern;
        Matcher matcher;

        String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }



    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String setServices(List<String> spinnerList, Context context, Spinner spinner) {

        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(context,R.layout.custom_text,spinnerList){
            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(context.getResources().getColor(R.color.black));
                }
                else {
                    tv.setTextColor(context.getResources().getColor(R.color.black));
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.custom_text);
        spinner.setAdapter(spinnerArrayAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // If user change the default selection
                // First item is disable and it is used for hint
                if(position > 0){
                    // Notify the selected item text

                    ((TextView) spinner.getSelectedView()).setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    selectedItem = (String) parent.getItemAtPosition(position);

                }
                else
                {
                    ((TextView) spinner.getSelectedView()).setTextColor(context.getResources().getColor(R.color.grey));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return selectedItem;
    }

    public static int getAge(int year, int month, int day){
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }

        Integer ageInt = new Integer(age);

        return ageInt;
    }


    public void alertBox(AppCompatActivity context, String msg, int frameId){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.alert_box);

        TextView text = (TextView) dialog.findViewById(R.id.txt_msg);


        text.setText(msg);

        Button btn_dialogYes = (Button) dialog.findViewById(R.id.btn_dialogYes);
        btn_dialogYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EditProfileActivity.class);
                context.startActivity(intent);
            }
        });


        Button btn_dialogNo = (Button) dialog.findViewById(R.id.btn_dialogNo);
        btn_dialogNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        try {
            dialog.show();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }


    public static String changeDateFormat(String comingDate, String fromFormat, String toFormat)
    {
        SimpleDateFormat inputFormat = new SimpleDateFormat(fromFormat);
        SimpleDateFormat outputFormat = new SimpleDateFormat(toFormat);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(comingDate);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }



}